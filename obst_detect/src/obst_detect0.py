"""
Obstacle and clear path detection
"""

import numpy as np
import numba
from numba import jit
import cv2
from time import sleep, time
import math
import logging
import redis
import os
from pathlib import Path
import json

from shm.reader import SharedMemoryFrameReader
from shm.writer import SharedMemoryFrameWriter

import state
from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

os.makedirs('/home/pi/tmp/images', exist_ok=True)

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

CAMHEIGHT = 62  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 60
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 12   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50
ZTHRESHOLD = 2

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

OUT_GRID = 0
OUT_COLORMAP = 1

CAM_ANGLE_COS = math.cos(math.radians(state.cameraAngle))
CAM_ANGLE_SIN = math.sin(math.radians(state.cameraAngle))

##### Vision #####

grid = np.empty((GRIDX, GRIDZ, 2), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 7), dtype=np.uint16)
sums = np.empty((GRIDX, GRIDZ), dtype=np.uint32)
dist = np.empty((GRIDX, GRIDZ, 128), dtype=np.uint8)

@jit(nopython=True)
def verts2accum(vs:numba.float32[:,:], a:numba.uint16[:,:,:], s:numba.uint32[:,:], d:numba.uint8[:,:,:], a_sin:numba.float32, a_cos:numba.float32):
    a.fill(0)
    s.fill(0)
    d.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 25.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]

        z = int((z0 * a_cos - y0 * a_sin) * 25.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * a_sin + y0 * a_cos) * 100) + GROUNDY
        # print(x, z, y)
        if y > 127: continue
        if y < 1: y = 1
        # Add y to sum of previous y
        a[x, z, 0] += 1
        s[x, z] += y
        # Record lowest y
        if a[x, z, 1] == 0 or y < a[x, z, 1]: a[x, z, 1] = y
        # Record highest y
        if y > a[x, z, 3]: a[x, z, 3] = y
        # Inc dist counter
        if d[x, z, y] < 255: d[x, z, y] += 1

    #After all the points are tallied we update the other columns in accum
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if a[x, z, 0] == 0: continue
            a[x, z, 2] = int(s[x, z] / a[x, z, 0] + 0.5)
            min = 0
            mode = 0
            mode_cnt = 0
            max = 0
            for i in range(128):
                if d[x, z, i] > 1:
                    if min == 0: min = i
                    max = i
                    if d[x, z, i] > mode_cnt:
                        mode = i
                        mode_cnt = d[x, z, i]
            a[x, z, 4] = min
            a[x, z, 5] = mode
            a[x, z, 6] = max

@jit(nopython=True)
def accum2grid(a, g):
    g.fill(0)
    for x in range(GRIDX):
        for z in range(GRIDZ):
            mode = a[x, z, 5]
            if mode == 0: continue
            low = a[x, z, 4]
            high = a[x, z, 6]
            if mode - low <= ZTHRESHOLD and high - mode <= ZTHRESHOLD:
                g[x, z, 0] = 1
            elif mode - low > ZTHRESHOLD and high - mode > ZTHRESHOLD:
                g[x, z, 0] = 3
            else:
                g[x, z, 0] = 2
            g[x, z, 1] = mode

@jit(nopython=True)
def gridFlow(g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != GRID_HORIZ: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = GROUNDY
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == GRID_NAVIGABLE:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0:
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = GRID_NAVIGABLE
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] != GRID_NAVIGABLE and g[x - 1, z, 0] == GRID_NAVIGABLE and g[x + 1, z, 0] == GRID_NAVIGABLE and g[x, z - 1, 0] == GRID_NAVIGABLE and g[x, z + 1, 0] == GRID_NAVIGABLE:
                g[x, z, 0] = GRID_NAVIGABLE

@jit(nopython=True)
def obstaclePadding(g):
    padding = (
        # gdgt: 44 cm wide
        (-1, 6), (0, 6), (1, 6),
        (-3, 5), (-2, 5), (-1, 5), (0, 5), (1, 5), (2, 5), (3, 5),
        (-4, 4), (-3, 4), (-2, 4), (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4),
        (-5, 3), (-4, 3), (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3),
        (-5, 2), (-4, 2), (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        (-6, 1), (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1),
        (-6, 0), (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
        (-6, -1), (-5, -1), (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1),
        (-5, -2), (-4, -2), (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2), (4, -2), (5, -2),
        (-5, -3), (-4, -3), (-3, -3), (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3), (3, -3), (4, -3), (5, -3),
        (-4, -4), (-3, -4), (-2, -4), (-1, -4), (0, -4), (1, -4), (2, -4), (3, -4), (4, -4),
        (-3, -5), (-2, -5), (-1, -5), (0, -5), (1, -5), (2, -5), (3, -5),
        (-1, -6), (0, -6), (1, -6),
    )
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] == GRID_HORIZ or g[x, z, 0] == GRID_TRANS or g[x, z, 0] == GRID_VERT:
                for p in padding:
                    if x + p[0] < 0 or x + p[0] >= GRIDX or z + p[1] < 0 or z + p[1] >= GRIDZ: continue
                    if g[x + p[0], z + p[1], 0] == GRID_NAVIGABLE:
                        g[x + p[0], z + p[1], 0] = GRID_PADDING

@jit(nopython=True)
def findPath2(g):
    dead_end = False
    first_obst = 0
    x = GRIDXHALF
    heading = 0
    heading_lock = False
    for z in range(GRIDZ):
        # print(x, z, g[x, z, 0])
        if g[x, z, 0] != GRID_NAVIGABLE:
            if first_obst == 0: first_obst = z
            new_x = -1
            for i in range(1, 10):
                if x - i >= 0 and g[x - i, z, 0] == GRID_NAVIGABLE:
                    new_x = x - i
                    break
                elif x + i < GRIDX and g[x + i, z, 0] == GRID_NAVIGABLE:
                    new_x = x + i
                    break
            if new_x == -1:
                dead_end = True
                break
            else:
                x = new_x
        g[x, z, 0] = GRID_PATH
        if z == 0: continue
        h = float(x - GRIDXHALF) / z
        if not heading_lock:
            if heading == 0:
                heading = h
            elif heading > 0:
                if h > heading: heading = h
                else: heading_lock = True
            elif heading < 0:
                if h < heading: heading = h
                else: heading_lock = True

    # return (first_obst, int(math.degrees(math.atan(heading))), dead_end)
    return (first_obst, heading, dead_end)

@jit(nopython=True)
def grid2out(grid, out):
    out.fill(0)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == GRID_NO_DATA: continue
            out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]

    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

	##### SETUP #####
    # Tell the CAN proxy what packets we want
    red.rpush('CANfilter', json.dumps({
        'action': 'add',
        'can_id': CAN_CMD_HALT,
        'can_mask': 0x7FF,
        'req_by': FILE
    }))
    red.rpush('CANfilter', json.dumps({
        'action': 'add',
        'can_id': CAN_CMD_VISION,
        'can_mask': 0x7FF,
        'req_by': FILE
    }))

    shm_verts = None
    shm_grid = None

    try:

        shm_verts = SharedMemoryFrameReader('verts')

        shm_grid = SharedMemoryFrameWriter('grid')
        logging.info(f'shm_grid: {shm_grid.shm_name}')

        speak_direct(red, 'obstacle detection running')

        while True:

            if state.state == CAN_CMD_PGM_RUNNING:

                now = time()

                verts = None
                while verts is None:
                    verts, c = shm_verts.get()

                verts2accum(verts, accum, sums, dist, CAM_ANGLE_SIN, CAM_ANGLE_COS)

                accum2grid(accum, grid)

                gridFlow(grid)

                obstaclePadding(grid)

                first_obst, heading, dead_end = findPath2(grid)

                dt = time() - now

                print(f"{int(1.0/dt)} FPS Obst: {first_obst} Head: {heading} Deadend: {dead_end}")

                grid2out(grid, out)
                shm_grid.add(out)

                red.rpush('CANtx', json.dumps({
                    'can_id': CAN_TS_VIS_OBST,
                    'dlc': 3,
                    'data': [first_obst, 128 + heading, 1 if dead_end else 0]
                }))

            else:
                sleep(2)

            CANrx = red.lpop(FILE)
            if CANrx is not None:
                CANrx = json.loads(CANrx)
                logging.info(f'CANrx: {CANrx}')
                if CANrx['can_id'] == CAN_CMD_VISION:
                    if CANrx['data'][0] == CAN_CMD_VISION_PGM:
                        if CANrx['data'][1] == CAN_CMD_VISION_PGM_CAPTURE:
                            state.state = CANrx['data'][2]
                    elif CANrx['data'][0] == CAN_CMD_D435_ANGLE:
                        state.cameraAngle = 90 - CANrx['data'][1]
                        logging.info(f'state.cameraAngle: {state.cameraAngle}')
                        CAM_ANGLE_COS = math.cos(math.radians(state.cameraAngle))
                        logging.info(f'CAM_ANGLE_COS: {CAM_ANGLE_COS}')
                        CAM_ANGLE_SIN = math.sin(math.radians(state.cameraAngle))
                        logging.info(f'CAM_ANGLE_SIN: {CAM_ANGLE_SIN}')
                if CANrx['can_id'] == CAN_CMD_HALT:
                    logging.info('shutdown')
                    state.state = CAN_CMD_PGM_QUIT

            if state.state == CAN_CMD_PGM_QUIT:
                break

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')

    if shm_verts: shm_verts.release()
    if shm_grid: shm_grid.release()

if __name__ == '__main__':
