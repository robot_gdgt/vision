# License: Apache 2.0. See LICENSE file in root directory.
# Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

"""
OpenCV and Numpy Point cloud Software Renderer

This sample is mostly for demonstration and educational purposes.
It really doesn't offer the quality or performance that can be
achieved with hardware acceleration.

Usage:
------
Keyboard:
    [f]     Fill holes
    [p]     Pause
    [d]     Cycle through decimation values
    [s]     Save PNG (./out.png)
    [q\ESC] Quit
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd
from numba import jit
import os

print(__file__)

# Create folder to store the frames
framesN = 0
for name in os.listdir('.'):
#     print(name)
    if name[0:6] == 'frames':
        n = int(name[6:])
        if n > framesN: framesN = n
# print(framesN)

# framesN += 1
# os.mkdir("./frames%d" % (framesN), 0o777)
# print("./frames%d created" % (framesN + 1))

# App state object class
class AppState:

    def __init__(self, *args, **kwargs):
        self.WIN_NAME = "win1"
        self.distance = 2
        self.decimate = 0
        self.fill = False
        self.spat = False
        self.temp = False
        self.paused = False
        self.laser = True

    def reset(self):
        self.distance = 0, 0, 2

state = AppState()
print("Initial settings:")
vd(state)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 15)

# Start streaming
profile = pipeline.start(config)

# Getting the depth sensor's depth scale (see rs-align example for explanation)
# depth_sensor = profile.get_device().first_depth_sensor()
# depth_scale = depth_sensor.get_depth_scale()
# print("Depth Scale is: " , depth_scale)

# Get stream profile and camera intrinsics
profile = pipeline.get_active_profile()
depth_profile = rs.video_stream_profile(profile.get_stream(rs.stream.depth))
depth_intrinsics = depth_profile.get_intrinsics()
# w, h = depth_intrinsics.width, depth_intrinsics.height
# print("w: %d h: %d" % (w, h))
# vd(("w", w, "h", h))
fov = rs.rs2_fov(depth_intrinsics)
vd(fov)

device = profile.get_device()
# depth_sensor = device.query_sensors()[0]
depth_sensor = profile.get_device().first_depth_sensor()
laser_pwr = depth_sensor.get_option(rs.option.laser_power)
print("laser power = ", laser_pwr)
laser_range = depth_sensor.get_option_range(rs.option.laser_power)
print("laser power range = " , laser_range.min , "~", laser_range.max)
# depth_sensor.set_option(rs.option.laser_power, set_laser)

# Processing blocks
pc = rs.pointcloud()
dec_filter = rs.decimation_filter()
dec_filter.set_option(rs.option.filter_magnitude, 2 ** state.decimate)

spat_filter = rs.spatial_filter()     # Spatial    - edge-preserving spatial smoothing

temp_filter = rs.temporal_filter()    # Temporal   - reduces temporal noise
temp_filter.set_option(rs.option.holes_fill, float(4))

CAMHEIGHT = 62  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 80
GRIDZSTART = 7 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 4   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50

cv2.namedWindow(state.WIN_NAME, cv2.WINDOW_AUTOSIZE)
cv2.resizeWindow(state.WIN_NAME, (640 + 640 + int(480 / GRIDZ) * GRIDX, 480))

grid = np.empty((GRIDX, GRIDZ), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 32), dtype=np.uint8)

@jit(nopython=True)
def verts2accum(vs, a):
    a.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        x = int(v[0] * 25 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]
        # z1 = z0 * 0.866 - y0 * 0.500
        # y1 = z0 * 0.500 + y0 * 0.866

        z = int((z0 * 0.866 - y0 * 0.500) * 25 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * 0.500 + y0 * 0.866) * 100) + GROUNDY
        if y < 1: y = 1
        elif y > 255: y = 255
        # Add y to sum of previous y
        # a[x, z, 0] += 1
        # a[x, z, 1] += y
        # Add y to sum of previous y
        if y > a[x, z, 0]: a[x, z, 0] = y
        # Add y to list for grid cell
        # n = a[x, z, 0]
        # if n < 31:
        #     n += 1
        #     a[x, z, n] = y
        #     a[x, z, 0] = n

@jit(nopython=True)
def accum2grid(a, g):
    g.fill(0)
    for x in range(GRIDX):
        for z in range(GRIDZ):
            # print(a[x, z])
            # if a[x, z, 0] > 0:
            #     g[x, z] = 255 - int(a[x, z, 1] / a[x, z, 0] + 0.5)
            g[x, z] = a[x, z, 0]
            # n = a[x, z, 0]
            # if n == 0: continue
            # elif n == 1: g[x, z] = a[x, z, 1]
            # elif n == 2: g[x, z] = int((a[x, z, 1] + a[x, z, 2]) / 2 + 0.5)
            # else:
            #     a[x, z, 1:n + 1].sort()
            #     if n % 2 == 0:
            #         g[x, z] = int((a[x, z, n >> 1] + a[x, z, (n >> 1) + 1]) / 2 + 0.5)
            #     else:
            #         g[x, z] = a[x, z, (n + 1) >> 1]

@jit(nopython=True)
def fillHoles(grid):
    filled = True
    while filled:
        filled = False
        for z in range(1, GRIDZ):
            for x in range(1, GRIDX - 1):
                if grid[x, z] != -999: continue
                if grid[x-1, z] != -999 and grid[x, z-1] != -999 and grid[x+1, z] != -999:
                    grid[x, z] = int((grid[x-1, z] + grid[x, z-1] + grid[x+1, z]) / 3 + 0.5)
                    filled = True

# @jit(nopython=True)
# def fillHoles(grid):
#     for z in range(1, GRIDZ - 1):
#         for x in range(1, GRIDX - 1):
#             n = 0
#             s = 0
#             if grid[x, z] != -999:
#                 n += 1
#                 s += grid[x, z]
#             if grid[x - 1, z] != -999:
#                 n += 1
#                 s += grid[x - 1, z]
#             if grid[x + 1, z] != -999:
#                 n += 1
#                 s += grid[x + 1, z]
#             if grid[x, z - 1] != -999:
#                 n += 1
#                 s += grid[x, z - 1]
#             if grid[x, z + 1] != -999:
#                 n += 1
#                 s += grid[x, z + 1]
#             if n > 0:
#                 grid[x, z] = int(s / n + 0.5)

@jit(nopython=True)
def grid2out(grid, out):
    for x in range(GRIDX):
        for z in range(GRIDZ):
#             if grid[x, z] == -999:
#                 out[x, z] = (64, 64, 64)
#             elif z == 0 or grid[x, z-1] == -999:
#                 out[x, z] = (64, 64, 64)
#             elif abs(grid[x, z] - grid[x, z-1]) < 5:
#                 out[x, z] = (255, 255, 255)
#             else:
#                 out[x, z] = (0, 0, 255)
            if grid[x, z] == 0:
                out[x, z] = (64, 64, 64)
            elif grid[x, z] < 55:
                out[x, z] = (255, 255, 255)
            else:
                out[x, z] = (0, 0, 255)

def printGrid(grid):
    for z in range(GRIDZ - 1, -1, -1):
        for x in range(GRIDX):
            if grid[x, z] == -999:
                print("     ", end='')
            else:
                print("%5d" % (grid[x, z]), end='')
        print('')
    print('')

now = time.time()
frameN = 0

while True:
    if not state.paused:
        # now2 = time.time()
        # Grab camera data
        frames = pipeline.wait_for_frames()

        depth_frame = frames.get_depth_frame()
        color_frame = frames.get_color_frame()
        if not depth_frame or not color_frame:
            continue

        # now = time.time()

        depth_frame = dec_filter.process(depth_frame)
        if state.spat: depth_frame = spat_filter.process(depth_frame)
        if state.temp: depth_frame = temp_filter.process(depth_frame)

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        # depth_image = cv2.resize(depth_image, (640, 480))
        color_image = np.asanyarray(color_frame.get_data())
        # color_image = cv2.resize(color_image, (640, 480))

        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        # dt = time.time() - now2
        # print("\nget data %.2fms" % (dt*1000))
        # now2 = time.time()

        points = pc.calculate(depth_frame)

        # dt = time.time() - now2
        # print("pc.calculate %.2fms" % (dt*1000))
        # now2 = time.time()

        # Pointcloud data to array
        verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz
#         print("verts shape", verts.shape)

#         minX = verts[:, 0].min()
#         maxX = verts[:, 0].max()
#         print("X %f %f" % (minX, maxX))
#         minY = verts[:, 1].min()
#         maxY = verts[:, 1].max()
#         print("Y %f %f" % (minY, maxY))
#         minZ = verts[:, 2].min()
#         maxZ = verts[:, 2].max()
#         print("Z %f %f" % (minZ, maxZ))
#         quit()

#         xSize = int(round(max(abs(minX), maxX) * 10, 0)) * 2
#         print("xSize %d" % (xSize))
#         zSize = int(round(maxZ * 10, 0)) * 2
#         print("zSize %d" % (zSize))

        # dt = time.time() - now2
        # print("get_vertices %.2fms" % (dt*1000))
        # now2 = time.time()

        verts2accum(verts, accum)
#         printGrid(grid)

        # dt = time.time() - now2
        # print("verts2accum %.2fms" % (dt*1000))
        # now2 = time.time()

        accum2grid(accum, grid)

        # dt = time.time() - now2
        # print("accum2grid %.2fms" % (dt*1000))
        # now2 = time.time()

        if state.fill: fillHoles(grid)

#         dt = time.time() - now2
#         print("3 %.2fms" % (dt*1000))
#         now2 = time.time()

        # grid2out(grid, out)
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        out = cv2.applyColorMap(grid, cv2.COLORMAP_BONE)

        # dt = time.time() - now2
        # print("grid2out %.2fms" % (dt*1000))
        # now2 = time.time()

        out2 = cv2.resize(np.rot90(out), (out.shape[1] * int(480 / out.shape[0]), 480), interpolation = cv2.INTER_NEAREST)

        # dt = time.time() - now2
        # print("out2 resize %.2fms" % (dt*1000))
        # now2 = time.time()

        images = np.hstack((color_image, depth_colormap, out2))

        # dt = time.time() - now2
        # print("hstack %.2fms" % (dt*1000))
        # now2 = time.time()

        cv2.imshow(state.WIN_NAME, images)
        # cv2.imwrite("./frames%d/frame%06d.png" % (framesN, frameN), images)
        frameN += 1

        # dt = time.time() - now2
        # print("imshow %.2fms" % (dt*1000))
        # now2 = time.time()

        dt = time.time() - now

        cv2.setWindowTitle(
            state.WIN_NAME, "%d FPS (%.2fms) D: %d P: %d%s%s%s%s%s" %
            (1.0/dt, dt*1000, state.decimate, 640 * 480 / (4 ** state.decimate), " LASER" if state.laser else "", " FILLED" if state.fill else "", " SPAT" if state.spat else "", " TEMP" if state.temp else "", " PAUSED" if state.paused else ""))

    now = time.time()

    key = cv2.waitKey(1)

    if key == ord("l"):
        state.laser ^= True
        depth_sensor.set_option(rs.option.laser_power, LASERPWR if state.laser else 0)

    if key == ord("p"):
        state.paused ^= True

    if key == ord("f"):
        state.fill ^= True

    if key == ord("s"):
        state.spat ^= True

    if key == ord("t"):
        state.temp ^= True

    if key == ord("d"):
        state.decimate = (state.decimate + 1) % 4
#         if state.decimate == 0: state.decimate = 1
        dec_filter.set_option(rs.option.filter_magnitude, 2 ** state.decimate)
        # GRIDSCALE = 4 * 2 ** state.decimate

    if key == ord("v"):
        np.save('verts.npy', verts)

    if key == ord("w"):
        cv2.imwrite('./out.png', out)

    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break

# Stop streaming
pipeline.stop()

# os.system("ffmpeg -f image2 -pattern_type glob -i \"./frames%d/*?png\" -r 15 -vcodec mpeg4 -y \"./frames%d/output.mp4\"" % (framesN, framesN))
