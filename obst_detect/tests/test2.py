# License: Apache 2.0. See LICENSE file in root directory.
# Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

"""
Process 1 frame of pointcloud data and present it as a matplotlib 3D cdolumn chart
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd

import matplotlib.pyplot as plt
# This import registers the 3D projection, but is otherwise unused.
from mpl_toolkits.mplot3d import Axes3D  # noqa: F401 unused import

class AppState:

    def __init__(self, *args, **kwargs):
        self.WIN_NAME = 'test2'
        self.distance = 2
        self.decimate = 1
        self.scale = True
        self.color = True

    def reset(self):
        self.distance = 0, 0, 2

state = AppState()
vd(state)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

# Start streaming
profile = pipeline.start(config)

# Getting the depth sensor's depth scale (see rs-align example for explanation)
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()
print("Depth Scale is: " , depth_scale)

# Get stream profile and camera intrinsics
# profile = pipeline.get_active_profile()
depth_profile = rs.video_stream_profile(profile.get_stream(rs.stream.depth))
depth_intrinsics = depth_profile.get_intrinsics()
w, h = depth_intrinsics.width, depth_intrinsics.height
print("w: %d h: %d" % (w, h))
vd(("w", w, "h", h))
fov = rs.rs2_fov(depth_intrinsics)
vd(fov)

# Processing blocks
pc = rs.pointcloud()
decimate = rs.decimation_filter()
decimate.set_option(rs.option.filter_magnitude, 2 ** state.decimate)

# cv2.namedWindow(state.WIN_NAME, cv2.WINDOW_AUTOSIZE)
# cv2.resizeWindow(state.WIN_NAME, w, h)

now = time.time()

while True:
    # Grab camera data
    frames = pipeline.wait_for_frames()

    depth_frame = frames.get_depth_frame()
#     print('depth_frame1')
#     vd(depth_frame)
    depth_frame = decimate.process(depth_frame)
#     print('depth_frame2')
#     vd(depth_frame)

    # Grab new intrinsics (may be changed by decimation)
    depth_intrinsics = rs.video_stream_profile(depth_frame.profile).get_intrinsics()
    print('depth_intrinsics')
    vd(depth_intrinsics)
    w, h = depth_intrinsics.width, depth_intrinsics.height

    depth_image = np.asanyarray(depth_frame.get_data())
    print('depth_image')
    vd(depth_image)
    print('depth at center')
    y = int(h /2)
    vd(y)
    x = int(w /2)
    vd(x)
    vd(depth_image[x, y])

    points = pc.calculate(depth_frame)
#     print('points')
#     vd(points)

    # Pointcloud data to arrays
    v = points.get_vertices()
#     print('v')
#     vd(v)
    verts = np.asanyarray(v).view(np.float32).reshape(-1, 3)  # xyz
#     print('verts')
#     vd(verts)

    minX = verts[:, 0].min()
    maxX = verts[:, 0].max()
    print("X %f %f" % (minX, maxX))
    minY = verts[:, 1].min()
    maxY = verts[:, 1].max()
    print("Y %f %f" % (minY, maxY))
    minZ = verts[:, 2].min()
    maxZ = verts[:, 2].max()
    print("Z %f %f" % (minZ, maxZ))

    xSize = int(round(max(abs(minX), maxX) * 10, 0)) * 2
    print("xSize %d" % (xSize))
    zSize = int(round(maxZ * 10, 0)) * 2
    print("zSize %d" % (zSize))

    grid = np.full([30, 30], -999, dtype=np.int16)

    for v in verts:
#         print(v)
        x = int(round(v[0], 1) * 10) + 15
        # if x is outside our area of interest skip the point
        if x < 0 or x > 29: continue
        # if depth is zero (no data) skip the point
        if v[2] == 0: continue
        z = int(round(v[2], 1) * 10)
        # if z is outside our area of interest skip the point
        if z < 0 or z > 29: continue
        y = 17 - v[1] * 100
#         print(x, z, y)
        if y > grid[x, z]: grid[x, z] = y
#     print(grid)

    len = (grid > -999).sum()
    X = np.empty(len, dtype=np.int8)
    Y = np.empty(len, dtype=np.int16)
    Z = np.empty(len, dtype=np.int8)
    i = 0
    for x in range(30):
        for z in range(30):
            if grid[x, z] == -999: continue
            X[i] = x - 15
            Z[i] = z
            Y[i] = grid[x, z]
            i += 1
    print(X, Y, Z)

    # setup the figure and axes
    fig = plt.figure()
    ax1 = fig.add_subplot(111, projection='3d')

    bottom = np.zeros_like(X)
    width = depth = 1

    ax1.bar3d(X, Z, bottom, width, depth, Y, shade=True)
#     ax1.set_title('Shaded')

    plt.show()


#     dt = time.time() - now
#     now = time.time()

#     cv2.setWindowTitle(
#         state.WIN_NAME, "RealSense (%dx%d) %dFPS (%.2fms) %s" %
#         (w, h, 1.0/dt, dt*1000, "PAUSED" if state.paused else ""))
#
#     cv2.imshow(state.WIN_NAME, out)

    break

# Stop streaming
pipeline.stop()
