"""
User POSIX IPC (shared memory) to share images with another script
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd
from numba import jit
import logging
import redis
import sys

from shm.writer import SharedMemoryFrameWriter

logging.basicConfig(
#     filename='test.log',
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%H:%M:%S'
)

print(__file__)

CAMHEIGHT = 62  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 60
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 8   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50
ZTHRESHOLD = 2

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

OUT_GRID = 0
OUT_COLORMAP = 1

# App state object class
class AppState:
    def __init__(self, *args, **kwargs):
        self.img_src = 'rgb'
        self.record = 0
        self.laser = 2

state = AppState()

##### Vision #####

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 15)

# Start streaming
profile = pipeline.start(config)

device = profile.get_device()
# depth_sensor = device.query_sensors()[0]
depth_sensor = profile.get_device().first_depth_sensor()
depth_sensor.set_option(rs.option.laser_power, state.laser * 90.0)

# Processing blocks
pc = rs.pointcloud()

grid = np.empty((GRIDX, GRIDZ, 2), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 7), dtype=np.uint16)
sums = np.empty((GRIDX, GRIDZ), dtype=np.uint32)
dist = np.empty((GRIDX, GRIDZ, 128), dtype=np.uint8)

@jit(nopython=True)
def verts2accum(vs, a, s, d):
    a.fill(0)
    s.fill(0)
    d.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 25.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]

        z = int((z0 * 0.866 - y0 * 0.500) * 25.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * 0.500 + y0 * 0.866) * 100) + GROUNDY
#         print(x, z, y)
        if y > 127: continue
        if y < 1: y = 1
        # Add y to sum of previous y
        a[x, z, 0] += 1
        s[x, z] += y
        # Record lowest y
        if a[x, z, 1] == 0 or y < a[x, z, 1]: a[x, z, 1] = y
        # Record highest y
        if y > a[x, z, 3]: a[x, z, 3] = y
        # Inc dist counter
        if d[x, z, y] < 255: d[x, z, y] += 1

    #After all the points are tallied we update the other columns in accum
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if a[x, z, 0] == 0: continue
            a[x, z, 2] = int(s[x, z] / a[x, z, 0] + 0.5)
            min = 0
            mode = 0
            mode_cnt = 0
            max = 0
            for i in range(128):
                if d[x, z, i] > 1:
                    if min == 0: min = i
                    max = i
                    if d[x, z, i] > mode_cnt:
                        mode = i
                        mode_cnt = d[x, z, i]
            a[x, z, 4] = min
            a[x, z, 5] = mode
            a[x, z, 6] = max

@jit(nopython=True)
def accum2grid(a, g):
    g.fill(0)
    for x in range(GRIDX):
        for z in range(GRIDZ):
            mode = a[x, z, 5]
            if mode == 0: continue
            low = a[x, z, 4]
            high = a[x, z, 6]
            if mode - low <= ZTHRESHOLD and high - mode <= ZTHRESHOLD:
                g[x, z, 0] = 1
            elif mode - low > ZTHRESHOLD and high - mode > ZTHRESHOLD:
                g[x, z, 0] = 3
            else:
                g[x, z, 0] = 2
            g[x, z, 1] = mode

@jit(nopython=True)
def gridFlow(g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != GRID_HORIZ: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = GROUNDY
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == GRID_NAVIGABLE:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0:
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = GRID_NAVIGABLE
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] != GRID_NAVIGABLE and g[x - 1, z, 0] == GRID_NAVIGABLE and g[x + 1, z, 0] == GRID_NAVIGABLE and g[x, z - 1, 0] == GRID_NAVIGABLE and g[x, z + 1, 0] == GRID_NAVIGABLE:
                g[x, z, 0] = GRID_NAVIGABLE

@jit(nopython=True)
def obstaclePadding(g):
    padding = (
        # gdgt: 44 cm wide
        (-1, 6), (0, 6), (1, 6),
        (-3, 5), (-2, 5), (-1, 5), (0, 5), (1, 5), (2, 5), (3, 5),
        (-4, 4), (-3, 4), (-2, 4), (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4),
        (-5, 3), (-4, 3), (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3),
        (-5, 2), (-4, 2), (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        (-6, 1), (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1),
        (-6, 0), (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
        (-6, -1), (-5, -1), (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1),
        (-5, -2), (-4, -2), (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2), (4, -2), (5, -2),
        (-5, -3), (-4, -3), (-3, -3), (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3), (3, -3), (4, -3), (5, -3),
        (-4, -4), (-3, -4), (-2, -4), (-1, -4), (0, -4), (1, -4), (2, -4), (3, -4), (4, -4),
        (-3, -5), (-2, -5), (-1, -5), (0, -5), (1, -5), (2, -5), (3, -5),
        (-1, -6), (0, -6), (1, -6),
    )
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] == GRID_HORIZ or g[x, z, 0] == GRID_TRANS or g[x, z, 0] == GRID_VERT:
                for p in padding:
                    if x + p[0] < 0 or x + p[0] >= GRIDX or z + p[1] < 0 or z + p[1] >= GRIDZ: continue
                    if g[x + p[0], z + p[1], 0] == GRID_NAVIGABLE:
                        g[x + p[0], z + p[1], 0] = GRID_PADDING

@jit(nopython=True)
def findPath2(g):
    dead_end = False
    first_obst = 0
    x = GRIDXHALF
    heading = 0
    heading_lock = False
    for z in range(GRIDZ):
#         print(x, z, g[x, z, 0])
        if g[x, z, 0] != GRID_NAVIGABLE:
            if first_obst == 0: first_obst = z
            new_x = -1
            for i in range(1, 10):
                if x - i >= 0 and g[x - i, z, 0] == GRID_NAVIGABLE:
                    new_x = x - i
                    break
                elif x + i < GRIDX and g[x + i, z, 0] == GRID_NAVIGABLE:
                    new_x = x + i
                    break
            if new_x == -1:
                dead_end = True
                break
            else:
                x = new_x
        g[x, z, 0] = GRID_PATH
        if z == 0: continue
        h = float(x - GRIDXHALF) / z
        if not heading_lock:
            if heading == 0:
                heading = h
            elif heading > 0:
                if h > heading: heading = h
                else: heading_lock = True
            elif heading < 0:
                if h < heading: heading = h
                else: heading_lock = True

    return (first_obst, int(math.degrees(math.atan(heading))), dead_end)

@jit(nopython=True)
def grid2out(grid, out):
    out.fill(0)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == GRID_NO_DATA: continue
            out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]

if __name__ == '__main__':

	##### SETUP #####
    try:
        red = redis.Redis(unix_socket_path='/tmp/redis.sock', decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        sys.exit('Is the redis server running?')

    # Set the redis keys to their default values.
    if red.get('img_src') == None: red.set('img_src', state.img_src)
    if red.get('record') == None: red.set('record', state.record)
    if red.get('laser') == None: red.set('laser', state.laser)

    state.img_src = red.get('img_src')
    state.record = red.get('record')
    state.laser = int(red.get('laser'))

    print("Initial settings:")
    vd(state)

    shm_w = None

    try:

        shm_w = SharedMemoryFrameWriter()
        print(f'shm_name: {shm_w.shm_name}')
        red.set('shm_name', shm_w.shm_name)

        while True:

            now = time.time()

            state.img_src = red.get('img_src')
            state.record = red.get('record')
            state.laser = int(red.get('laser'))

            while True:
                frames = pipeline.wait_for_frames()

                depth_frame = frames.get_depth_frame()
                if state.img_src == 'rgb': color_frame = frames.get_color_frame()
                if depth_frame and (state.img_src != 'rgb' or color_frame):
                    break

            points = pc.calculate(depth_frame)

            # Pointcloud data to array
            verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz
            print(verts.shape.size)
            continue;

            verts2accum(verts, accum, sums, dist)

            accum2grid(accum, grid)

            gridFlow(grid)

            obstaclePadding(grid)

            first_obst, heading, dead_end = findPath2(grid)

            dt = time.time() - now

            print(f"{int(1.0/dt)} FPS Obst: {first_obst} Head: {heading} Deadend: {dead_end}")

            if state.img_src == 'rgb':
                color_image = np.asanyarray(color_frame.get_data())
                shm_w.add(color_image)
            elif state.img_src == 'depth':
                depth_image = np.asanyarray(depth_frame.get_data())
                depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
                shm_w.add(depth_colormap)
            else:
                grid2out(grid, out)
                out2 = cv2.resize(np.rot90(out), (480, 480), interpolation = cv2.INTER_NEAREST)
                out3 = np.hstack((np.zeros((480, 80, 3), dtype=np.uint8), out2, np.zeros((480, 80, 3), dtype=np.uint8)))
                shm_w.add(out3)

#             if state.record:
#                 if state.img_src != 'rgb':
#                     color_image = np.asanyarray(color_frame.get_data())
#                 if state.img_src != 'depth':
#                     depth_image = np.asanyarray(depth_frame.get_data())
#                     depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)
#                 if state.img_src != 'obst':
#                     grid2out(grid, out)
#                     out2 = cv2.resize(np.rot90(out), (480, 480), interpolation = cv2.INTER_NEAREST)
#                 out2 = np.hstack(color_image, depth_colormap, out2)

            depth_sensor.set_option(rs.option.laser_power, state.laser * 90.0)

    except KeyboardInterrupt:
        pass

    if shm_w: shm_w.release()
