# License: Apache 2.0. See LICENSE file in root directory.
# Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

"""
OpenCV and Numpy Point cloud Software Renderer

This sample is mostly for demonstration and educational purposes.
It really doesn't offer the quality or performance that can be
achieved with hardware acceleration.

Usage:
------
Keyboard:
    [f]     Fill holes
    [p]     Pause
    [d]     Cycle through decimation values
    [s]     Save PNG (./out.png)
    [q\ESC] Quit
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd
from numba import jit
import os
import random

print(__file__)

CAMHEIGHT = 61  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 60
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 4   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50
ZTHRESHOLD = 2

METHOD_POINTS = 0
METHOD_MAX = 1
METHOD_MEAN = 2
METHOD_MEDIAN = 3
METHOD_CLASSIFY = 4
METHOD_FLOW = 5
METHOD_PAD = 6
METHOD_PATH = 7
METHOD_NAMES = (
    "points",
    "max",
    "mean",
    "median",
    "classify",
    "classify + flow",
    "classify + flow + padding",
    "classify + flow + padding + path",
)

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

OUT_GRID = 0
OUT_COLORMAP = 1

# App state object class
class AppState:

    def __init__(self, *args, **kwargs):
        self.WIN_NAME = "win1"
        self.method = METHOD_PATH # METHOD_POINTS
        self.out = 0

    def reset(self):
        self.distance = 0, 0, 2

state = AppState()
print("Initial settings:")
vd(state)

def mouse_cb(event, x, y, flags, param):
#     print(event, x, y, flags, param)

    if event == cv2.EVENT_LBUTTONDOWN:
#         print("EVENT_LBUTTONDOWN")
        cv2.setWindowTitle(state.WIN_NAME, "%s T: %d X: %d Z: %d" % (METHOD_NAMES[state.method], ZTHRESHOLD, int(x / 480 * 60), 59 - int(y / 480 * 60)))

#     if event == cv2.EVENT_LBUTTONUP:
#         print("EVENT_LBUTTONUP")
#
#     if event == cv2.EVENT_RBUTTONDOWN:
#         print("EVENT_RBUTTONDOWN")
#
#     if event == cv2.EVENT_RBUTTONUP:
#         print("EVENT_RBUTTONUP")
#
#     if event == cv2.EVENT_MBUTTONDOWN:
#         print("EVENT_MBUTTONDOWN")
#
#     if event == cv2.EVENT_MBUTTONUP:
#         print("EVENT_MBUTTONUP")

cv2.namedWindow(state.WIN_NAME, cv2.WINDOW_AUTOSIZE)
cv2.resizeWindow(state.WIN_NAME, (480, 480))
cv2.setMouseCallback(state.WIN_NAME, mouse_cb)

grid = np.empty((GRIDX, GRIDZ, 2), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 256), dtype=np.uint32)

@jit(nopython=True)
def verts2accum(vs, a):
    a.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 25.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]

        z = int((z0 * 0.866 - y0 * 0.500) * 25.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * 0.500 + y0 * 0.866) * 100) + GROUNDY
        if y < 1: y = 1
        elif y > 255: y = 255
        # Add y to sum of previous y
        a[x, z, 0] += 1
        a[x, z, 1] += y
        # Record highest y
        if y > a[x, z, 2]: a[x, z, 2] = y
        # Add y to list for grid cell
        n = a[x, z, 3]
        if n < 252:
            n += 1
            a[x, z, n + 3] = y
            a[x, z, 3] = n
        else:
            a[x, z, random.randint(4, 255)] = y

# @jit(nopython=True)
def accum2grid(a, g, m):
    g.fill(0)
    if m >= METHOD_CLASSIFY: # classify
        fp = open("classify.txt", "w")
        fp.write("x\tz\tlow\tmedian\thigh\t0 cnt\t1 sum\tmean\t2 max\t3 cnt")
        for i in range(4, 256): fp.write("\t%d" % (i))
        fp.write("\n")
    for x in range(GRIDX):
        for z in range(GRIDZ):
            # print(a[x, z])
            if m == METHOD_MAX: # max
                g[x, z, 0] = a[x, z, 2]
            elif m == METHOD_MEAN: # mean
                if a[x, z, 0] > 0:
                    g[x, z, 0] = int(a[x, z, 1] / a[x, z, 0] + 0.5)
            elif m == METHOD_MEDIAN: # median
                n = a[x, z, 3]
                if n == 0: continue
                elif n == 1: g[x, z, 0] = a[x, z, 4]
                elif n == 2: g[x, z, 0] = int((a[x, z, 4] + a[x, z, 5]) / 2 + 0.5)
                else:
                    a[x, z, 4:n + 4].sort()
                    if n % 2 == 0:
                        g[x, z, 0] = int((a[x, z, (n >> 1) + 3] + a[x, z, (n >> 1) + 4]) / 2 + 0.5)
                    else:
                        g[x, z, 0] = a[x, z, ((n + 1) >> 1) + 3]
            elif m >= METHOD_CLASSIFY: # classify
                n = a[x, z, 3]
                if n < 3: continue
                a[x, z, 4:n + 4].sort()
                if n == 3:
                    median = a[x, z, 5]
                    low = a[x, z, 4]
                    high = a[x, z, 6]
                    avg = 0
                else:
                    if n % 2 == 0:
                        median = int((a[x, z, (n >> 1) + 3] + a[x, z, (n >> 1) + 4]) / 2 + 0.5)
                    else:
                        median = a[x, z, ((n + 1) >> 1) + 3]
                    low = a[x, z, (n >> 3) + 5]
                    high = a[x, z, n + 2 - (n >> 3)]
                    if a[x, z, 1] > 0: avg = int(a[x, z, 1] / a[x, z, 0] + 0.5)
                    else: avg = 0
                fp.write("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d" % (x, z, low, median, high, a[x, z, 0], a[x, z, 1], avg, a[x, z, 2], a[x, z, 3]))
                for i in range(4, n + 4): fp.write("\t%d" % (a[x, z, i]))
                fp.write("\n")
                if median - low <= ZTHRESHOLD and high - median <= ZTHRESHOLD:
                    g[x, z, 0] = 1
                elif median - low > ZTHRESHOLD and high - median > ZTHRESHOLD:
                    g[x, z, 0] = 3
                else:
                    g[x, z, 0] = 2
                g[x, z, 1] = median
            elif m == METHOD_POINTS: # points
                if a[x, z, 0] == 0: continue
                g[x, z, 0] = int(math.log(a[x, z, 0], 4) * 32)
    if m >= METHOD_CLASSIFY: # classify
        fp.close()

# @jit(nopython=True)
def gridFlow(g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != GRID_HORIZ: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = GROUNDY
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == GRID_NAVIGABLE:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0:
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = GRID_NAVIGABLE
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] != GRID_NAVIGABLE and g[x - 1, z, 0] == GRID_NAVIGABLE and g[x + 1, z, 0] == GRID_NAVIGABLE and g[x, z - 1, 0] == GRID_NAVIGABLE and g[x, z + 1, 0] == GRID_NAVIGABLE:
                g[x, z, 0] = GRID_NAVIGABLE

# @jit(nopython=True)
def obstaclePadding(g):
    padding = (
        # gdgt: 44 cm wide
        # hall: 100 cm wide
        (-1, 6), (0, 6), (1, 6),
        (-3, 5), (-2, 5), (-1, 5), (0, 5), (1, 5), (2, 5), (3, 5),
        (-4, 4), (-3, 4), (-2, 4), (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4),
        (-5, 3), (-4, 3), (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3),
        (-5, 2), (-4, 2), (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        (-6, 1), (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1),
        (-6, 0), (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
        (-6, -1), (-5, -1), (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1),
        (-5, -2), (-4, -2), (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2), (4, -2), (5, -2),
        (-5, -3), (-4, -3), (-3, -3), (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3), (3, -3), (4, -3), (5, -3),
        (-4, -4), (-3, -4), (-2, -4), (-1, -4), (0, -4), (1, -4), (2, -4), (3, -4), (4, -4),
        (-3, -5), (-2, -5), (-1, -5), (0, -5), (1, -5), (2, -5), (3, -5),
        (-1, -6), (0, -6), (1, -6),
    )
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] == GRID_HORIZ or g[x, z, 0] == GRID_TRANS or g[x, z, 0] == GRID_VERT:
                for p in padding:
                    if x + p[0] < 0 or x + p[0] >= GRIDX or z + p[1] < 0 or z + p[1] >= GRIDZ: continue
                    if g[x + p[0], z + p[1], 0] == GRID_NAVIGABLE:
                        g[x + p[0], z + p[1], 0] = GRID_PADDING

# @jit(nopython=True)
def findPath(g):
    dead_end = False
    x = 30
    for z in range(GRIDZ):
        print(x, z, g[x, z, 0])
        if g[x, z, 0] != GRID_NAVIGABLE:
            dead_end = True
            break
        l = r = -1
        for i in range(1, GRIDX):
            if l == -1:
                print("x-i: %d = %d" % (x - i, g[x - i, z, 0] if x - i >= 0 else -1))
                if x - i < 0: l = 0
                elif g[x - i, z, 0] != GRID_NAVIGABLE: l = x - i + 1
            if r == -1:
                print("x+i: %d = %d" % (x + i, g[x + i, z, 0] if x + i < GRIDX else -1))
                if x + i == GRIDX: r = GRIDX - 1
                elif g[x + i, z, 0] != GRID_NAVIGABLE: r = x + i - 1
            if l > -1 and r > -1: break
        x = (l + r) >> 1
        print("l: %d r: %d New X: %d" % (l, r, x))
        g[x, z, 0] = GRID_PATH

# @jit(nopython=True)
def grid2out(grid, out, m):
    out.fill(0)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == GRID_NO_DATA: continue
            elif m >= METHOD_CLASSIFY: # classify
                out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]
            elif m == METHOD_POINTS: # points
                out[x, z] = (grid[x, z, 0] * 1, grid[x, z, 0] * 1, grid[x, z, 0] * 1)
            else:
                s = 0
                n = 0
                for i in range(-1, 2):
                    if x + i >= 0 and x + i < GRIDX and grid[x + i, z - 1, 0] > 0:
                        s += grid[x + i, z - 1, 0]
                        n += 1
                if n == 0: a = GROUNDY
                else: a = int(s / n + 0.5)
                if abs(grid[x, z, 0] - a) < 2:
                    out[x, z] = (255, 255, 255)
                else:
                    out[x, z] = (0, 0, 255)

def printGrid(grid):
    for z in range(GRIDZ - 1, -1, -1):
        for x in range(GRIDX):
            if grid[x, z] == -999:
                print("     ", end='')
            else:
                print("%5d" % (grid[x, z]), end='')
        print('')
    print('')

# verts = np.load('verts.npy')
#         print("verts shape", verts.shape)

#         minX = verts[:, 0].min()
#         maxX = verts[:, 0].max()
#         print("X %f %f" % (minX, maxX))
#         minY = verts[:, 1].min()
#         maxY = verts[:, 1].max()
#         print("Y %f %f" % (minY, maxY))
#         minZ = verts[:, 2].min()
#         maxZ = verts[:, 2].max()
#         print("Z %f %f" % (minZ, maxZ))
#         quit()

#         xSize = int(round(max(abs(minX), maxX) * 10, 0)) * 2
#         print("xSize %d" % (xSize))
#         zSize = int(round(maxZ * 10, 0)) * 2
#         print("zSize %d" % (zSize))

# dt = time.time() - now2
# print("get_vertices %.2fms" % (dt*1000))
# now2 = time.time()

# verts2accum(verts, accum)

while(True):

    verts = np.load('verts hall.npy')
    verts2accum(verts, accum)
    #         printGrid(grid)

    # dt = time.time() - now2
    # print("verts2accum %.2fms" % (dt*1000))
    # now2 = time.time()

    accum2grid(accum, grid, state.method)
    if state.method >= METHOD_FLOW:
        gridFlow(grid)
        if state.method >= METHOD_PAD:
            obstaclePadding(grid)
            if state.method >= METHOD_PATH:
                findPath(grid)

    # dt = time.time() - now2
    # print("accum2grid %.2fms" % (dt*1000))
    # now2 = time.time()

    # fillHoles(grid)

    # dt = time.time() - now2
    # print("3 %.2fms" % (dt*1000))
    # now2 = time.time()

    if state.out == OUT_GRID:
        grid2out(grid, out, state.method)
    else:
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        out = cv2.applyColorMap(grid[::0], cv2.COLORMAP_BONE)

    # dt = time.time() - now2
    # print("grid2out %.2fms" % (dt*1000))
    # now2 = time.time()

    out2 = cv2.resize(np.rot90(out), (480, 480), interpolation = cv2.INTER_NEAREST)

    # dt = time.time() - now2
    # print("out2 resize %.2fms" % (dt*1000))
    # now2 = time.time()

    cv2.imshow(state.WIN_NAME, out2)

    cv2.setWindowTitle(state.WIN_NAME, "%s T: %d" % (METHOD_NAMES[state.method], ZTHRESHOLD))

    key = cv2.waitKey(0)

    if key == ord("m"):
        state.method = (state.method + 1) % 8

    if key == ord("o"):
        state.out = (state.out + 1) % 2

    if key == ord("t"):
        ZTHRESHOLD = (ZTHRESHOLD + 1) % 6
        if ZTHRESHOLD == 0: ZTHRESHOLD = 1

    if key == ord("w"):
        cv2.imwrite('./out.png', out2)

    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break
