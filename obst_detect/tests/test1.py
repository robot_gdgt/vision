# First import the library
import pyrealsense2 as rs
# Import Numpy for easy array manipulation
import numpy as np

# Create a pipeline
pipeline = rs.pipeline()

#Create a config and configure the pipeline to stream
#  different resolutions of color and depth streams
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 6)

# Start streaming
profile = pipeline.start(config)

# Getting the depth sensor's depth scale (see rs-align example for explanation)
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()
print("Depth Scale is: " , depth_scale)

# Streaming loop
try:
    while True:
        # Get frameset of color and depth
        frames = pipeline.wait_for_frames()
        # frames.get_depth_frame() is a 640x360 depth image
        depth = frames.get_depth_frame()

        # Validate that both frames are valid
        if not depth:
            continue

        dist = depth.get_distance(320, 240)
        if dist:
            print(dist / depth_scale)

finally:
    pipeline.stop()
