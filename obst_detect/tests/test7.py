# License: Apache 2.0. See LICENSE file in root directory.
# Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

"""
OpenCV and Numpy Point cloud Software Renderer

This sample is mostly for demonstration and educational purposes.
It really doesn't offer the quality or performance that can be
achieved with hardware acceleration.

Usage:
------
Keyboard:
    [f]     Fill holes
    [p]     Pause
    [d]     Cycle through decimation values
    [s]     Save PNG (./out.png)
    [q\ESC] Quit
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd
from numba import jit
import os
import random

print(__file__)

# App state object class
class AppState:

    def __init__(self, *args, **kwargs):
        self.WIN_NAME = "win1"
        self.method = 0
        self.method_text = 'max'
        self.out = 0

    def reset(self):
        self.distance = 0, 0, 2

state = AppState()
print("Initial settings:")
vd(state)

CAMHEIGHT = 61  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 80
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 4   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50
ZTHRESHOLD = 2

cv2.namedWindow(state.WIN_NAME, cv2.WINDOW_AUTOSIZE)
cv2.resizeWindow(state.WIN_NAME, (480, 480))

grid = np.empty((GRIDX, GRIDZ, 2), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 256), dtype=np.uint32)

@jit(nopython=True)
def verts2accum(vs, a):
    a.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 25.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]

        z = int((z0 * 0.866 - y0 * 0.500) * 25.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * 0.500 + y0 * 0.866) * 100) + GROUNDY
        if y < 1: y = 1
        elif y > 255: y = 255
        # Add y to sum of previous y
        a[x, z, 0] += 1
        a[x, z, 1] += y
        # Record highest y
        if y > a[x, z, 2]: a[x, z, 2] = y
        # Add y to list for grid cell
        n = a[x, z, 3]
        if n < 252:
            n += 1
            a[x, z, n + 3] = y
            a[x, z, 3] = n
        else:
            a[x, z, random.randint(4, 255)] = y

# @jit(nopython=True)
def accum2grid(a, g, m):
    g.fill(0)
    if m == 3 or m == 4: # classify
        fp = open("classify.txt", "w")
        fp.write("x\tz\tlow\tmedian\thigh\t0 cnt\t1 sum\tmean\t2 max\t3 cnt")
        for i in range(4, 256): fp.write("\t%d" % (i))
        fp.write("\n")
    for x in range(GRIDX):
        for z in range(GRIDZ):
            # print(a[x, z])
            if m == 0: # max
                g[x, z, 0] = a[x, z, 2]
            elif m == 1: # mean
                if a[x, z, 0] > 0:
                    g[x, z, 0] = int(a[x, z, 1] / a[x, z, 0] + 0.5)
            elif m == 2: # median
                n = a[x, z, 3]
                if n == 0: continue
                elif n == 1: g[x, z, 0] = a[x, z, 4]
                elif n == 2: g[x, z, 0] = int((a[x, z, 4] + a[x, z, 5]) / 2 + 0.5)
                else:
                    a[x, z, 4:n + 4].sort()
                    if n % 2 == 0:
                        g[x, z, 0] = int((a[x, z, (n >> 1) + 3] + a[x, z, (n >> 1) + 4]) / 2 + 0.5)
                    else:
                        g[x, z, 0] = a[x, z, ((n + 1) >> 1) + 3]
            elif m == 3 or m == 4: # classify
                n = a[x, z, 3]
                if n < 3: continue
                a[x, z, 4:n + 4].sort()
                if n == 3:
                    median = a[x, z, 5]
                    low = a[x, z, 4]
                    high = a[x, z, 6]
                else:
                    if n % 2 == 0:
                        median = int((a[x, z, (n >> 1) + 3] + a[x, z, (n >> 1) + 4]) / 2 + 0.5)
                    else:
                        median = a[x, z, ((n + 1) >> 1) + 3]
                    low = a[x, z, 5]
                    high = a[x, z, n + 2]
                    if a[x, z, 1] > 0: avg = int(a[x, z, 1] / a[x, z, 0] + 0.5)
                    else: avg = 0
                fp.write("%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d\t%d" % (x, z, low, median, high, a[x, z, 0], a[x, z, 1], avg, a[x, z, 2], a[x, z, 3]))
                for i in range(4, n + 4): fp.write("\t%d" % (a[x, z, i]))
                fp.write("\n")
                if median - low < ZTHRESHOLD and high - median < ZTHRESHOLD:
                    g[x, z, 0] = 1
                elif median - low > ZTHRESHOLD and high - median > ZTHRESHOLD:
                    g[x, z, 0] = 3
                else:
                    g[x, z, 0] = 2
                g[x, z, 1] = median
            elif m == 5: # points
                if a[x, z, 0] == 0: continue
                # if a[x, z, 0] < 10: g[x, z, 0] = 50
                # elif a[x, z, 0] < 50: g[x, z, 0] = 100
                # elif a[x, z, 0] < 250: g[x, z, 0] = 150
                # elif a[x, z, 0] < 1250: g[x, z, 0] = 200
                # else: g[x, z, 0] = 250
                g[x, z, 0] = int(math.log(a[x, z, 0], 4) * 32)
    if m == 3 or m == 4: # classify
        fp.close()

def gridFlow(a, g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != 1: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = GROUNDY
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == 4:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0: 
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = 4

# @jit(nopython=True)
def grid2out(grid, out, m):
    out.fill(0)
    mp = 0
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (64, 64, 64)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == 0: continue
            elif m == 3 or m == 4: # classify
                out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]
            elif m == 5: # points
                out[x, z] = (grid[x, z, 0] * 1, grid[x, z, 0] * 1, grid[x, z, 0] * 1)
            else:
                s = 0
                n = 0
                for i in range(-1, 2):
                    if x + i >= 0 and x + i < GRIDX and grid[x + i, z - 1, 0] > 0:
                        s += grid[x + i, z - 1, 0]
                        n += 1
                if n == 0: a = GROUNDY
                else: a = int(s / n + 0.5)
                if abs(grid[x, z, 0] - a) < 2:
                    out[x, z] = (255, 255, 255)
                else:
                    out[x, z] = (0, 0, 255)

def printGrid(grid):
    for z in range(GRIDZ - 1, -1, -1):
        for x in range(GRIDX):
            if grid[x, z] == -999:
                print("     ", end='')
            else:
                print("%5d" % (grid[x, z]), end='')
        print('')
    print('')

# verts = np.load('verts.npy')
#         print("verts shape", verts.shape)

#         minX = verts[:, 0].min()
#         maxX = verts[:, 0].max()
#         print("X %f %f" % (minX, maxX))
#         minY = verts[:, 1].min()
#         maxY = verts[:, 1].max()
#         print("Y %f %f" % (minY, maxY))
#         minZ = verts[:, 2].min()
#         maxZ = verts[:, 2].max()
#         print("Z %f %f" % (minZ, maxZ))
#         quit()

#         xSize = int(round(max(abs(minX), maxX) * 10, 0)) * 2
#         print("xSize %d" % (xSize))
#         zSize = int(round(maxZ * 10, 0)) * 2
#         print("zSize %d" % (zSize))

# dt = time.time() - now2
# print("get_vertices %.2fms" % (dt*1000))
# now2 = time.time()

# verts2accum(verts, accum)

while(True):

    verts = np.load('verts stairs.npy')
    verts2accum(verts, accum)
    #         printGrid(grid)

    # dt = time.time() - now2
    # print("verts2accum %.2fms" % (dt*1000))
    # now2 = time.time()

    accum2grid(accum, grid, state.method)
    if state.method == 4: gridFlow(accum, grid)

    # dt = time.time() - now2
    # print("accum2grid %.2fms" % (dt*1000))
    # now2 = time.time()

    # fillHoles(grid)

    # dt = time.time() - now2
    # print("3 %.2fms" % (dt*1000))
    # now2 = time.time()

    if state.out == 0:
        grid2out(grid, out, state.method)
    else:
        # Apply colormap on depth image (image must be converted to 8-bit per pixel first)
        out = cv2.applyColorMap(grid[::0], cv2.COLORMAP_BONE)

    # dt = time.time() - now2
    # print("grid2out %.2fms" % (dt*1000))
    # now2 = time.time()

    out2 = cv2.resize(np.rot90(out), (out.shape[1] * int(480 / out.shape[0]), 480), interpolation = cv2.INTER_NEAREST)

    # dt = time.time() - now2
    # print("out2 resize %.2fms" % (dt*1000))
    # now2 = time.time()

    cv2.imshow(state.WIN_NAME, out2)

    cv2.setWindowTitle(state.WIN_NAME, state.method_text)

    key = cv2.waitKey(0)

    if key == ord("m"):
        state.method = (state.method + 1) % 6
        if state.method == 0: state.method_text = 'max'
        elif state.method == 1: state.method_text = 'mean'
        elif state.method == 2: state.method_text = 'median'
        elif state.method == 3: state.method_text = 'classify'
        elif state.method == 4: state.method_text = 'classify + flow'
        else: state.method_text = 'points'

    if key == ord("o"):
        state.out = (state.out + 1) % 2

    if key == ord("t"):
        ZTHRESHOLD = (ZTHRESHOLD + 1) % 6
        if ZTHRESHOLD == 0: ZTHRESHOLD = 1
        print("ZTHRESHOLD = %d" % (ZTHRESHOLD))

    if key == ord("w"):
        cv2.imwrite('./out.png', out2)

    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break
