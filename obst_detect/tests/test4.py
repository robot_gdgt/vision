# License: Apache 2.0. See LICENSE file in root directory.
# Copyright(c) 2015-2017 Intel Corporation. All Rights Reserved.

"""
OpenCV and Numpy Point cloud Software Renderer

This sample is mostly for demonstration and educational purposes.
It really doesn't offer the quality or performance that can be
achieved with hardware acceleration.

Usage:
------
Keyboard:
    [p]     Pause
    [d]     Cycle through decimation values
    [s]     Save PNG (./out.png)
    [q\ESC] Quit
"""

import math
import time
import cv2
import numpy as np
import pyrealsense2 as rs
from var_dump import var_dump as vd
from numba import jit

print(__file__)

class AppState:

    def __init__(self, *args, **kwargs):
        self.WIN_NAME = "win1"
        self.distance = 2
        self.decimate = 1
        self.scale = True
        self.color = True
        self.paused = False

    def reset(self):
        self.distance = 0, 0, 2

state = AppState()
vd(state)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)

# Start streaming
profile = pipeline.start(config)

# Getting the depth sensor's depth scale (see rs-align example for explanation)
depth_sensor = profile.get_device().first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale()
print("Depth Scale is: " , depth_scale)

# Get stream profile and camera intrinsics
# profile = pipeline.get_active_profile()
depth_profile = rs.video_stream_profile(profile.get_stream(rs.stream.depth))
depth_intrinsics = depth_profile.get_intrinsics()
w, h = depth_intrinsics.width, depth_intrinsics.height
print("w: %d h: %d" % (w, h))
# vd(("w", w, "h", h))
# fov = rs.rs2_fov(depth_intrinsics)
# vd(fov)

# Processing blocks
pc = rs.pointcloud()
decimate = rs.decimation_filter()
decimate.set_option(rs.option.filter_magnitude, 2 ** state.decimate)

GRIDX = 40
GRIDXHALF = 20
GRIDZ = 40
GRIDSCALE = 20

cv2.namedWindow(state.WIN_NAME, cv2.WINDOW_AUTOSIZE)
cv2.resizeWindow(state.WIN_NAME, GRIDX * GRIDSCALE, GRIDZ * GRIDSCALE)

grid = np.empty((GRIDX, GRIDZ), dtype=np.int16)
out = np.empty((GRIDX, GRIDZ, 3), np.uint8)

# @jit(nopython=True, nogil=True)
@jit(nopython=True)
def verts2grid(vs, g):
    g.fill(-999)

    for i in range(vs.shape[0]):
        v = vs[i]

        x = int(v[0] * 20 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue
        z = int(v[2] * 20 + 0.5)
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = int(17 - v[1] * 100)
        if y > g[x, z]: g[x, z] = y

now = time.time()
frameN = 0

while True:
    if not state.paused:
        now2 = time.time()
        # Grab camera data
        frames = pipeline.wait_for_frames()

        depth_frame = frames.get_depth_frame()
        if not depth_frame:
            continue
        depth_frame = decimate.process(depth_frame)

        points = pc.calculate(depth_frame)

        # Pointcloud data to array
        verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz
#         print("verts shape", verts.shape)

#         minX = verts[:, 0].min()
#         maxX = verts[:, 0].max()
#         print("X %f %f" % (minX, maxX))
#         minY = verts[:, 1].min()
#         maxY = verts[:, 1].max()
#         print("Y %f %f" % (minY, maxY))
#         minZ = verts[:, 2].min()
#         maxZ = verts[:, 2].max()
#         print("Z %f %f" % (minZ, maxZ))
#
#         xSize = int(round(max(abs(minX), maxX) * 10, 0)) * 2
#         print("xSize %d" % (xSize))
#         zSize = int(round(maxZ * 10, 0)) * 2
#         print("zSize %d" % (zSize))
        dt = time.time() - now2
        now2 = time.time()
#         print("\n1 %.2fms" % (dt*1000));

        verts2grid(verts, grid)
#         print(grid)

        dt = time.time() - now2
        now2 = time.time()
#         print("3 %.2fms" % (dt*1000));

        for x in range(GRIDX):
            for z in range(GRIDZ):
                if grid[x, z] == -999:
                    out[x, z] = (0, 0, 0)
                elif grid[x, z] < 5:
                    out[x, z] = (255, 255, 255)
                else:
                    out[x, z] = (0, 0, 255)

        dt = time.time() - now2
        now2 = time.time()
#         print("4 %.2fms" % (dt*1000));

        out2 = cv2.resize(np.rot90(out), (out.shape[1] * GRIDSCALE, out.shape[0] * GRIDSCALE), interpolation = cv2.INTER_NEAREST)

        cv2.imshow(state.WIN_NAME, out2)
        cv2.imwrite("frames/out%06d.png" % (frameN), out)
        frameN += 1

        dt = time.time() - now

        cv2.setWindowTitle(
            state.WIN_NAME, "%d FPS (%.2fms) D: %d P: %d %s" %
            (1.0/dt, dt*1000, state.decimate, 640 * 480 / (4 ** state.decimate), " - PAUSED" if state.paused else ""))

    now = time.time()

    key = cv2.waitKey(1)

    if key == ord("p"):
        state.paused ^= True

    if key == ord("d"):
        state.decimate = (state.decimate + 1) % 4
#         if state.decimate == 0: state.decimate = 1
        decimate.set_option(rs.option.filter_magnitude, 2 ** state.decimate)

    if key == ord("s"):
        cv2.imwrite('./out.png', out)

    # Press esc or 'q' to close the image window
    if key & 0xFF == ord('q') or key == 27:
        cv2.destroyAllWindows()
        break

# Stop streaming
pipeline.stop()
