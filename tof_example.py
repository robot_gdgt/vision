import serial # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
from struct import unpack
from time import sleep

FRAME_HEAD = b"\x00\xFF"
FRAME_TAIL = b"\xDD"
FRAME_TAIL_INT = 0xDD

uart_A = serial.Serial('/dev/ttyUSB0', 115200, 8, 'N', 1, timeout=0.1)
print(f'usb.is_open: {uart_A.is_open}')

def uart_readBytes():
    # return uart_A.read(4096)
    return uart_A.read_until(FRAME_TAIL)

def uart_hasData():
    return uart_A.in_waiting

def uart_sendCmd(cmd):
    uart_A.write(cmd)

# send_cmd("AT+BINN=2\r")
# uart_sendCmd(b"AT+DISP=5\r")
# uart_sendCmd(b"AT+FPS=10\r")

# uart_sendCmd(b"AT+BAUD=2\r")
# print(uart_readBytes())
# sleep(0.1)
uart_sendCmd(b"AT+ISP=0\r")
print(uart_readBytes())
sleep(0.1)
uart_sendCmd(b"AT+BINN=2\r")
print(uart_readBytes())
sleep(0.1)
uart_sendCmd(b"AT+UNIT=9\r")
print(uart_readBytes())
sleep(0.1)
uart_sendCmd(b"AT+FPS=3\r")
print(uart_readBytes())
sleep(0.1)
uart_sendCmd(b"AT+DISP=7\r")
print(uart_readBytes())
sleep(0.1)
uart_sendCmd(b"AT+ISP=1\r")
print(uart_readBytes())
sleep(2)

# while True:
#     if uart_hasData():
#         print(uart_readBytes())

rawData = b''
while True:
    if not uart_hasData():
        continue
    rawData += uart_readBytes()
    idx = rawData.find(FRAME_HEAD)
    if idx < 0:
        rawData = rawData[-1:]
        continue
    # rawData = rawData[idx:]
    if idx > 0:
        rawData = rawData[idx:]
    # print(rawData)
    # check data length 2Byte
    dataLen = unpack("H", rawData[2:4])[0]
    print(f"len: {dataLen}")
    frameLen = len(FRAME_HEAD) + 2 + dataLen + 2
    frameDataLen = dataLen - 16

    if len(rawData) < frameLen:
        continue
    # get data
    frameData = rawData[:frameLen]
    # print(frameData.hex())
    rawData = rawData[frameLen:]

    # frame tail
    frameTail = frameData[-1]
    print(f"tail: {hex(frameTail)}")
    if frameTail != FRAME_TAIL_INT:
        print('invalid tail')
        frameData = b''
        continue

    # checksum
    cksum = frameData[-2]
    print(f"checksum: {hex(cksum)}")
    if cksum != sum(frameData[:frameLen - 2]) % 256:
        print('checksum failed')
        frameData = b''
        continue

    frameID = unpack("H", frameData[16:18])[0]
    print(f"frame ID: {frameID}")

    resR = unpack("B", frameData[14:15])[0]
    resC = unpack("B", frameData[15:16])[0]
    res = (resR, resC)
    print(res)
    # frameData=[ unpack("H", frameData[20+i:22+i])[0] for i in range(0, frameDataLen, 2) ]
    frameData = [unpack("B", frameData[20+i:21+i])[0]
                    for i in range(0, frameDataLen, 1)]

    del frameData
