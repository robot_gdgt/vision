"""
Vision - ArUco Tag Test
"""

import numpy as np
import cv2
from time import sleep, time
import math
# import logging
import redis
# import os
from pathlib import Path
# import json

import globals
from my_rs import rs_get_frames
# import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
# from can_ids import *
from var_dump import var_dump as vd

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

# get some farmes so the auto-exposure can do its thing
for i in range(0, 5):
    color_frame = None
    # Validate that both frames are valid
    while not color_frame:
        _, color_frame = rs_get_frames()
    print(f"Got frame {i}")
    
def tick():
    color_frame = None

    while not color_frame:
        _, color_frame = rs_get_frames()
    
    color_image = np.asanyarray(color_frame.get_data())
    gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
    # if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    # return

    # cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
    # cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

    ##########################
    # Find the ArUco markers #
    ##########################

    print("[INFO] detecting ArUco markers...")
    arucoDict = cv2.aruco.Dictionary_get(cv2.aruco.DICT_4X4_50)
    arucoParams = cv2.aruco.DetectorParameters_create()
    (corners, ids, rejected) = cv2.aruco.detectMarkers(gray_image, arucoDict, parameters=arucoParams)

    print(f"[INFO] {len(corners)} total ArUco markers detected")

    # verify *at least* one ArUco marker was detected
    if len(corners) > 0:
        # flatten the ArUco IDs list
        ids = ids.flatten()
        # loop over the detected ArUco marker corners
        for (markerCorner, markerID) in zip(corners, ids):
            # extract the marker corners (which are always returned in
            # top-left, top-right, bottom-right, and bottom-left order)
            corners = markerCorner.reshape((4, 2))

            vd(corners)
            # sides = [0, 0, 0, 0]
            # sides[0] = math.sqrt((corners[0][0] - corners[1][0]) ** 2 + (corners[0][1] - corners[1][1]) ** 2)
            # sides[1] = math.sqrt((corners[1][0] - corners[2][0]) ** 2 + (corners[1][1] - corners[2][1]) ** 2)
            # sides[2] = math.sqrt((corners[2][0] - corners[3][0]) ** 2 + (corners[2][1] - corners[3][1]) ** 2)
            # sides[3] = math.sqrt((corners[3][0] - corners[0][0]) ** 2 + (corners[3][1] - corners[0][1]) ** 2)

            longestSideLen = 0
            sideLen = math.sqrt((corners[0][0] - corners[1][0]) ** 2 + (corners[0][1] - corners[1][1]) ** 2)
            if sideLen > longestSideLen:
                longestSideLen = sideLen
            sideLen = math.sqrt((corners[1][0] - corners[2][0]) ** 2 + (corners[1][1] - corners[2][1]) ** 2)
            if sideLen > longestSideLen:
                longestSideLen = sideLen
            sideLen = math.sqrt((corners[2][0] - corners[3][0]) ** 2 + (corners[2][1] - corners[3][1]) ** 2)
            if sideLen > longestSideLen:
                longestSideLen = sideLen
            sideLen = math.sqrt((corners[3][0] - corners[0][0]) ** 2 + (corners[3][1] - corners[0][1]) ** 2)
            if sideLen > longestSideLen:
                longestSideLen = sideLen
            
            # extract the bounding box (x, y)-coordinates for the ArUco marker
            # and convert each of the (x, y)-coordinate pairs to integers
            (ptA, ptB, ptC, ptD) = corners
            ptA = (int(ptA[0]), int(ptA[1]))
            ptB = (int(ptB[0]), int(ptB[1]))
            ptC = (int(ptC[0]), int(ptC[1]))
            ptD = (int(ptD[0]), int(ptD[1]))
            # draw the bounding box of the ArUco marker detection
            cv2.line(color_image, ptA, ptB, (0, 255, 0), 1)
            cv2.line(color_image, ptB, ptC, (0, 255, 0), 1)
            cv2.line(color_image, ptC, ptD, (0, 255, 0), 1)
            cv2.line(color_image, ptD, ptA, (0, 255, 0), 1)
            # draw the center (x, y)-coordinates of the ArUco marker
            cX = int((ptA[0] + ptC[0]) / 2.0)
            cY = int((ptA[1] + ptC[1]) / 2.0)
            cv2.circle(color_image, (cX, cY), 3, (0, 0, 255), -1) 
            # draw the tag family on the color_image
            # tagFamily = r.tag_family.decode("utf-8")
            cv2.putText(color_image, f"{markerID}", (ptA[0], ptA[1] - 15),
                cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 0), 2)
            print(f"tag_id={markerID}, center={cX}, {cY}, longestSideLen={longestSideLen}")

            red.set('caption', f"C: {cX},{cY} longestSideLen: {longestSideLen}")

    else:
        red.set('caption', f"C: ?, ? longestSideLen: ?")

    # show the output color_image after ArUco marker detection
    Path('/home/pi/tmp/images/rgb.lock').touch()
    cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    Path('/home/pi/tmp/images/rgb.lock').unlink()

# while(True):
#     tick()
#     sleep(2.0)

tick()
