import numpy as np

def downSample(aIn, factor):
	if factor < 2:
		return aIn

	rows, cols = aIn.shape
	rowSkip = int((rows % factor) / 2)
	colSkip = int((cols % factor) / 2)
	rows = int(rows / factor)
	cols = int(cols / factor)
	print(f'rows: {rows} cols: {cols} rowSkip: {rowSkip} colSkip: {colSkip}')

	aOut = np.zeros((rows, cols))

	for r in range(rows):
		for c in range(cols):
			region = aIn[
				r * factor + rowSkip : r * factor + factor + rowSkip, 
				c * factor + colSkip : c * factor + factor + colSkip
			]
			print(region)
			cnz = np.count_nonzero(region)
			print(cnz)
			if cnz > 0:
				sum = np.sum(region)
				print(sum)
				aOut[r, c] = sum / cnz
	return aOut

a = np.array([
	[1, 2, 3, 4, 5, 0],
	[4, 0, 6, 7, 8, 0],
	[7, 8, 9, 0, 0, 3],
	[1, 2, 0, 4, 5, 6],
	[0, 5, 6, 7, 8, 9],
	[7, 8, 9, 1, 0, 0]
])

print(a)

print(downSample(a, 1))
