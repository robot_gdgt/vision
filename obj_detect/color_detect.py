import pyrealsense2 as rs
import numpy as np
import cv2
import time
import math
from var_dump import var_dump as vd

def nothing(dummy):
    pass

cv2.namedWindow('Mask', cv2.WINDOW_AUTOSIZE)
cv2.createTrackbar('H', 'Mask', 104, 179, nothing)
cv2.createTrackbar('H ±', 'Mask', 20, 90, nothing)
cv2.createTrackbar('S', 'Mask', 255, 255, nothing)
cv2.createTrackbar('S ±', 'Mask', 128, 128, nothing)
cv2.createTrackbar('V', 'Mask', 255, 255, nothing)
cv2.createTrackbar('V ±', 'Mask', 128, 128, nothing)

cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()

        # Get aligned frames
        color_frame = frames.get_color_frame()

        # Validate that both frames are valid
        if not color_frame:
            continue

        # Convert images to numpy arrays
        color_image = np.asanyarray(color_frame.get_data())
        # color_image = cv2.blur(color_image, (5, 5))
        color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
        
        Hmin = cv2.getTrackbarPos('H', 'Mask') - cv2.getTrackbarPos('H ±', 'Mask')
        if Hmin < 0:
            Hmin = Hmin + 180
        Hmax = cv2.getTrackbarPos('H', 'Mask') + cv2.getTrackbarPos('H ±', 'Mask')
        if Hmax > 179:
            Hmax = Hmax - 180
        Smin = cv2.getTrackbarPos('S', 'Mask') - cv2.getTrackbarPos('S ±', 'Mask')
        if Smin < 0:
            Smin = 0
        Smax = cv2.getTrackbarPos('S', 'Mask') + cv2.getTrackbarPos('S ±', 'Mask')
        if Smax > 255:
            Smax = 255
        Vmin = cv2.getTrackbarPos('V', 'Mask') - cv2.getTrackbarPos('V ±', 'Mask')
        if Vmin < 0:
            Vmin = 0
        Vmax = cv2.getTrackbarPos('V', 'Mask') + cv2.getTrackbarPos('V ±', 'Mask')
        if Vmax > 255:
            Vmax = 255
    
        if Hmax > Hmin:
            minValues = np.array([Hmin, Smin, Vmin], np.uint8)
            maxValues = np.array([Hmax, Smax, Vmax], np.uint8)
            mask = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)
        else:
            minValues = np.array([0, Smin, Vmin], np.uint8)
            maxValues = np.array([Hmax, Smax, Vmax], np.uint8)
            mask1 = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)

            minValues = np.array([Hmin, Smin, Vmin], np.uint8)
            maxValues = np.array([179, Smax, Vmax], np.uint8)
            mask2 = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)

            mask = mask1 + mask2

        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        for c in contours:
            print(cv2.contourArea(c))

        tx, ty, dy, font, font_size, font_thickness = 4, 16, 19, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1
        text = f"H: {Hmin} - {Hmax}\nS: {Smin} - {Smax}\nV: {Vmin} - {Vmax}"
        for i, line in enumerate(text.split('\n')):
            textSize, _ = cv2.getTextSize(line, font, font_size, font_thickness)
            cv2.rectangle(
                color_image, 
                (tx - 2, ty + i * dy + 4),
                (tx + textSize[0] + 2, ty + i * dy - textSize[1] - 2),
                (64, 64, 64),
                -1
            )
            cv2.putText(
                color_image, 
                line, 
                (tx, ty + i * dy),
                font, 
                font_size, 
                (255,255,255),
                font_thickness
            )

        # Show images
        cv2.imshow('RealSense', color_image)
        cv2.imshow('Mask', mask)

        if cv2.waitKey(1) == 27:
            break

finally:

    # Stop streaming
    pipeline.stop()