"""
mini-frig detection
"""

import pyrealsense2 as rs
import numpy as np
import cv2
from time import sleep, time
import redis
import sys
import os
from pathlib import Path
import json
# from speak_direct import *
from gdgt_platform import *
from var_dump import var_dump as vd

os.makedirs('/home/pi/tmp/images', exist_ok=True)

if __name__ == '__main__':

	##### SETUP #####

    print('Starting...')

    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    try:
        # Configure depth and color streams
        pipeline = rs.pipeline()
        config = rs.config()

        config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

        # Start streaming
        profile = pipeline.start(config)

        # Get data scale from the device and convert to meters
        device = profile.get_device()
        depth_sensor = device.first_depth_sensor()
        depth_scale = depth_sensor.get_depth_scale() * 1000 # convert to mm
        print(f"depth_scale: {depth_scale}")
        
        # # Turn on IR laser
        depth_sensor.set_option(rs.option.emitter_enabled, 1)

        # depth_scale = profile.get_device().first_depth_sensor().get_depth_scale() * 1000 # convert to mm
        # print(f"depth_scale: {depth_scale}")

        # Create an align object
        # rs.align allows us to perform alignment of depth frames to others frames
        # The 'align_to' is the stream type to which we plan to align depth frames.
        align_to = rs.stream.color
        align = rs.align(align_to)

        # used to record the time when we processed last frame
        prev_frame_time = 0

        # speak_direct(red, 'mini frig detection running')

        kernel = np.ones((3, 3), np.uint8)

        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()

            # Align the depth frame to color frame
            aligned_frames = align.process(frames)

            # Get aligned frames
            depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
            color_frame = aligned_frames.get_color_frame()

            # Validate that both frames are valid
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
            gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)

            gray_image = cv2.blur(gray_image, (5, 5))

            # gray_image = cv2.bilateralFilter(gray_image, 11, 17, 17)
            # edged_image = cv2.Canny(gray_image, 30, 200)

            fMask = cv2.inRange(gray_image, 0, 8)

            fMask = cv2.erode(fMask, kernel, iterations=1)
            # fMask = cv2.dilate(fMask, kernel, iterations=1)

            # red.set('frig', json.dumps({
            #     'frig': centerWorld,
            #     'left': leftWorld
            # }))

            new_frame_time = time()

            # Calculating the fps
            fps = round(1 / (new_frame_time - prev_frame_time))
            print(f"FPS: {round(fps, 2)}")
            print()
            prev_frame_time = new_frame_time

            # Save images
            cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
            cv2.imwrite('/home/pi/tmp/images/gray.jpg', gray_image)
            cv2.imwrite('/home/pi/tmp/images/fmask.png', fMask)
            cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET))

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')

    finally:
        # Stop streaming
        pipeline.stop()

        print('Good-bye')
