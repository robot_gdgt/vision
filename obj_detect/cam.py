import pyrealsense2 as rs
import numpy as np
import cv2

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)

# Start streaming
pipeline.start(config)

# Start a while loop
while True:
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()

    color_frame = frames.get_color_frame()
    if not color_frame:
        continue

    # Convert images to numpy arrays
    color_image = np.asanyarray(color_frame.get_data())

    cv2.line(color_image, (600, 360), (680, 360), (255, 255, 255), 1)
    cv2.imshow("Image", color_image)
    
    if cv2.waitKey(100) == 27:
        break

cv2.destroyAllWindows()
