import pyrealsense2 as rs
import numpy as np
import cv2
import time
import math

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)

while True:

    frames = pipeline.wait_for_frames()
    color_frame = frames.get_color_frame()

    # Validate that both frames are valid
    if not color_frame:
        continue

    # Convert images to numpy arrays
    img = np.asanyarray(color_frame.get_data())

    # blurred = cv2.GaussianBlur(img, (5,5), 0)

    # hsv = cv2.cvtColor(img, cv2.COLOR_BGR2HSV)

    # lower=np.array([0, 0, 0],np.uint8)
    # upper=np.array([10, 50, 50],np.uint8)
    # separated=cv2.inRange(img,lower,upper)
    # contours,hierarchy=cv2.findContours(separated,cv2.RETR_LIST,cv2.CHAIN_APPROX_SIMPLE)

    # gray = cv2.cvtColor(img, cv2.COLOR_BGR2GRAY)
    # blurred = cv2.GaussianBlur(gray, (5, 5), 0)
    # thresh = cv2.threshold(gray, 60, 255, cv2.THRESH_BINARY_INV)

    # minValues = np.array([  0,   0,   0], np.uint8)
    # maxValues = np.array([179, 255,  20], np.uint8)
    # mask = cv2.inRange(cv2.cvtColor(img, cv2.COLOR_BGR2HSV), minValues, maxValues)

    # _, thresh = cv2.threshold(mask, 128, 255, 0)
    # contours, _ = cv2.findContours(thresh, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    # cv2.line(img, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
    # cv2.line(img, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

    # if len(contours) > 0:
    #     # find the biggest countour (c) by the area
    #     c = max(contours, key = cv2.contourArea)
    #     rx, ry, rw, rh = cv2.boundingRect(c)
        
    #     cv2.line(img, (px - 10, py), (px + 10, py), (255, 255, 255), 1)
    #     cv2.line(img, (px, py - 10), (px, py + 10), (255, 255, 255), 1)

    #     cv2.rectangle(mask, (rx, ry), (rx + rw, ry + rh), 128, 1)

    # Show images
    cv2.imshow('RealSense', img)
    # cv2.imshow('Mask', mask)

    # cv2.namedWindow('Depth', cv2.WINDOW_AUTOSIZE)
    # cv2.imshow('Depth', depth_colormap)

    if cv2.waitKey(1) == 27:
        break
