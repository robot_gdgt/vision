class RollingMedian:
    def __init__(self, size, init_value = 0):
        self.size = size
        self.middle = int(size / 2) + 1
        self.buffer = [init_value] * size
        self.ptr = 0
        self.full = False

    def reset(self):
        self.ptr = 0
        self.full = False

    def add(self, v):
        self.buffer[self.ptr] = v
        self.ptr = (self.ptr + 1) % self.size
        if not self.full and self.ptr == 0:
            self.full = True

    def get(self):
        if not self.full:
            return None
        buf = self.buffer.copy()
        buf.sort()
        return buf[self.middle]
