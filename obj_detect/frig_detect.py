"""
mini-frig detection
"""

import pyrealsense2 as rs
import numpy as np
import cv2
import can_proxy_thread as can
from time import sleep, time
import math
from RollingMedian import RollingMedian
import logging
# import logger
import redis
# import sys
import os
# import keyboard
from pathlib import Path
import json
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

os.makedirs('/home/pi/tmp/images', exist_ok=True)

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

WORLD_OFFSET_X = -32.5  # RGB camera is 32.5 mm to the left of center
# D435 is 542 mm above robot origin (ground) when looking down
WORLD_OFFSET_Y = 542
# D435 ref plane is 117 mm in from of robot origin (center of drive wheels) when looking down
WORLD_OFFSET_Z = 117

FLH = 616
FLV = 616

# App state object class

class AppState:
    def __init__(self):
        self.cameraAngle = 90 - 15  # 15 degrees down
        self.cameraAngleRads = math.radians(self.cameraAngle)
        self.threshold = 8
        self.imagesToWrite = VISION_OUTPUT_RGB | VISION_OUTPUT_MASK | VISION_OUTPUT_GRAY | VISION_OUTPUT_DEPTH

state = AppState()

def localMean(depth_frame, x, y):
    neighbors = [(0, 0), (1, 0), (0, 1), (-1, 0), (0, -1)]
    s = 0
    c = 0
    for n in neighbors:
        z = depth_frame.get_distance(x + n[0], y + n[1])
        if z != 0:
            s = s + z
            c = c + 1
    if c == 0:
        return None
    return s / c

def xyd2local(x, y, d):
    return (d * (x - (640 / 2)) / FLH, d * ((480 / 2) - y) / FLV, d)

def local2world(local):
    x, y, z = local
    r = math.sqrt(y ** 2 + z ** 2)
    angA = math.asin(y / r)

    z1 = r * math.sin(angA + state.cameraAngleRads)
    h1 = math.sqrt(math.pow(r, 2) - math.pow(z1, 2))

    return (round(WORLD_OFFSET_X + x), round(WORLD_OFFSET_Y - h1 - 10), round(WORLD_OFFSET_Z + z1))

if __name__ == '__main__':

	##### SETUP #####

    print('Starting...')

    print(f'threshold: {state.threshold}')

    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    # Tell the CAN proxy what packets we want
    can.del_filter('main', 'all', 'all')
    can.add_filter('main', CAN_CMD_HALT, 0x7FF)
    can.add_filter('main', CAN_CMD_VISION, 0x7FF)

    can.send('frig', CAN_CMD_VISION, False, [VISION_D435_ANGLE, 90 - state.cameraAngle])

    try:
        # Configure depth and color streams
        pipeline = rs.pipeline()
        config = rs.config()

        config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

        # Start streaming
        profile = pipeline.start(config)

        # Get data scale from the device and convert to meters
        device = profile.get_device()
        depth_sensor = device.first_depth_sensor()
        depth_scale = depth_sensor.get_depth_scale() * 1000 # convert to mm
        depth_scale = 0.96 * 1000 # Includes conversion from meters to mm
        print(f"depth_scale: {depth_scale}")
        
        # # Turn on IR laser
        depth_sensor.set_option(rs.option.emitter_enabled, 1)

        # depth_scale = profile.get_device().first_depth_sensor().get_depth_scale() * 1000 # convert to mm
        # print(f"depth_scale: {depth_scale}")

        # Create an align object
        # rs.align allows us to perform alignment of depth frames to others frames
        # The 'align_to' is the stream type to which we plan to align depth frames.
        align_to = rs.stream.color
        align = rs.align(align_to)

        # used to record the time when we processed last frame
        prev_frame_time = 0

        # speak_direct(red, 'mini frig detection running')

        kernel = np.ones((3, 3), np.uint8)

        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()

            # Align the depth frame to color frame
            aligned_frames = align.process(frames)

            # Get aligned frames
            depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
            color_frame = aligned_frames.get_color_frame()

            # Validate that both frames are valid
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            # depth_image = np.asanyarray(depth_frame.get_data())
            if state.imagesToWrite & VISION_OUTPUT_DEPTH: cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(np.asanyarray(depth_frame.get_data()), alpha=0.03), cv2.COLORMAP_JET))

            color_image = np.asanyarray(color_frame.get_data())
            # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
            if state.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)

            gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
            # gray_image = cv2.bilateralFilter(gray_image, 11, 17, 17)
            gray_image = cv2.blur(gray_image, (5, 5))

            # cv2.line(gray_image, (0, 240), (639, 240), 128, 1) # horiz line
            # cv2.line(gray_image, (320, 0), (320, 479), 128, 1) # vert line

            ######################
            # Find the black box #
            ######################
            cwX = 0
            cwY = 0
            cwZ = 0

            fMask = cv2.inRange(gray_image, 0, state.threshold)

            # cv2.line(gray_image, (0, 240), (639, 240), 128, 1) # horiz line
            # cv2.line(gray_image, (320, 0), (320, 479), 128, 1) # vert line

            fMask = cv2.erode(fMask, kernel, iterations=1)
            # fMask = cv2.dilate(fMask, kernel, iterations=1)
            if state.imagesToWrite & VISION_OUTPUT_MASK: cv2.imwrite('/home/pi/tmp/images/fmask.png', fMask)

            # _, thresh = cv2.threshold(fMask, 128, 255, 0)
            contours, _ = cv2.findContours(fMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

            if len(contours) > 0:
                # find the biggest countour (c) by the area
                c = max(contours, key = cv2.contourArea)
                crX, crY, crW, crH = cv2.boundingRect(c)
                print(f"crX: {crX} crY: {crY} crW: {crW} crH: {crH}")

                cpX = int(crX + crW / 2)
                cpY = int(crY + crH / 2)

                # draw the biggest contour (c) in white
                cv2.rectangle(gray_image, (crX, crY), (crX + crW, crY + crH), 255, 1)

                # draw white cross at center
                cv2.line(gray_image, (cpX, cpY - 5), (cpX, cpY + 5), 255, 2)
                cv2.line(gray_image, (cpX  - 5, cpY), (cpX + 5, cpY), 255, 2)

                if cpX < 50 or cpX > (640 - 50):
                    if state.imagesToWrite & VISION_OUTPUT_GRAY: cv2.imwrite('/home/pi/tmp/images/gray.jpg', gray_image)
                    print("center too close to edge of image")
                    continue
                
                cpZ = localMean(depth_frame, cpX, cpY)
                if cpZ == None:
                    if state.imagesToWrite & VISION_OUTPUT_GRAY: cv2.imwrite('/home/pi/tmp/images/gray.jpg', gray_image)
                    print("failed to get centerZ")
                    continue

                cpZ *= depth_scale
                print(f"centerZ {round(cpZ)}")
                centerLocal = xyd2local(cpX, cpY, cpZ)
                centerWorld = local2world(centerLocal)
                print(f"C Mean: {round(cpZ)} - Image X, Y: {round(cpX)}, {round(cpY)} - Camera X, Y, Z: {round(centerLocal[0])}, {round(centerLocal[1])}, {round(centerLocal[2])} - World X, Y, Z: {round(centerWorld[0])}, {round(centerWorld[1])}, {round(centerWorld[2])}")

                leftX = None
                leftWorld = None
                if crX > 10:
                    leftZ = cpZ
                    for testX in range(cpX - 20, crX - 10, -20):
                        testZ = localMean(depth_frame, testX, cpY)
                        if testZ == None:
                            continue
                        testZ *= depth_scale
                        if abs(testZ - leftZ) > 200:
                            break
                        leftX = testX
                        leftZ = testZ
                    
                    if leftX == None:
                        print("did not find the left edge")
                    else:
                        # leftZ = leftZ * depth_scale
                        leftLocal = xyd2local(leftX, cpY, leftZ)
                        leftWorld = local2world(leftLocal)
                        print(f"L Mean: {round(leftZ)} - Image X, Y: {round(leftX)}, {round(cpY)} - Camera X, Y, Z: {round(leftLocal[0])}, {round(leftLocal[1])}, {round(leftLocal[2])} - World X, Y, Z: {round(leftWorld[0])}, {round(leftWorld[1])}, {round(leftWorld[2])}")

                        # draw white line at left
                        cv2.line(gray_image, (leftX, cpY - 5), (leftX, cpY + 5), 255, 2)

                """
                0-1 center x signed
                2-3 center z signed
                4-5 left x signed
                6-7 left z signed
                """
                bytes = []
                bytes.extend(centerWorld[0].to_bytes(2, 'big', signed=True))
                bytes.extend(centerWorld[2].to_bytes(2, 'big', signed=True))
                if leftWorld != None:
                    bytes.extend(leftWorld[0].to_bytes(2, 'big', signed=True))
                    bytes.extend(leftWorld[2].to_bytes(2, 'big', signed=True))
                else:
                    bytes.extend([0, 0, 0, 0])
                can.send('frig', CAN_TS_FRIG, False, bytes)
                red.set('frig', json.dumps({
                    'frig': centerWorld,
                    'left': leftWorld
                }))

            if state.imagesToWrite & VISION_OUTPUT_GRAY: cv2.imwrite('/home/pi/tmp/images/gray.jpg', gray_image)

            new_frame_time = time()

            # Calculating the fps
            fps = round(1 / (new_frame_time - prev_frame_time))
            print(f"FPS: {round(fps, 2)}")
            print()
            prev_frame_time = new_frame_time

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')

    finally:
        # Stop streaming
        pipeline.stop()

        print('Good-bye')
