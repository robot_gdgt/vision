import pyrealsense2 as rs
import numpy as np
import cv2
import time
import math

def nothing(dummy):
    pass

showDepth = False

cv2.namedWindow('Mask', cv2.WINDOW_AUTOSIZE)
cv2.createTrackbar('H', 'Mask', 0, 179, nothing)
cv2.createTrackbar('H ±', 'Mask', 10, 90, nothing)
cv2.createTrackbar('S', 'Mask', 200, 255, nothing)
cv2.createTrackbar('S ±', 'Mask', 60, 128, nothing)
cv2.createTrackbar('V', 'Mask', 130, 255, nothing)
cv2.createTrackbar('V ±', 'Mask', 60, 128, nothing)

cv2.namedWindow('RealSense', cv2.WINDOW_AUTOSIZE)
cv2.createTrackbar('Sample Size', 'RealSense', 25, 100, nothing)
cv2.createTrackbar('FL - H 600+', 'RealSense', 16, 30, nothing)
cv2.createTrackbar('FL - V 600+', 'RealSense', 16, 30, nothing)

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

# Get data scale from the device and convert to meters
depth_scale = profile.get_device().first_depth_sensor().get_depth_scale() * 1000 # convert to mm
# print(depth_scale)

# Create an align object
# rs.align allows us to perform alignment of depth frames to others frames
# The 'align_to' is the stream type to which we plan to align depth frames.
align_to = rs.stream.color
align = rs.align(align_to)

# used to record the time when we processed last frame
prev_frame_time = 0

try:
    while True:

        # Wait for a coherent pair of frames: depth and color
        frames = pipeline.wait_for_frames()

        # Align the depth frame to color frame
        aligned_frames = align.process(frames)

        # Get aligned frames
        depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
        color_frame = aligned_frames.get_color_frame()

        # Validate that both frames are valid
        if not depth_frame or not color_frame:
            continue

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        color_image = np.asanyarray(color_frame.get_data())
        
        depth_colormap = cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET)

        Hmin = cv2.getTrackbarPos('H', 'Mask') - cv2.getTrackbarPos('H ±', 'Mask')
        if Hmin < 0:
            Hmin = Hmin + 180
        Hmax = cv2.getTrackbarPos('H', 'Mask') + cv2.getTrackbarPos('H ±', 'Mask')
        if Hmax > 179:
            Hmax = Hmax - 180
        Smin = cv2.getTrackbarPos('S', 'Mask') - cv2.getTrackbarPos('S ±', 'Mask')
        if Smin < 0:
            Smin = 0
        Smax = cv2.getTrackbarPos('S', 'Mask') + cv2.getTrackbarPos('S ±', 'Mask')
        if Smax > 255:
            Smax = 255
        Vmin = cv2.getTrackbarPos('V', 'Mask') - cv2.getTrackbarPos('V ±', 'Mask')
        if Vmin < 0:
            Vmin = 0
        Vmax = cv2.getTrackbarPos('V', 'Mask') + cv2.getTrackbarPos('V ±', 'Mask')
        if Vmax > 255:
            Vmax = 255
    
        if Hmax > Hmin:
            minValues = np.array([Hmin, Smin, Vmin], np.uint8)
            maxValues = np.array([Hmax, Smax, Vmax], np.uint8)
            mask = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)
        else:
            minValues = np.array([0, Smin, Vmin], np.uint8)
            maxValues = np.array([Hmax, Smax, Vmax], np.uint8)
            mask1 = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)

            minValues = np.array([Hmin, Smin, Vmin], np.uint8)
            maxValues = np.array([179, Smax, Vmax], np.uint8)
            mask2 = cv2.inRange(cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV), minValues, maxValues)

            mask = mask1 + mask2

        # _, thresh = cv2.threshold(mask, 128, 255, 0)
        contours, _ = cv2.findContours(mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

        cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
        cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

        cZ = '?'
        centerXY = '?, ?'
        canWH = '?, ?'
        camXYZ = '?, ?, ?'
        worldXYZ = '?, ?, ?'

        if len(contours) > 0:
            # find the biggest countour (c) by the area
            c = max(contours, key = cv2.contourArea)

            mask = np.zeros_like(color_image)
            cv2.fillPoly(mask, pts=[c], color=(255, 255, 255))
            # masked_img = cv2.bitwise_and(img, mask)

            rx, ry, rw, rh = cv2.boundingRect(c)

            sampleSize = cv2.getTrackbarPos('Sample Size', 'RealSense')
            px = int(rx + rw / 2)
            if sampleSize < rw:
                sx0 = px - int(sampleSize / 2)
                sx1 = sx0 + sampleSize
            else:
                sx0 = rx
                sx1 = rx + rw
            py = int(ry + rh / 2)
            if sampleSize < rh:
                sy0 = py - int(sampleSize / 2)
                sy1 = sy0 + sampleSize
            else:
                sy0 = ry
                sy1 = ry + rh
            
#             depth_scale = profile.get_device().first_depth_sensor().get_depth_scale()
#             print(depth_scale)

            depth_cropped = depth_image[sy0:sy1, sx0:sx1].astype(float) * depth_scale
#             print(depth_cropped)
            
            cv2.line(color_image, (px - 10, py), (px + 10, py), (255, 255, 255), 1)
            cv2.line(color_image, (px, py - 10), (px, py + 10), (255, 255, 255), 1)
            centerXY = f'{px}, {py}'
            
            # cnz = np.count_nonzero(depth_cropped)
            # if cnz > 0:
            #     dist_mean = round(np.sum(depth_cropped) / cnz)
            # else:
            #     dist_mean = '?'

            cZ = np.median(depth_cropped)
            if cZ > 0:
                flh = cv2.getTrackbarPos('FL - H 600+', 'RealSense') + 600
                flv = cv2.getTrackbarPos('FL - V 600+', 'RealSense') + 600

                cX = cZ * (px - (640 / 2)) / flh
                cY = cZ * ((480 / 2) - py) / flv
                canW = cZ * rw / flh
                canH = cZ * rh / flv

                canWH = f'{round(canW)}, {round(canH)}'

                if canW > 40 and canW < 80 and canH > 70 and canH < 120:
                
                    camXYZ = f'{round(cX)}, {round(cY)}, {round(cZ)}'

                    cR = math.sqrt(cY ** 2 + cZ ** 2)
                    cAngA = math.asin(cY / cR)

                    cZ1 = cR * math.sin(cAngA + math.radians(45))
                    cH1 = math.sqrt(math.pow(cR, 2) - math.pow(cZ1, 2))

                    wz = 117 + round(cZ1) # D435 ref plane is 117 mm in from of robot origin (center of drive wheels) when looking down
                    wy = 542 - round(cH1) # D435 is 542 mm above robot origin (ground) when looking down
                    wx = round(cX - 32.5) # RGB camera is 32.5 mm to the left of center

                    worldXYZ = f'{wx}, {wy}, {wz}'

                    # draw the biggest contour (c) in green
                    cv2.rectangle(color_image, (rx, ry), (rx + rw, ry + rh), (0, 255, 0), 1)

                    # draw the sample area in blue
                    cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (0, 0, 255), 1)

                else:
                    # draw the biggest contour (c) in gray
                    cv2.rectangle(color_image, (rx, ry), (rx + rw, ry + rh), (128, 128, 128), 1)

                    # draw the sample area in gray
                    cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (128, 128, 128), 1)

                cv2.rectangle(mask, (rx, ry), (rx + rw, ry + rh), 128, 1)

                cZ = round(cZ)
            
        new_frame_time = time.time()

        # Calculating the fps
        fps = round(1 / (new_frame_time - prev_frame_time))
        prev_frame_time = new_frame_time

        tx, ty, dy, font, font_size, font_thickness = 4, 16, 19, cv2.FONT_HERSHEY_SIMPLEX, 0.5, 1
        text = f"H: {Hmin} - {Hmax}\nS: {Smin} - {Smax}\nV: {Vmin} - {Vmax}\nMedian: {cZ}\nImage X, Y: {centerXY}\nCan W, H: {canWH}\nCamera X, Y, Z: {camXYZ}\nWorld X, Y, Z: {worldXYZ}\nFPS: {fps}"
        for i, line in enumerate(text.split('\n')):
            textSize, _ = cv2.getTextSize(line, font, font_size, font_thickness)
            cv2.rectangle(
                color_image, 
                (tx - 2, ty + i * dy + 4),
                (tx + textSize[0] + 2, ty + i * dy - textSize[1] - 2),
                (64, 64, 64),
                -1
            )
            cv2.putText(
                color_image, 
                line, 
                (tx, ty + i * dy),
                font, 
                font_size, 
                (255,255,255),
                font_thickness
            )

        # Show images
        cv2.imshow('RealSense', color_image)
        cv2.imshow('Mask', mask)
        if showDepth:
            cv2.namedWindow('Depth', cv2.WINDOW_AUTOSIZE)
            cv2.imshow('Depth', depth_colormap)

        if cv2.waitKey(1) == 27:
            break

finally:

    # Stop streaming
    pipeline.stop()