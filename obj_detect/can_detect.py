"""
pop can detection
"""

import pyrealsense2 as rs
import numpy as np
import numpy.ma as ma
import cv2
from time import sleep, time
import math
from RollingMedian import RollingMedian
import logging
import redis
import sys
import os
# import keyboard
from pathlib import Path
import json
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
import logger
from var_dump import var_dump as vd
import can_proxy_thread as can

os.makedirs('/home/pi/tmp/images', exist_ok=True)

WORLD_OFFSET_X = -32.5 # RGB camera is 32.5 mm to the left of center
WORLD_OFFSET_Y = 542 # D435 is 542 mm above robot origin (ground) when looking down
WORLD_OFFSET_Z = 117 # D435 ref plane is 117 mm in from of robot origin (center of drive wheels) when looking down

SAMPLESIZE = 25
FLH = 616
FLV = 616

GRIP_WIDTH = 62

# canMin0 = [0, 140, 70]
# canMax0 = [10, 255, 190]
# canMin1 = [169, 140, 70]
# canMax1 = [179, 255, 190]
canMin0 = [0, 110, 55]
canMax0 = [5, 255, 195]
canMin1 = [174, 110, 55]
canMax1 = [179, 255, 195]

gripMin = [84, 120, 75]
gripMax = [124, 255, 255]

# App state object class
class AppState:
    def __init__(self):
        self.cam_angle = 45 # degrees up
        self.cam_angleRads = math.radians(self.cam_angle)
        # self.state = CAN_CMD_PGM_RUNNING

state = AppState()

if __name__ == '__main__':

	##### SETUP #####

    print('Starting...')

    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    can.send('can', CAN_CMD_VISION, False, [VISION_D435_ANGLE, 45])

    try:
        # Configure depth and color streams
        pipeline = rs.pipeline()
        config = rs.config()

        config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
        config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

        # Start streaming
        profile = pipeline.start(config)

        # Get data scale from the device and convert to meters
        depth_scale = profile.get_device().first_depth_sensor().get_depth_scale() * 1000 # convert to mm
        print(f"depth_scale: {depth_scale}")

        # Create an align object
        # rs.align allows us to perform alignment of depth frames to others frames
        # The 'align_to' is the stream type to which we plan to align depth frames.
        align_to = rs.stream.color
        align = rs.align(align_to)

        # used to record the time when we processed last frame
        prev_frame_time = 0

        kernel = np.ones((3, 3), np.uint8)

        # speak_direct(red, 'can detection running')

        while True:

            # Wait for a coherent pair of frames: depth and color
            frames = pipeline.wait_for_frames()

            # Align the depth frame to color frame
            aligned_frames = align.process(frames)

            # Get aligned frames
            depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
            color_frame = aligned_frames.get_color_frame()

            # Validate that both frames are valid
            if not depth_frame or not color_frame:
                continue

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            color_image = np.asanyarray(color_frame.get_data())
            # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
            hsv_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV)

            cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
            cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

            ####################
            # Find the red can #
            ####################
            cwX = 0
            cwY = 0
            cwZ = 0

            minValues = np.array(canMin0, np.uint8)
            maxValues = np.array(canMax0, np.uint8)
            cMask0 = cv2.inRange(hsv_image, minValues, maxValues)

            minValues = np.array(canMin1, np.uint8)
            maxValues = np.array(canMax1, np.uint8)
            cMask1 = cv2.inRange(hsv_image, minValues, maxValues)

            cMask = cMask0 + cMask1

            # cMask = cv2.erode(cMask, kernel, iterations=1)
            # cMask = cv2.dilate(cMask, kernel, iterations=1)

            # _, thresh = cv2.threshold(cMask, 128, 255, 0)
            contours, _ = cv2.findContours(cMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

            if len(contours) > 0:
                # find the biggest countour (c) by the area
                c = max(contours, key = cv2.contourArea)
                crX, crY, crW, crH = cv2.boundingRect(c)

                cpX = int(crX + crW / 2)
                if SAMPLESIZE < crW:
                    sx0 = cpX - int(SAMPLESIZE / 2)
                    sx1 = sx0 + SAMPLESIZE
                else:
                    sx0 = crX
                    sx1 = crX + crW
                cpY = int(crY + crH / 2)
                if SAMPLESIZE < crH:
                    sy0 = cpY - int(SAMPLESIZE / 2)
                    sy1 = sy0 + SAMPLESIZE
                else:
                    sy0 = crY
                    sy1 = crY + crH
                
                depth_cropped = depth_image[sy0:sy1, sx0:sx1].astype(float)                  

                ccZ = np.median(depth_cropped)
                # print(f"ccZ: {ccZ}")
                if ccZ > 0:
                    ccZ = ccZ * depth_scale
                    ccX = ccZ * (cpX - (640 / 2)) / FLH
                    ccY = ccZ * ((480 / 2) - cpY) / FLV
                    canW = ccZ * crW / FLH
                    canH = ccZ * crH / FLV

                    if canW > 45 and canW < 75 and canH > 70 and canH < 110:
                        cR = math.sqrt(ccY ** 2 + ccZ ** 2)
                        cAngA = math.asin(ccY / cR)

                        cZ1 = cR * math.sin(cAngA + state.cam_angleRads)
                        cH1 = math.sqrt(math.pow(cR, 2) - math.pow(cZ1, 2))

                        cwX = round(WORLD_OFFSET_X + ccX)
                        cwY = round(WORLD_OFFSET_Y - cH1 - 10)
                        cwZ = round(WORLD_OFFSET_Z + cZ1)

                        print(f"C Median: {round(ccZ)} - Image X, Y: {round(cpX)}, {round(cpY)} - Can W, H: {round(canW)}, {round(canH)} - Camera X, Y, Z: {round(ccX)}, {round(ccY)}, {round(ccZ)} - World X, Y, Z: {round(cwX)}, {round(cwY)}, {round(cwZ)}")

                        # draw the biggest contour (c) in white
                        cv2.rectangle(color_image, (crX, crY), (crX + crW, crY + crH), (255, 255, 255), 1)

                        # draw the sample area in blue
                        cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (255, 255, 255), 2)

                    else:
                        # draw the biggest contour (c) in gray
                        cv2.rectangle(color_image, (crX, crY), (crX + crW, crY + crH), (128, 128, 128), 1)

                        # draw the sample area in gray
                        cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (128, 128, 128), 2)

            #################################
            # Find the blue gripper markers #
            #################################
            gwX = 0
            gwY = 0
            gwZ = 0

            minValues = np.array(gripMin, np.uint8)
            maxValues = np.array(gripMax, np.uint8)
            gMask = cv2.inRange(hsv_image, minValues, maxValues)

            # cv2.rectangle(gMask, (0, 0), (640, 250), (0), -1)
            # gMask = cv2.erode(gMask, kernel, iterations=1)
            # gMask = cv2.dilate(gMask, kernel, iterations=1)

            contours, _ = cv2.findContours(gMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
            for c in contours:
                print(f"{cv2.contourArea(c)} ", end=' ')
            print()

            if len(contours) >= 2:
                if len(contours) > 2:
                    totalSize = 0
                    for c in contours:
                        totalSize = totalSize + cv2.contourArea(c)
                    avgSize = totalSize / len(contours)
                    contours = [c for c in contours if cv2.contourArea(c) > avgSize]

                for c in contours:
                    print(f"{cv2.contourArea(c)} ", end=' ')
                print()

                if len(contours) == 2:
                    pts = []
                    for c in contours:
                        x, y, w, h = cv2.boundingRect(c)
                        cv2.rectangle(color_image, (x, y), (x + w, y + h), (255, 255, 255), 1)
                        pts.append({'x': x + w / 2, 'y': y + h / 2})
                    print(f"pt0: {pts[0]['x']}, {pts[0]['y']}  pt1: {pts[1]['x']}, {pts[1]['y']}")

                    grW = math.sqrt(math.pow(pts[0]['x'] - pts[1]['x'], 2) + math.pow(pts[0]['y'] - pts[1]['y'], 2))

                    gcZ = FLH * GRIP_WIDTH / grW
                    print(f"gcZ: {round(gcZ)}")

                    gpX = int((pts[0]['x'] + pts[1]['x']) / 2)
                    gpY = int((pts[0]['y'] + pts[1]['y']) / 2)

                    # cnz = np.count_nonzero(depth_cropped)
                    # if cnz > 0:
                    #     gZ = np.sum(depth_cropped) / cnz
                    # gW = 0
                    # gcZ = np.max(depth_cropped)
                    if gcZ > 0:
                        gcX = gcZ * (gpX - (640 / 2)) / FLH
                        gcY = gcZ * ((480 / 2) - gpY) / FLV
                        # gW = gcZ * grW / FLH

                        gR = math.sqrt(gcY ** 2 + gcZ ** 2)
                        gAngA = math.asin(gcY / gR)

                        gZ1 = gR * math.sin(gAngA + state.cam_angleRads)
                        gH1 = math.sqrt(math.pow(gR, 2) - math.pow(gZ1, 2))

                        gwX = round(WORLD_OFFSET_X + gcX)
                        gwY = round(WORLD_OFFSET_Y - gH1)
                        gwZ = round(WORLD_OFFSET_Z + gZ1)

                        # print(f"G Median: {round(gcZ)} - Image X, Y: {round(gpX)}, {round(grY)} - Grip W: {round(grW)} - Camera X, Y, Z: {round(gcX)}, {round(gcY)}, {round(gcZ)} - World X, Y, Z: {round(gwX)}, {round(gwY)}, {round(gwZ)}")
                        print(f"G Image X, Y: {round(gpX)}, {round(gpY)} - Camera X, Y, Z: {round(gcX)}, {round(gcY)}, {round(gcZ)} - World X, Y, Z: {round(gwX)}, {round(gwY)}, {round(gwZ)}")

                        # draw the biggest contour (c) in white
                        # cv2.rectangle(color_image, (grX, grY), (grX + grW, grY + grH), (255, 255, 255), 1)
                        cv2.line(color_image, (round(pts[0]['x']), round(pts[0]['y'])), (round(pts[1]['x']), round(pts[1]['y'])), (255, 255, 255), 2) # line
                        cv2.line(color_image, (gpX, gpY - 10), (gpX, gpY + 10), (255, 255, 255), 1)

            if cwZ or gwZ:
                """
                0 can flags
                    6-7 n/a
                    5 z 10th bit
                    4 z 9th bit
                    3 y 10th bit
                    2 y 9th bit
                    1 x sign
                    0 x 9th bit
                1 can x
                2 can y
                3 can z
                4 grip flags
                    6-7 n/a
                    5 z 10th bit
                    4 z 9th bit
                    3 y 10th bit
                    2 y 9th bit
                    1 x sign
                    0 x 9th bit
                5 grip x
                6 grip y
                7 grip z
                """
                can_flags = ((cwZ >> 4) & 0b00110000) | ((cwY >> 6) & 0b00001100) | ((abs(cwX) >> 8) & 0b00000001) | (0b10 if cwX < 0 else 0b00)
                grip_flags = ((gwZ >> 4) & 0b00110000) | ((gwY >> 6) & 0b00001100) | ((abs(gwX) >> 8) & 0b00000001) | (0b10 if gwX < 0 else 0b00)
                can.send('can', CAN_TS_CAN_GRIP, False, [can_flags,
                    abs(cwX) & 0xFF,
                    cwY & 0xFF,
                    cwZ & 0xFF,
                    grip_flags,
                    abs(gwX) & 0xFF,
                    gwY & 0xFF,
                    gwZ & 0xFF
                ])
                red.set('can_grip', json.dumps({
                    'can': [cwX, cwY, cwZ],
                    'grip': [gwX, gwY, gwZ]
                }))

            new_frame_time = time()

            # Calculating the fps
            fps = round(1 / (new_frame_time - prev_frame_time))
            print(f"FPS: {round(fps, 2)}")
            print()
            prev_frame_time = new_frame_time

            # Save images
            cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
            cv2.imwrite('/home/pi/tmp/images/cmask.png', cMask)
            cv2.imwrite('/home/pi/tmp/images/gmask.png', gMask)
            cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET))

            # CANrx = red.lpop(FILE)
            # if CANrx is not None:
            #     CANrx = json.loads(CANrx)
            #     logging.info(f'CANrx: {CANrx}')
            #     if CANrx['can_id'] == CAN_CMD_VISION:
            #         if CANrx['data'][0] == CAN_CMD_D435_ANGLE:
            #             state.cam_angle = CANrx['data'][1]
            #             logging.info(f'state.cam_angle: {state.cam_angle}')
            #             state.cam_angle_cos = math.cos(math.radians(state.cam_angle))
            #             logging.info(f'state.cam_angle_cos: {state.cam_angle_cos}')
            #             state.cam_angle_sin = math.sin(math.radians(state.cam_angle))
            #             logging.info(f'state.cam_angle_sin: {state.cam_angle_sin}')
            #     if CANrx['can_id'] == CAN_CMD_HALT:
            #         logging.info('shutdown')
            #         state.state = CAN_CMD_PGM_QUIT

            # if state.state == CAN_CMD_PGM_QUIT:
            #     break

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')

    finally:
        # Stop streaming
        pipeline.stop()

        print('Good-bye')
