import pyrealsense2 as rs
import numpy as np
import cv2

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

w = 1280
h = 720
scale = 0.5

# w = 640
# h = 480
scale = 1

config.enable_stream(rs.stream.color, w, h, rs.format.bgr8, 6)

w = int(w * scale)
h = int(h * scale)

# Start streaming
pipeline.start(config)

# Start a while loop
while True:
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()

    color_frame = frames.get_color_frame()
    if not color_frame:
        continue

    # Convert images to numpy arrays
    color_image = np.asanyarray(color_frame.get_data())
    if scale != 1:
        color_image = cv2.resize(color_image, (w, h), interpolation = cv2.INTER_AREA)

    cv2.line(color_image, (0, int(h / 2)), (w, int(h / 2)), (255, 255, 255), 1)
    cv2.line(color_image, (int(w / 2), 0), (int(w / 2), h), (255, 255, 255), 1)
    cv2.imshow(f"Source: {int(w / scale)}x{int(h / scale)} Show: {w}x{h}", color_image)
    
    if cv2.waitKey(100) == 27:
        break

cv2.destroyAllWindows()
