"""
depth export
"""

import pyrealsense2 as rs
import numpy as np
import numpy.ma as ma
import cv2
from time import sleep, time
import math
from pathlib import Path
import os
from gdgt_platform import *
from var_dump import var_dump as vd

if __name__ == '__main__':

	##### SETUP #####

    print('Starting...')

    try:
        # Configure depth and color streams
        pipeline = rs.pipeline()
        config = rs.config()

        config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)

        # Start streaming
        profile = pipeline.start(config)

        # Get data scale from the device and convert to meters
        device = profile.get_device()
        depth_sensor = device.first_depth_sensor()
        depth_scale = depth_sensor.get_depth_scale() * 1000 # convert to mm
        # print(depth_scale)
        
        # Turn on IR laser
        depth_sensor.set_option(rs.option.emitter_enabled, 1)

        for c in range(5):

            while True:
                frames = pipeline.wait_for_frames()

                depth_frame = frames.get_depth_frame()

                if depth_frame:
                    break

            # Convert images to numpy arrays
            depth_image = np.asanyarray(depth_frame.get_data())
            print(c)

            fp = open('/tmp/depth.txt', 'w')
            for x in range(640):
                for y in range(480):
                    # print(f"X {x} Y {y}")
                    fp.write(f"{depth_image[y, x]}\t")
                fp.write('\n')
            fp.close()

            # Save images
            Path('/var/tmp/images/depth.lock').touch()
            cv2.imwrite('/var/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET))
            os.remove('/var/tmp/images/depth.lock')

    finally:
        # Stop streaming
        pipeline.stop()

        print('Good-bye')
