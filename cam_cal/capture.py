import pyrealsense2 as rs
import numpy as np
import cv2
from time import sleep, time

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.color, 1280, 720, rs.format.bgr8, 6)

# Start streaming
pipeline.start(config)

# Let autoexposure settle
for n in range(0, 5):
    print(f'skip {n}')
    color_frame = None
    while not color_frame:
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()
    sleep(1)

# Capture images
for n in range(0, 5):
    print(f'capture {n}')
    color_frame = None
    while not color_frame:
        frames = pipeline.wait_for_frames()
        color_frame = frames.get_color_frame()

    color_image = np.asanyarray(color_frame.get_data())
    # gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)

    cv2.imwrite(f'/home/pi/tmp/images/img{n}.jpg', color_image)

    sleep(1)
