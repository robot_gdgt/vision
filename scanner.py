"""
Wall and obstacle detection using MetaSense A010
"""

import cv2
import numpy as np
import json
# import logging
import redis
# import os
from pathlib import Path
import serial # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
from struct import unpack
from time import sleep

import globals
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
# from var_dump import var_dump as vd

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

FRAME_HEAD = b"\x00\xFF"
FRAME_TAIL = b"\xDD"
FRAME_TAIL_INT = 0xDD

STATE_IDLE = 0
STATE_WAITING = 1
STATE_DONE = 9

state = STATE_IDLE

# MetaSense ref plane is 110 mm in front of robot origin (center of drive wheels)
WORLD_OFFSET_Z = 110

FLH = 78

usb = None
rawData = b''
prevID = -1
scannerReady = False
scannerUnit = 9

verbose = False

##### ON LOAD #####

"""
# attempt to communicate at default speed
print('open usb at low speed')
usb = serial.Serial('/dev/ttyUSB0', 115200, 8, 'N', 1, timeout=0.1)
print(f'usb.is_open: {usb.is_open}')
usb.write(b"AT+ISP=0\r")
sleep(0.5)
usb.reset_input_buffer()
usb.write(b"AT\r") # test communications
recd = usb.read(128)
print(f'recd1: {recd}')
if recd[0:2] == b'OK': # we now know it is at low speed so set it to high speed
    usb.write(b"AT+BAUD=3\r")
    # no response will be received

usb.close()
print('reopen usb at high speed')
usb = serial.Serial('/dev/ttyUSB0', 230400, 8, 'N', 1, timeout=0.1)
print(f'usb.is_open: {usb.is_open}')
usb.write(b"AT+ISP=0\r")
sleep(0.5)
usb.reset_input_buffer()
usb.write(b"AT\r") # test communications
recd = usb.read(128)
print(f'recd3: {recd}')
if recd[0:2] != b'OK':
    print('failed to init MetaSense')
else:
    usb.reset_input_buffer()
    usb.write(b"AT+BINN=1\r")
    recd = usb.read(256)
    print(f'BINN=1: {recd}')
    usb.write(b"AT+UNIT=9\r")
    recd = usb.read(256)
    print(f'UNIT=9: {recd}')
    usb.write(b"AT+FPS=5\r")
    recd = usb.read(256)
    print(f'FPS=5: {recd}')
    usb.write(b"AT+DISP=5\r")
    recd = usb.read(256)
    print(f'DISP=5: {recd}')
    usb.write(b"AT+ISP=1\r")
    print('scanner initialized')
    scannerReady = True
"""

# attempt to communicate at default speed
print('open usb')
usb = serial.Serial('/dev/ttyUSB0', 115200, 8, 'N', 1, timeout=0.1)
print(f'usb.is_open: {usb.is_open}')
for i in range(5):
    usb.write(b"AT+ISP=0\r")
    sleep(0.5)
    usb.reset_input_buffer()
    usb.write(b"AT\r") # test communications
    recd = usb.read(128)
    print(f'recd1: {recd}')
    if recd[0:2] == b'OK':
        break
if recd[0:2] != b'OK':
    print('failed to initialize MetaSense')
else:
    usb.reset_input_buffer()
    usb.write(b"AT+BINN=1\r")
    recd = usb.read(256)
    print(f'BINN=1: {recd}')
    usb.write(bytes(f"AT+UNIT={scannerUnit}\r", 'ASCII')) # UNIT = 1 is '1' = 49
    recd = usb.read(256)
    print(f'UNIT={scannerUnit}: {recd}')
    usb.write(b"AT+FPS=5\r")
    recd = usb.read(256)
    print(f'FPS=5: {recd}')
    usb.write(b"AT+DISP=4\r")
    recd = usb.read(256)
    print(f'DISP=4: {recd}')
    usb.write(b"AT+ISP=1\r")
    print('MetaSense initialized')

usb.close()

##### USB #####

def openUsbIfNeeded():
    global usb

    if usb and usb.is_open: 
        if verbose: print('usb already open')
        return True
    print('open usb')
    usb = serial.Serial('/dev/ttyUSB0', 115200, 8, 'N', 1, timeout=0.1) # usbreset
    print(f'usb.is_open: {usb.is_open}')
    return usb.is_open

def setScannerResolution(unit):
    global usb
    global scannerUnit
    scannerUnit = unit

    openUsbIfNeeded()
    for i in range(5):
        usb.write(b"AT+ISP=0\r")
        sleep(0.5)
        usb.reset_input_buffer()
        usb.write(bytes(f"AT+UNIT={scannerUnit}\r", 'ASCII')) # UNIT = 1 is '1' = 49
        recd = usb.read(128)
        print(f'UNIT={scannerUnit}: {recd}')
        if recd[0:2] == b'OK':
            usb.write(b"AT+ISP=1\r")
            break
    if recd[0:2] != b'OK':
        print('failed to change UNIT')
        return False
    else:
        print(f'MetaSense UNIT = {scannerUnit}')
        return True

def initScannerIfNeeded(force = False):
    return
    global scannerReady
    global rawData

    if scannerReady and not force: 
        if verbose: print('scanner already initialized')
        return

    usb.write(b"AT+ISP=0\r")
    sleep(0.1)
    usb.reset_input_buffer()
    usb.write(b"AT+BINN=1\r")
    recd = usb.read(256)
    print(f'BINN=1: {recd}')
    usb.write(b"AT+UNIT=9\r")
    recd = usb.read(256)
    print(f'UNIT=9: {recd}')
    usb.write(b"AT+FPS=5\r")
    recd = usb.read(256)
    print(f'FPS=5: {recd}')
    usb.write(b"AT+DISP=5\r")
    recd = usb.read(256)
    print(f'DISP=5: {recd}')
    usb.write(b"AT+ISP=1\r")
    print('scanner initialized')
    scannerReady = True
    rawData = b''

##### INIT #####

def init(cmd = 0):
    global usb

    # global state
    # can.del_filter('scanner', 'all', 'all')
    # can.add_filter('obst_detect', CAN_STATUS_HEADLIGHT, 0x7FF)

    globals.imagesToWrite =  VISION_OUTPUT_DEPTH | VISION_OUTPUT_GRID | VISION_OUTPUT_GRAPH1

    if cmd in (CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_HALL_WALLS_POS, CAN_STATUS_FRIG_POS) and scannerUnit != 9:
        setScannerResolution(9)
    elif cmd == CAN_STATUS_DOOR_POS and scannerUnit != 2:
        setScannerResolution(2)

##### TICK #####

def tick(cmd):
    global usb
    global scannerReady
    global rawData
    global prevID
    global state

    if not openUsbIfNeeded():
        print('usb did not open!')
        return STATE_IDLE
    
    # initScannerIfNeeded()

    if not usb.in_waiting:
        return STATE_WAITING
    
    rawData += usb.read_until(FRAME_TAIL)
    print('.', end='')
    idx = rawData.find(FRAME_HEAD)
    if idx < 0:
        rawData = rawData[-1:]
        return STATE_WAITING
    if idx > 0:
        rawData = rawData[idx:]

    if len(rawData) < 647: # minimum length is 625 data + 20 header + 2 tail bytes
        return STATE_WAITING

    # print(rawData)
    # check data length 2Byte
    dataLen = unpack("H", rawData[2:4])[0]
    frameLen = len(FRAME_HEAD) + 2 + dataLen + 2
    # print(f"dataLen: {dataLen} frameLen: {frameLen} len(rawData): {len(rawData)}")
    if len(rawData) < frameLen: # We need more data to complete the frame
        return STATE_WAITING

    # print(f"\ndataLen: {dataLen} frameLen: {frameLen} len(rawData): {len(rawData)}")

    # print('rawData')
    # print(rawData.hex())

    # frame is now complete
    frameData = rawData[:frameLen]
    # print('frameData')
    # print(frameData.hex())

    # We may have some of the next frame
    rawData = rawData[frameLen:]
    # print('new rawData')
    # print(rawData.hex())

    # frame tail
    frameTail = frameData[-1]
    print(f"tail: {hex(frameTail)}")
    if frameTail != FRAME_TAIL_INT:
        print('invalid tail')
        frameData = b''
        return STATE_WAITING

    # checksum
    cksum = frameData[-2]
    print(f"checksum: {hex(cksum)}")
    if cksum != sum(frameData[:frameLen - 2]) % 256:
        print('checksum failed')
        frameData = b''
        return STATE_WAITING

    frameID = unpack("H", frameData[16:18])[0]
    print(f"prevID: {prevID} frameID: {frameID}")

    if frameID == 0:
        print('reseting frame')
        frameData = b''
        return STATE_WAITING

    if frameID == prevID:
        print('duplicate frame')
        frameData = b''
        return STATE_WAITING
    
    prevID = frameID

    numR = frameData[14]
    numC = frameData[15]
    print((numR, numC))

    frameDataLen = dataLen - 16
    frameData = np.array([unpack("B", frameData[20+i:21+i])[0] for i in range(0, frameDataLen)], dtype=np.uint8).reshape(numR, numC)
    
    row1 = int(numR / 2 + 0.5) - 3
    row2 = row1 + 6

    # frameData = np.array(frameData, dtype=np.uint8).reshape(numR, numC)
    # sweep = np.zeros((numC), dtype=np.uint8)
    # for c in range(numC):
    #     n = 0
    #     z = 0
    #     for r in range(row1, row2+1):
    #         if frameData[r, c] < 255:
    #             n += 1
    #             z += frameData[r, c]
    #     if n > 2:
    #         sweep[c] = int(z / n + 0.5)

    # MetaSense uses 255 to indicate no data.
    # The code below uses 0 to indicate the same.
    frameData[frameData == 255] = 0

    avgSweep = np.zeros((numC), dtype=np.uint8)
    for c in range(numC):
        idxFrom = c - 3
        if idxFrom < 0: idxFrom = 0
        idxTo = c + 3
        if idxTo > numC: idxTo = numC
        avgSweep[c] = int(avgNonZero(frameData[row1:row2+1, idxFrom:idxTo+1].flatten(), 30) + 0.5)
    # print(avgSweep)
    # offset = 0
    # while offset < numC:
    #     for c in range(offset, offset+25):
    #         print(f'{c:3} ', end='')
    #     print()
    #     for c in range(offset, offset+25):
    #         print(f'{avgSweep[c]:3} ', end='')
    #     print()
    #     print()
    #     offset += 25

    cornerL = None
    cornerR = None

    ##### HALL ENTRY #####

    if cmd == CAN_STATUS_HALL_ENTER_POS:

        # look for two valleys
        # cornerL = {'c':-1, 'z':255}
        # cornerR = {'c':-1, 'z':255}
        # for c in range(0, int(numC / 2)):
        #     if avgSweep[c] > 0 and avgSweep[c] < cornerL['z']:
        #         cornerL = {'c':c, 'z':avgSweep[c]}
        #     c = numC - c - 1
        #     if avgSweep[c] > 0 and avgSweep[c] < cornerR['z']:
        #         cornerR = {'c':c, 'z':avgSweep[c]}
        # if cornerL['c'] == -1: cornerL = None
        # if cornerR['c'] == -1: cornerR = None

        change = np.full((numC), 999, dtype=np.int16)
        for c in range(2, numC - 2):
            if avgSweep[c-2] > 0 and avgSweep[c+2] > 0:
                change[c] = avgSweep[c-2].astype(np.int16) - avgSweep[c+2].astype(np.int16)

        # look for walls
        cornerL = {'c':-1, 'z':0, 'd':0}
        cornerR = {'c':-1, 'z':0, 'd':0}
        for c in range(2, numC - 2):
            if change[c] != 999 and change[c] > cornerL['d']:
                cornerL = {'c':c, 'z':avgSweep[c], 'd':change[c]}
            c = numC - c - 1
            if change[c] != 999 and change[c] < cornerR['d']:
                cornerR = {'c':c, 'z':avgSweep[c], 'd':change[c]}
        
        # if cornerL['c'] > cornerR['c']:
        #     if change[cornerL['c']] > abs(change[cornerR['c']]):
        #         cornerR = None
        #     else:
        #         cornerL = None

        # if cornerL:
        #     c = cornerL['c']
        #     while c < (numC - 1) and change[c] > 6: c += 1
        #     cornerL['z'] = avgSweep[c]

        # if cornerR:
        #     c = cornerR['c']
        #     while c > 0 and change[c] < -6: c -= 1
        #     cornerR['z'] = avgSweep[c]

    ##### FRIG FACE #####

    elif cmd == CAN_STATUS_FRIG_POS:

        ##  Find the first and last column with a distance
        farL = 100
        farR = 0
        for c in range(numC):
            if farL == 100 and avgSweep[c] != 0:
                farL = c
            c = numC - c - 1
            if farR == 0 and avgSweep[c] != 0:
                farR = c

        # What is the closest column
        closest = avgSweep[avgSweep > 0].min()
        # print(f'closest: {closest}')

        # What is the furthest column
        furthest = avgSweep[avgSweep > 0].max()
        # print(f'furthest: {furthest}')

        # Scan from closest to farthest, determining which depth is most likely the frig based on comparision to the actual width (445 mm) of the frig.
        scans = []
        likelyIndex = None
        widthError = 1000
        for z in range(closest, furthest+1):
            scan = {'z':z, 'l':100, 'r':0, 'n':0}
            for c in range(farL, farR+1):
                if avgSweep[c] <= z:
                    if scan['l'] == 100: scan['l'] = c
                    scan['r'] = c
                    scan['n'] += 1
            scan['mmW'] = int((scan['r'] - scan['l']) * z * scannerUnit / FLH + 0.5)
            if abs(445 - scan['mmW']) < widthError:
                widthError = abs(445 - scan['mmW'])
                likelyIndex = len(scans)
            scans.append(scan)
        # print(f'scans: {scans}')
        # for scan in scans:
        #     print(f"{scan['z']:3} {scan['l']:3} {scan['r']:3} {scan['n']:3} {scan['mmW']:3}")
        
        # print(f'likelyIndex: {likelyIndex}')
        z = round(np.median(avgSweep[scans[likelyIndex]['l']:scans[likelyIndex]['r']+1]))
        if likelyIndex:
            cornerL = {'c':scans[likelyIndex]['l'], 'z':z}
            cornerR = {'c':scans[likelyIndex]['r'], 'z':z}

        # change = np.full((numC), 999, dtype=np.int16)
        # for c in range(2, numC - 2):
        #     if avgSweep[c-2] > 0 and avgSweep[c+2] > 0:
        #         change[c] = avgSweep[c-2].astype(np.int16) - avgSweep[c+2].astype(np.int16)
        #     elif avgSweep[c+2] > 0:
        #         change[c] = 12
        #     elif avgSweep[c-2] > 0:
        #         change[c] = -12

        # # look for box
        # cornerL = {'c':-1, 'z':0, 'd':0, 'edge':False}
        # cornerR = {'c':-1, 'z':0, 'd':0, 'edge':False}
        # for c in range(2, numC - 2):
        #     if change[c] != 999 and change[c] > cornerL['d']:
        #         cornerL = {'c':c, 'z':avgSweep[c], 'd':change[c]}
        #     c = numC - c - 1
        #     if change[c] != 999 and change[c] < cornerR['d']:
        #         cornerR = {'c':c, 'z':avgSweep[c], 'd':change[c]}
        
        # if cornerL['c'] > cornerR['c']:
        #     if change[cornerL['c']] > abs(change[cornerR['c']]):
        #         cornerR = None
        #     else:
        #         cornerL = None

        # if not cornerL or cornerL['c'] == -1:
        #     for c in range(2, numC - 2):
        #         if avgSweep[c] != 0:
        #             cornerL = {'c':c, 'z':avgSweep[c], 'edge':True}
        #             break
        # if not cornerR or cornerR['c'] == -1:
        #     for c in range(numC - 3, 1, -1):
        #         if avgSweep[c] != 0:
        #             cornerR = {'c':c, 'z':avgSweep[c], 'edge':True}
        #             break

        # if cornerL:
        #     c = cornerL['c']
        #     while c < (numC - 1) and change[c] > 6: c += 1
        #     cornerL['z'] = avgSweep[c]

        # if cornerR:
        #     c = cornerR['c']
        #     while c > 0 and change[c] < -6: c -= 1
        #     cornerR['z'] = avgSweep[c]

    ##### FRIG DOOR EDGE MORE PERCISELY #####

    elif cmd == CAN_STATUS_DOOR_POS:
        listZ = []
        leftC = 0
        rightC = 0
        for c in range(numC - 3, 1, -1):
            if avgSweep[c] != 0:
                listZ.append(avgSweep[c])
                leftC = c
                if not rightC: rightC = c
        if leftC:
            z = round(np.median(np.array(listZ, dtype=np.uint8)))
            cornerL = {'c':leftC, 'z':z}
            cornerR = {'c':rightC, 'z':z}

    ##### ALL MODES #####

    if cornerL: 
        cornerL['mmZ'] = int(cornerL['z'] * scannerUnit + 0.5) + WORLD_OFFSET_Z # scannerUnit mm
        cornerL['mmX'] = int((cornerL['c'] - (numC / 2)) * cornerL['z'] * scannerUnit / FLH + 0.5)
    if cornerR: 
        cornerR['mmZ'] = int(cornerR['z'] * scannerUnit + 0.5) + WORLD_OFFSET_Z # scannerUnit mm
        cornerR['mmX'] = int((cornerR['c'] - (numC / 2)) * cornerR['z'] * scannerUnit / FLH + 0.5)

    print(f'cornerL: {cornerL}')
    print(f'cornerR: {cornerR}')

    if globals.imagesToWrite & VISION_OUTPUT_DEPTH: 
        # img = cv2.applyColorMap(frameData, np.arange(255, -1, -1, dtype=np.uint8)) #cv2.COLORMAP_BONE)
        img = np.zeros((numR, numC, 3))
        for r in range(numR):
            for c in range(numC):
                if frameData[r, c] == 0:
                    img[r, c] = (0, 0, 48)
                else:
                    img[r, c, :] = 255 - frameData[r, c]
            
        cv2.line(img, (0, row1 - 1), (numC, row1 - 1), (128, 0, 0), 1) # horiz line
        cv2.line(img, (0, row2 + 1), (numC, row2 + 1), (128, 0, 0), 1) # horiz line

        if cmd in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS}:
            if cornerL:
                cv2.line(img, (cornerL['c'], row1-4), (cornerL['c'], row2+4), (0, 0, 128), 1) # left edge
        if cmd in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_FRIG_POS}:
            if cornerR:
                cv2.line(img, (cornerR['c'], row1-4), (cornerR['c'], row2+4), (0, 0, 128), 1) # right edge

        Path('/home/pi/tmp/images/depth.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.resize(img, (400, 400), interpolation = cv2.INTER_NEAREST))
        Path('/home/pi/tmp/images/depth.lock').unlink()

    if globals.imagesToWrite & VISION_OUTPUT_GRID: 
        img = np.zeros((numC, 256, 3), dtype=np.uint8)
        for c in range(numC):
            if avgSweep[c] > 0:
                for y in range(avgSweep[c] + 1):
                    img[c, y] = (128, 128, 128)
            else:
                for y in range(256):
                    img[c, y] = (64, 64, 64)

        # cv2.line(img, (0, row1 - 1), (numC, row1 - 1), (128, 0, 0), 1) # horiz line
        # cv2.line(img, (0, row2 + 1), (numC, row2 + 1), (128, 0, 0), 1) # horiz line

        if cmd in {CAN_STATUS_HALL_ENTER_POS} and globals.imagesToWrite & VISION_OUTPUT_GRAPH1:
            for c in range(2, numC - 2):
                z = 80 + change[c]
                if z >= 0 and z <= 225:
                    cv2.line(img, (80, c), (z, c), (128, 0, 0), 1)

        if cmd in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS}:
            if cornerL: 
                cv2.line(img, (cornerL['z'], cornerL['c']), (cornerL['z'] - 10, cornerL['c']), (0, 0, 255), 1) # vert line
        if cmd in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_FRIG_POS}:
            if cornerR: 
                cv2.line(img, (cornerR['z'], cornerR['c']), (cornerR['z'] - 10, cornerR['c']), (0, 0, 255), 1) # vert line
        if cmd in {CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS}:
            if cornerL and cornerR: 
                cv2.line(img, (cornerL['z'], cornerL['c']), (cornerR['z'], cornerR['c']), (0, 0, 255), 1) # horiz line

        Path('/home/pi/tmp/images/grid.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/grid.png', cv2.resize(np.rot90(img), (400, 400), interpolation = cv2.INTER_NEAREST))
        Path('/home/pi/tmp/images/grid.lock').unlink()

    ##### Broadcast the results #####

    if cornerL or cornerR:
        bytes = []
        loc = 'Left: '
        if cornerL:
            bytes.extend(cornerL['mmX'].to_bytes(2, 'big', signed=True))
            bytes.extend(cornerL['mmZ'].to_bytes(2, 'big'))
            loc += f"{cornerL['mmX']}, {cornerL['mmZ']} "
        else:
            bytes.extend([0, 0, 0, 0])
            loc += '?'
        loc += ' Right: '
        if cornerR:
            bytes.extend(cornerR['mmX'].to_bytes(2, 'big', signed=True))
            bytes.extend(cornerR['mmZ'].to_bytes(2, 'big'))
            loc += f"{cornerR['mmX']}, {cornerR['mmZ']} "
        else:
            bytes.extend([0, 0, 0, 0])
            loc += '?'

        can.send('scanner', cmd, False, bytes)
        red.set('caption', loc)

    state = STATE_IDLE

def idle():
    global usb

    usb.close()
    print('usb closed')
    usb = None

##### UTILITY #####

def avgNonZero(values, minCount=0):
    n = 0
    s = 0
    for v in values:
        if v > 0:
            n += 1
            s += v
    return s / n if n > minCount else 0

"""
    def compute_real_distance(val, unit):
    ret = 0
    if unit != 0:
        ret = val * unit
    else:
        ret = int(val) / 5.1
        ret *= ret

    if len(self.distances) == 0:
        for _ in range(8):
            self.distances.append(ret)
    else:
        self.distances.append(ret)
        self.distances = self.distances[-8:]
    return int(np.mean(self.distances))

"""

