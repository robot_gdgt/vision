"""
Obstacle and clear path detection
"""

import numpy as np
import numba as nb
import cv2
from time import sleep # process_time
import math
# import logging
import redis
# import os
from pathlib import Path
# import json

import pyrealsense2 as rs

import globals
# from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
from my_rs import rs_get_depth_frame, rs_get_frames, depth_scale, rs_set_ir_emitter, rs_set_ir_power
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

SCANX = 400 # cm
SCANXHALF = 200
GRIDZ = 400 # cm
LASERPWR = 150
# GROUNDY = 50
BOTTOMTHRESHOLD = 50 # mm
TOPTHRESHOLD = 250 # mm

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

STATE_IDLE = 0
STATE_WAITING = 1
STATE_PROCESSED = 2

CAMHEIGHT = globals.WORLD_OFFSET_Y

##### Vision #####

# Processing blocks
pc = rs.pointcloud()
# decimate = rs.decimation_filter()
# decimate.set_option(rs.option.filter_magnitude, 4)

@nb.jit(nb.types.Tuple((nb.uint16[:], nb.float32[:]))(nb.float32[:,:], nb.float32, nb.float32), nopython=True)
def processVerts(vs, a_sin, a_cos):
    counts = np.zeros(SCANX, dtype=np.uint16)
    means = np.zeros((SCANX), dtype=np.float32)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 100.0 + SCANXHALF + 0.5)
        if x < 0 or x >= SCANX: continue

        """
        x' = x * cos + y * sin
        y' = -x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector
        y0 = -v[1]
        z0 = v[2]

        # z = (z0 * a_cos + y0 * a_sin)
        z = (z0 * a_cos + y0 * a_sin) * 100.0 # cm

        # y = (-z0 * a_sin + y0 * a_cos)
        y = CAMHEIGHT + (-z0 * a_sin + y0 * a_cos) * 1000 # mm
        # print(x - SCANXHALF, z0, y0, z, y)
        # continue
        if y < BOTTOMTHRESHOLD or y > TOPTHRESHOLD: continue

        counts[x] += 1
        # delta = z - means[x]
        # means[x] += delta / counts[x]
        if means[x] == 0 or z < means[x]: means[x] = z

    return (counts, means)

@nb.jit(nb.uint8[:,:,:](nb.uint16[:], nb.float32[:]), nopython=True)
def scan2out(counts, means):
    out = np.zeros((SCANX, GRIDZ, 3), dtype=np.uint8)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(SCANX):
        if counts[x] < 10: continue
        if means[x] >= GRIDZ: continue 
        out[x, int(means[x] + 0.5)] = CLASSIFY_COLORS[1]

    return out

##### INIT #####

def init():
    # global state
    can.del_filter('obst_detect', 'all', 'all')
    # can.add_filter('obst_detect', CAN_STATUS_HEADLIGHT, 0x7FF)

    rs_set_ir_emitter(True)
    rs_set_ir_power(100)

    globals.cam_angle = 30 # degrees down
    can.send('obst_detect', CAN_CMD_VISION, False, [VISION_D435_ANGLE, globals.cam_angle])
    sleep(2)
    globals.cam_angle_rads = math.radians(globals.cam_angle)
    globals.cam_angle_sin = math.sin(globals.cam_angle_rads)
    globals.cam_angle_cos = math.cos(globals.cam_angle_rads)

    globals.imagesToWrite = VISION_OUTPUT_RGB | VISION_OUTPUT_DEPTH | VISION_OUTPUT_GRID

    # state = STATE_IDLE

##### TICK #####

def tick():
    # return true to run again
    # global state

    if globals.imagesToWrite & VISION_OUTPUT_RGB: 
        depth_frame, color_frame = rs_get_frames()

        # Validate that both frames are valid
        if not depth_frame or not color_frame:
            return STATE_WAITING

        # color_image = np.asanyarray(color_frame.get_data())
        Path('/home/pi/tmp/images/rgb.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/rgb.jpg', np.asanyarray(color_frame.get_data()))
        Path('/home/pi/tmp/images/rgb.lock').unlink()

    else:
        depth_frame = rs_get_depth_frame()

        # Validate that frame is valid
        if not depth_frame:
            return STATE_WAITING

    if globals.imagesToWrite & VISION_OUTPUT_DEPTH: 
        # depth_image = np.asanyarray(depth_frame.get_data())
        Path('/home/pi/tmp/images/depth.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(np.asanyarray(depth_frame.get_data()), alpha=0.03), cv2.COLORMAP_JET))
        Path('/home/pi/tmp/images/depth.lock').unlink()

    # Pointcloud data to array
    points = pc.calculate(depth_frame)
    verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz

    counts, means = processVerts(verts, globals.cam_angle_sin, globals.cam_angle_cos)

    # for x in range(SCANX):
    #     # print(f'{x} {z} {counts[x, z]} {means[x, z]} {m2s[x, z]} {grid[x, z, 0]} {grid[x, z, 1]}')
    #     print(f'{x - SCANXHALF:>4d} {counts[x]:>4d} {means[x]:>6.2f}')

    if globals.imagesToWrite & VISION_OUTPUT_GRID: 
        out = scan2out(counts, means)
        Path('/home/pi/tmp/images/grid.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/grid.png', cv2.resize(np.rot90(out), (SCANX, GRIDZ), interpolation = cv2.INTER_NEAREST))
        Path('/home/pi/tmp/images/grid.lock').unlink()

    return 0 #STATE_PROCESSED
