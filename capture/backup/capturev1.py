"""
Capture image data from RealSense camera and store in POSIX IPC shared memory
"""

# import math
from time import sleep
import cv2
import numpy as np
import pyrealsense2 as rs
import numba
import logging
import logger
import redis
import sys
from os import path
import json
from speak_direct import *
from gdgt_platform import *
from can_ids import *
# from var_dump import var_dump as vd

from shm.writer import SharedMemoryFrameWriter

FILE = path.splitext(path.basename(__file__))[0]

logger.init(FILE, logging.INFO)

# logging.info(sys.path)
# sys.exit()

# App state object class
class AppState:
    def __init__(self):
        self.laser = 180
        self.state = CAN_CMD_PGM_RUNNING

state = AppState()

##### Vision #####

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 15)

# Start streaming
profile = pipeline.start(config)

# Processing blocks
pc = rs.pointcloud()

device = profile.get_device()
# depth_sensor = device.query_sensors()[0]
depth_sensor = profile.get_device().first_depth_sensor()
depth_sensor.set_option(rs.option.laser_power, state.laser)

decimate = rs.decimation_filter()
decimate.set_option(rs.option.filter_magnitude, 4)

# Prime the pump (create the depth_image and color_image variables)
while True:
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    if depth_frame and color_frame:
        break

# depth_frame = decimate.process(depth_frame)

depth_image = np.asanyarray(depth_frame.get_data())
color_image = np.asanyarray(color_frame.get_data())

if __name__ == '__main__':

	##### SETUP #####
    try:
        red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        raise Exception('Is the redis server running?')

    # Tell the CAN proxy what packets we want
    red.rpush('CANfilter', json.dumps({
        'action': 'add',
        'can_id': CAN_CMD_HALT,
        'can_mask': 0x7FF,
        'req_by': FILE
    }))
    red.rpush('CANfilter', json.dumps({
        'action': 'add',
        'can_id': CAN_CMD_VISION,
        'can_mask': 0x7FF,
        'req_by': FILE
    }))

    shm_depth = None
    shm_color = None
    shm_verts = None

    try:

        shm_depth = SharedMemoryFrameWriter('depth_frame')
#         red.set('shm_depth', shm_depth.shm_name)
        logging.info(f'shm_depth: {shm_depth.shm_name}')

        shm_color = SharedMemoryFrameWriter('color_frame')
#         red.set('shm_color', shm_color.shm_name)
        logging.info(f'shm_color: {shm_color.shm_name}')

        shm_verts = SharedMemoryFrameWriter('verts')
#         red.set('shm_verts', shm_verts.shm_name)
        logging.info(f'shm_verts: {shm_verts.shm_name}')

        speak_direct(red, 'depth capture running')

        ##### LOOP #####
        while True:

            if state.state == CAN_CMD_PGM_RUNNING:

                while True:
                    frames = pipeline.wait_for_frames()
                    depth_frame = frames.get_depth_frame()
                    color_frame = frames.get_color_frame()
                    if depth_frame and color_frame:
                        break

    #             depth_frame = decimate.process(depth_frame)

                depth_image = np.asanyarray(depth_frame.get_data())
                shm_depth.add(depth_image)
                color_image = np.asanyarray(color_frame.get_data())
                shm_color.add(color_image)

                # Pointcloud data to array
                points = pc.calculate(depth_frame)
                verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz
    #             print(numba.typeof(verts))
    #             quit()
                shm_verts.add(verts)

            else:
                sleep(2)

            CANrx = red.lpop(FILE)
            if CANrx is not None:
                CANrx = json.loads(CANrx)
                if CANrx['can_id'] == CAN_CMD_VISION:
                    if CANrx['data'][0] == CAN_CMD_VISION_PGM:
                        if CANrx['data'][1] == CAN_CMD_VISION_PGM_CAPTURE:
                            state.state = CANrx['data'][2]
                    elif CANrx['data'][0] == CAN_CMD_D435_LASER:
                        state.laser = CANrx['data'][1] * 2
                        logging.info(f'state.laser: {state.laser}')
                        depth_sensor.set_option(rs.option.laser_power, state.laser)
                elif CANrx['can_id'] == CAN_CMD_HALT:
                    logging.info('shutdown')
                    state.state = CAN_CMD_PGM_QUIT

#         dt = time.time() - start
#         print(10.0 / dt)

            if state.state == CAN_CMD_PGM_QUIT:
                break

    except KeyboardInterrupt:
        logging.info('KeyboardInterrupt')

    if shm_depth: shm_depth.release()
    if shm_color: shm_color.release()
    if shm_verts: shm_verts.release()
