"""
Capture image data from RealSense camera and perform obst detection
"""

import cv2
import numpy as np
import pyrealsense2 as rs
import numba
from numba import jit

import json
import logger
import logging
import math
from os import path, rename, chmod, mkdir
import redis
import subprocess
import sys
from time import sleep, time

import can_bus_thread
from status_led import StatusLED
from speak_direct_can import speak_direct
from gdgt_platform import *
from can_ids import *
from tts_groups import *
# from var_dump import var_dump as vd

hbLED = StatusLED(LED_PIN, True)

FILE = path.splitext(path.basename(__file__))[0]

logger.init(None, logging.INFO)

CAMHEIGHT = 59  # Height in cm to center of camera
GRIDX = 60
GRIDXHALF = 30
GRIDZ = 60
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to CAMHEIGHT
GRIDSCALE = 12   # Pixels per grid cell in output image
LASERPWR = 150
GROUNDY = 50
ZTHRESHOLD = 2

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

OUT_GRID = 0
OUT_COLORMAP = 1

# App state object class
class AppState:
    def __init__(self):
        self.laser = 180
        self.cameraAngle = 30
        self.output = 0
        self.state = CAN_CMD_PGM_RUNNING

state = AppState()

CAM_ANGLE_COS = math.cos(math.radians(state.cameraAngle))
CAM_ANGLE_SIN = math.sin(math.radians(state.cameraAngle))

##### Vision #####

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()
config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 15)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 15)

# Start streaming
profile = pipeline.start(config)

# Processing blocks
pc = rs.pointcloud()

device = profile.get_device()
# depth_sensor = device.query_sensors()[0]
depth_sensor = profile.get_device().first_depth_sensor()
depth_sensor.set_option(rs.option.laser_power, state.laser)

decimate = rs.decimation_filter()
decimate.set_option(rs.option.filter_magnitude, 4)

# Prime the pump (create the depth_image and color_image variables)
while True:
    frames = pipeline.wait_for_frames()
    depth_frame = frames.get_depth_frame()
    color_frame = frames.get_color_frame()
    if depth_frame and color_frame:
        break

# depth_frame = decimate.process(depth_frame)

depth_image = np.asanyarray(depth_frame.get_data())
color_image = np.asanyarray(color_frame.get_data())

##### Obst Detection #####

grid = np.empty((GRIDX, GRIDZ, 2), dtype=np.uint8)
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)
accum = np.empty((GRIDX, GRIDZ, 7), dtype=np.uint16)
sums = np.empty((GRIDX, GRIDZ), dtype=np.uint32)
dist = np.empty((GRIDX, GRIDZ, 128), dtype=np.uint8)

@jit(nopython=True)
def verts2accum(vs:numba.float32[:,:], a:numba.uint16[:,:,:], s:numba.uint32[:,:], d:numba.uint8[:,:,:], a_sin:numba.float32, a_cos:numba.float32, CAMHEIGHT:numba.uint8):
    a.fill(0)
    s.fill(0)
    d.fill(0)
    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 25.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector 30°
        y0 = v[1]
        z0 = v[2]

        z = int((z0 * a_cos - y0 * a_sin) * 25.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        y = CAMHEIGHT - int((z0 * a_sin + y0 * a_cos) * 100) + GROUNDY
#         print(x, z, y)
        if y > 127: continue
        if y < 1: y = 1
        # Add y to sum of previous y
        a[x, z, 0] += 1
        s[x, z] += y
        # Record lowest y
        if a[x, z, 1] == 0 or y < a[x, z, 1]: a[x, z, 1] = y
        # Record highest y
        if y > a[x, z, 3]: a[x, z, 3] = y
        # Inc dist counter
        if d[x, z, y] < 255: d[x, z, y] += 1

    #After all the points are tallied we update the other columns in accum
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if a[x, z, 0] == 0: continue
            a[x, z, 2] = int(s[x, z] / a[x, z, 0] + 0.5)
            min = 0
            mode = 0
            mode_cnt = 0
            max = 0
            for i in range(128):
                if d[x, z, i] > 1:
                    if min == 0: min = i
                    max = i
                    if d[x, z, i] > mode_cnt:
                        mode = i
                        mode_cnt = d[x, z, i]
            a[x, z, 4] = min
            a[x, z, 5] = mode
            a[x, z, 6] = max

@jit(nopython=True)
def accum2grid(a, g):
    g.fill(0)
    for x in range(GRIDX):
        for z in range(GRIDZ):
            mode = a[x, z, 5]
            if mode == 0: continue
            low = a[x, z, 4]
            high = a[x, z, 6]
            if mode - low <= ZTHRESHOLD and high - mode <= ZTHRESHOLD:
                g[x, z, 0] = 1
            elif mode - low > ZTHRESHOLD and high - mode > ZTHRESHOLD:
                g[x, z, 0] = 3
            else:
                g[x, z, 0] = 2
            g[x, z, 1] = mode

@jit(nopython=True)
def gridFlow(g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != GRID_HORIZ: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = GROUNDY
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == GRID_NAVIGABLE:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0:
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = GRID_NAVIGABLE
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] != GRID_NAVIGABLE and g[x - 1, z, 0] == GRID_NAVIGABLE and g[x + 1, z, 0] == GRID_NAVIGABLE and g[x, z - 1, 0] == GRID_NAVIGABLE and g[x, z + 1, 0] == GRID_NAVIGABLE:
                g[x, z, 0] = GRID_NAVIGABLE

@jit(nopython=True)
def obstaclePadding(g):
    padding = (
        # gdgt: 44 cm wide
        (-1, 6), (0, 6), (1, 6),
        (-3, 5), (-2, 5), (-1, 5), (0, 5), (1, 5), (2, 5), (3, 5),
        (-4, 4), (-3, 4), (-2, 4), (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4),
        (-5, 3), (-4, 3), (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3),
        (-5, 2), (-4, 2), (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        (-6, 1), (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1),
        (-6, 0), (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
        (-6, -1), (-5, -1), (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1),
        (-5, -2), (-4, -2), (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2), (4, -2), (5, -2),
        (-5, -3), (-4, -3), (-3, -3), (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3), (3, -3), (4, -3), (5, -3),
        (-4, -4), (-3, -4), (-2, -4), (-1, -4), (0, -4), (1, -4), (2, -4), (3, -4), (4, -4),
        (-3, -5), (-2, -5), (-1, -5), (0, -5), (1, -5), (2, -5), (3, -5),
        (-1, -6), (0, -6), (1, -6),
#         (-1, 4), (0, 4), (1, 4),
#         (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3),
#         (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2),
#         (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1),
#         (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0),
#         (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1),
#         (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2),
#         (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3),
#         (-1, -4), (0, -4), (1, -4),
    )
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] == GRID_HORIZ or g[x, z, 0] == GRID_TRANS or g[x, z, 0] == GRID_VERT:
                for p in padding:
                    if x + p[0] < 0 or x + p[0] >= GRIDX or z + p[1] < 0 or z + p[1] >= GRIDZ: continue
                    if g[x + p[0], z + p[1], 0] == GRID_NAVIGABLE:
                        g[x + p[0], z + p[1], 0] = GRID_PADDING

@jit(nopython=True)
def findPath2(g):
    dead_end = False
    first_obst = -1
    x = GRIDXHALF
    heading = 0
    heading_lock = False
    for z in range(GRIDZ):
#         print(x, z, g[x, z, 0])
        if g[x, z, 0] != GRID_NAVIGABLE:
            if first_obst == -1: first_obst = z
            new_x = -1
            for i in range(1, 10):
                if x - i >= 0 and g[x - i, z, 0] == GRID_NAVIGABLE:
                    new_x = x - i
                    break
                elif x + i < GRIDX and g[x + i, z, 0] == GRID_NAVIGABLE:
                    new_x = x + i
                    break
            if new_x == -1:
                dead_end = True
                break
            else:
                x = new_x
        g[x, z, 0] = GRID_PATH
        if z == 0: continue
        h = float(x - GRIDXHALF) / z
        if not heading_lock:
            if heading == 0:
                heading = h
            elif heading > 0:
                if h > heading: heading = h
                else: heading_lock = True
            elif heading < 0:
                if h < heading: heading = h
                else: heading_lock = True
    if first_obst == -1:
        first_obst = GRIDZ

    return (first_obst, int(math.degrees(math.atan(heading)) + 0.5), dead_end)

@jit(nopython=True)
def grid2out(grid, out):
    out.fill(0)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == GRID_NO_DATA: continue
            out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]

def canBusMsgRecd(msg):
    print(msg)

    if msg['can_id'] == CAN_CMD_VISION: # any alert
        if msg['data'][0] == CAN_CMD_VISION_PGM:
            state.state = msg['data'][1]
        elif msg['data'][0] == CAN_CMD_D435_LASER:
            state.laser = msg['data'][1] * 2
            logging.info(f'state.laser: {state.laser}')
            depth_sensor.set_option(rs.option.laser_power, state.laser)
            red.set('laser', state.laser)
        elif msg['data'][0] == CAN_CMD_D435_ANGLE:
            state.cameraAngle = msg['data'][1]
            logging.info(f'state.cameraAngle: {state.cameraAngle}')
            CAM_ANGLE_COS = math.cos(math.radians(state.cameraAngle))
            logging.info(f'CAM_ANGLE_COS: {CAM_ANGLE_COS}')
            CAM_ANGLE_SIN = math.sin(math.radians(state.cameraAngle))
            logging.info(f'CAM_ANGLE_SIN: {CAM_ANGLE_SIN}')
        elif msg['data'][0] == CAN_CMD_VISION_OUTPUT:
            state.output = msg['data'][1]

    elif msg['can_id'] == CAN_CMD_HALT:
        logging.info('shutdown')
        state.state = CAN_CMD_HALT

##### SETUP #####

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')
red.set('camheight', CAMHEIGHT)

can_bus_thread.msgRecdCallback = canBusMsgRecd
can_filters = [
    {"can_id": CAN_CMD_HALT, "can_mask": 0x7FF, "extended": False},
    {"can_id": CAN_CMD_VISION, "can_mask": 0x7FF, "extended": False},
]
can_bus_thread.set_filters(can_filters)

speak_direct('depth capture running')

try:
    mkdir('/var/tmp/images')
    chmod('/var/tmp/images', 0o777)
except:
    pass

# Clear the image queue
while red.lpop('img_req') is not None:
    pass

hbLED.heartbeat()

##### LOOP #####

try:

    while True:

        if state.state == CAN_CMD_PGM_RUNNING:

#             h = input('CAMHEIGHT: ')
#             if h != '':
#                 CAMHEIGHT = int(h)
#
#             a = input('Cam angle: ')
#             if a != '':
#                 state.cameraAngle = int(a)
#                 logging.info(f'state.cameraAngle: {state.cameraAngle}')
#                 CAM_ANGLE_COS = math.cos(math.radians(state.cameraAngle))
#                 logging.info(f'CAM_ANGLE_COS: {CAM_ANGLE_COS}')
#                 CAM_ANGLE_SIN = math.sin(math.radians(state.cameraAngle))
#                 logging.info(f'CAM_ANGLE_SIN: {CAM_ANGLE_SIN}')

            now = time()

            img_req = red.lpop('img_req')
            if img_req is not None:
                (src, n) = img_req.split('_')
                state.output = int(src)
            else:
                state.output = 0

            laser = red.get('laser')
            if laser is not None:
                laser = int(int(laser) * 360 / 4);
                if state.laser != laser:
                    state.laser = laser
                    depth_sensor.set_option(rs.option.laser_power, state.laser)
                    print(f'Laser set to {laser}')

            camh = red.get('camheight')
            if camh is not None:
                CAMHEIGHT = int(camh);

            while True:
                frames = pipeline.wait_for_frames()
                depth_frame = frames.get_depth_frame()
                if state.output == CAN_CMD_VISION_OUTPUT_RGB:
                    color_frame = frames.get_color_frame()
                if depth_frame and (state.output != CAN_CMD_VISION_OUTPUT_RGB or color_frame):
                    break

#             depth_frame = decimate.process(depth_frame)

            if state.output == CAN_CMD_VISION_OUTPUT_DEPTH:
                depth_image = np.asanyarray(depth_frame.get_data())
                cv2.imwrite('/var/tmp/images/tmp.jpg', cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET))
                chmod('/var/tmp/images/tmp.jpg', 0o777)
                rename('/var/tmp/images/tmp.jpg', f'/var/tmp/images/{img_req}.jpg')
                print(f'/var/tmp/images/{img_req}.jpg')
            elif state.output == CAN_CMD_VISION_OUTPUT_RGB:
                color_image = np.asanyarray(color_frame.get_data())
                cv2.imwrite('/var/tmp/images/tmp.jpg', color_image)
                chmod('/var/tmp/images/tmp.jpg', 0o777)
                rename('/var/tmp/images/tmp.jpg', f'/var/tmp/images/{img_req}.jpg')
                print(f'/var/tmp/images/{img_req}.jpg')

            # Pointcloud data to array
            points = pc.calculate(depth_frame)
            verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz

            verts2accum(verts, accum, sums, dist, CAM_ANGLE_SIN, CAM_ANGLE_COS, CAMHEIGHT)

            accum2grid(accum, grid)

            gridFlow(grid)

            obstaclePadding(grid)

            first_obst, heading, dead_end = findPath2(grid)

#                 first_obst = int(input('first_obst: '))
#                 heading = int(input('heading: '))
#                 dead_end = int(input('dead_end: '))

            can_bus_thread.send({
                'can_id': CAN_TS_VIS_OBST,
                'data': [first_obst, int(128 + heading), 1 if dead_end else 0],
            })

            dt = time() - now
#             print(f"{int(1.0/dt)} FPS Obst: {first_obst} Head: {heading} Deadend: {dead_end}")

            red.set('obst', json.dumps({
                'fps': int(1.0/dt),
                'dist': first_obst,
                'dir': heading,
                'dead_end': dead_end
            }))

            if state.output == CAN_CMD_VISION_OUTPUT_GRID:
                grid2out(grid, out)
                cv2.imwrite('/var/tmp/images/tmp.jpg', cv2.resize(np.rot90(out), (480, 480), interpolation = cv2.INTER_NEAREST))
                chmod('/var/tmp/images/tmp.jpg', 0o777)
                rename('/var/tmp/images/tmp.jpg', f'/var/tmp/images/{img_req}.jpg')
                print(f'/var/tmp/images/{img_req}.jpg')

#             if state.output > 0:
#                 f = open(f'/var/tmp/images/{img_req}.json', 'w')
#                 j = json.dumps({
#                     'dist': first_obst,
#                     'dir': heading,
#                     'dead_end': dead_end
#                 })
#                 print(j)
#                 f.write(json.dumps({
#                     'fps': int(1.0/dt),
#                     'dist': first_obst,
#                     'dir': heading,
#                     'dead_end': dead_end
#                 }))
#                 f.close()
        else:
            sleep(2)

#         dt = time() - now
#         print(f"{int(1.0/dt)} FPS")

        # Blink the LED
        hbLED.tickle()

        if state.state == CAN_CMD_PGM_QUIT or state.state == CAN_CMD_HALT:
            break

except KeyboardInterrupt:
    logging.info('KeyboardInterrupt')

subprocess.run("sudo ip link set can0 down", shell=True)

if state.state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)
