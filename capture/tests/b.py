"""
Capture image data from RealSense camera and store in POSIX IPC shared memory
"""

# import math
from time import sleep, time
import cv2
import numpy as np
import pyrealsense2 as rs
# from var_dump import var_dump as vd
import numba
import logging
import redis
import sys

from shm.writer import SharedMemoryFrameWriter

logging.basicConfig(
#     filename='test.log',
    level=logging.INFO,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%H:%M:%S'
)
logging.info('START')

# App state object class
class AppState:
    def __init__(self):
        self.laser = 180

state = AppState()


if __name__ == '__main__':

	##### SETUP #####
    try:
        red = redis.Redis(unix_socket_path='/tmp/redis.sock', decode_responses=True)
        red.get('dummy')
    except redis.exceptions.ConnectionError:
        sys.exit('Is the redis server running?')

    # Set the redis keys to their default values.
    if red.get('laser') == None: red.set('laser', state.laser)

    state.laser = int(red.get('laser'))

    try:

        shm_depth = SharedMemoryFrameWriter()
        red.set('shm_depth', shm_depth.shm_name)
        logging.info(f'shm_depth: {shm_depth.shm_name}')

        shm_color = SharedMemoryFrameWriter()
        red.set('shm_color', shm_color.shm_name)
        logging.info(f'shm_color: {shm_color.shm_name}')

        shm_verts = SharedMemoryFrameWriter()
        red.set('shm_verts', shm_verts.shm_name)
        logging.info(f'shm_verts: {shm_verts.shm_name}')

#         start = time.time()
#         for i in range(10):

        while True:

            depth_image = np.empty((480, 640, 3), dtype=np.uint8)
            depth_image.fill(192)
            shm_depth.add(depth_image)
            color_image = np.empty((480, 640, 3), dtype=np.uint8)
            color_image.fill(64)
            shm_color.add(color_image)

            sleep(0.5)

#         dt = time.time() - start
#         print(10.0 / dt)

    except KeyboardInterrupt:
        pass

    if shm_depth: shm_depth.release()
    if shm_color: shm_color.release()
    if shm_verts: shm_verts.release()
