import cv2
import logging

from shm.reader import SharedMemoryFrameReader

logging.basicConfig(
#     filename='test.log',
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%H:%M:%S'
)


if __name__ == '__main__':

    shm_r = None

    try:

        shm_r = SharedMemoryFrameReader('/gdgt2')

        while True:
            f, c = shm_r.get()
            if f is not None:
                logging.info(f'new frame {c}')
                cv2.imshow('frame', f)
            if cv2.waitKey(1) & 0xFF == ord('q'):
                break
    except KeyboardInterrupt:
        pass

    if shm_r: shm_r.release()
