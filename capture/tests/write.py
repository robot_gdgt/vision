import numpy as np
import time
import logging

from shm.writer import SharedMemoryFrameWriter

logging.basicConfig(
#     filename='test.log',
    level=logging.DEBUG,
    format='%(asctime)s %(levelname)s: %(message)s',
    datefmt='%H:%M:%S'
)


if __name__ == '__main__':

    a = np.empty((480, 640, 3), dtype=np.uint8)

    try:

        shm_w = SharedMemoryFrameWriter('/gdgt3')

        gray = 0
        while True:
            a.fill(gray)
            gray = (gray + 16) % 256
            shm_w.add(a)
            time.sleep(0.2)
    except KeyboardInterrupt:
        pass

    shm_w.release()
