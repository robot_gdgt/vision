"""
Vision - Door Detect
"""

# import pyrealsense2 as rs
import numpy as np
import cv2
from time import sleep, time
import math
import logging
import redis
import os
from pathlib import Path
import json

import globals
from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

os.makedirs('/home/pi/tmp/images', exist_ok=True)

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

kernel = np.ones((3, 3), np.uint8)

def init():

    print(f'frig_threshold: {globals.frig_threshold}')

    # Tell the CAN proxy what packets we want
    # can.del_filter('main', 'all', 'all')
    # can.add_filter('main', CAN_CMD_HALT, 0x7FF)
    # can.add_filter('main', CAN_CMD_VISION, 0x7FF)

    globals.cam_angle = 30

    can.send('door', CAN_CMD_VISION, False, [VISION_D435_ANGLE, 90 - globals.cam_angle])

def tick():
    _, color_frame = rs_get_frames()

    # Validate that both frames are valid
    if not color_frame:
        return

    # Convert images to numpy arrays
    color_image = np.asanyarray(color_frame.get_data())
    # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
    if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)

    gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    # gray_image = cv2.bilateralFilter(gray_image, 11, 17, 17)
    gray_image = cv2.blur(gray_image, (5, 5))

    # cv2.line(gray_image, (0, 240), (639, 240), 128, 1) # horiz line
    # cv2.line(gray_image, (320, 0), (320, 479), 128, 1) # vert line

    ######################
    # Find the black box #
    ######################

    fMask = cv2.inRange(gray_image, 0, globals.frig_threshold)

    # cv2.line(gray_image, (0, 240), (639, 240), 128, 1) # horiz line
    # cv2.line(gray_image, (320, 0), (320, 479), 128, 1) # vert line

    fMask = cv2.erode(fMask, kernel, iterations=1)
    # fMask = cv2.dilate(fMask, kernel, iterations=1)
    if globals.imagesToWrite & VISION_OUTPUT_MASK: cv2.imwrite('/home/pi/tmp/images/fmask.png', fMask)

    # _, thresh = cv2.threshold(fMask, 128, 255, 0)
    contours, _ = cv2.findContours(fMask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)

    if len(contours) > 0:
        # find the biggest countour (c) by the area
        c = max(contours, key = cv2.contourArea)
        crX, crY, crW, crH = cv2.boundingRect(c)
        print(f"crX: {crX} crY: {crY} crW: {crW} crH: {crH}")

        # draw the biggest contour (c) in white
        cv2.rectangle(gray_image, (crX, crY), (crX + crW, crY + crH), 255, 1)

        """
        0-1 left x unsigned pixel
        2-3 width x unsigned pixel
        """
        bytes = []
        bytes.extend(crX.to_bytes(2, 'big', signed=False))
        bytes.extend(crW.to_bytes(2, 'big', signed=False))
        can.send('door', CAN_TS_FRIG, False, bytes)
        red.set('door', json.dumps({
            'crX': crX,
            'crW': crW
        }))

    if globals.imagesToWrite & VISION_OUTPUT_GRAY: cv2.imwrite('/home/pi/tmp/images/gray.jpg', gray_image)
