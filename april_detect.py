"""
Vision - April Tag Detect
"""

import numpy as np
import cv2
import apriltag
from time import sleep, time
import math
# import logging
import redis
# import os
from pathlib import Path
# import json

import globals
from my_rs import rs_get_frames
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
# from var_dump import var_dump as vd

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

def init():
    globals.imagesToWrite = VISION_OUTPUT_RGB

    globals.cam_angle = 0  # horiz
    can.send('can_detect', CAN_CMD_VISION, False, [VISION_D435_ANGLE, globals.cam_angle])
    sleep(1.0)
    globals.cam_angle_rads = math.radians(globals.cam_angle)
    globals.cam_angle_sin = math.sin(globals.cam_angle_rads)
    globals.cam_angle_cos = math.cos(globals.cam_angle_rads)

def tick():
    color_frame = None

    # Turn on ref LEDs
    can.send('frig', CAN_GRIP_REF_LED, False, [1])
    sleep(0.5)

    # Validate that both frames are valid
    while not color_frame:
        _, color_frame = rs_get_frames()

    # Turn off ref LEDs
    can.send('frig', CAN_GRIP_REF_LED, False, [0])
    sleep(0.5)

    color_image = np.asanyarray(color_frame.get_data())
    gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
    # if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    # return

    # cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
    # cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

    ######################
    # Find the april tag #
    ######################
    wX = 0
    wY = 0
    wZ = 0

    image = cv2.imread(args["image"])
    gray = cv2.cvtColor(image, cv2.COLOR_BGR2GRAY)

    # define the AprilTags detector options and then detect the AprilTags
    # in the input image
    print("[INFO] detecting AprilTags...")
    options = apriltag.DetectorOptions(families="tag36h11")
    detector = apriltag.Detector(options)
    results = detector.detect(gray)
    print("[INFO] {} total AprilTags detected".format(len(results)))

    # loop over the AprilTag detection results
    for r in results:
        # extract the bounding box (x, y)-coordinates for the AprilTag
        # and convert each of the (x, y)-coordinate pairs to integers
        (ptA, ptB, ptC, ptD) = r.corners
        ptB = (int(ptB[0]), int(ptB[1]))
        ptC = (int(ptC[0]), int(ptC[1]))
        ptD = (int(ptD[0]), int(ptD[1]))
        ptA = (int(ptA[0]), int(ptA[1]))
        # draw the bounding box of the AprilTag detection
        cv2.line(image, ptA, ptB, (0, 255, 0), 2)
        cv2.line(image, ptB, ptC, (0, 255, 0), 2)
        cv2.line(image, ptC, ptD, (0, 255, 0), 2)
        cv2.line(image, ptD, ptA, (0, 255, 0), 2)
        # draw the center (x, y)-coordinates of the AprilTag
        (cX, cY) = (int(r.center[0]), int(r.center[1]))
        cv2.circle(image, (cX, cY), 5, (0, 0, 255), -1)
        # draw the tag family on the image
        tagFamily = r.tag_family.decode("utf-8")
        cv2.putText(image, tagFamily, (ptA[0], ptA[1] - 15),
            cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 2)
        print("[INFO] tag family: {}".format(tagFamily))
    # show the output image after AprilTag detection
    cv2.imshow("Image", image)
    cv2.waitKey(0)

    if wZ:
        """
        0 grip x high
        1 grip x low
        2 grip y high
        3 grip y low
        4 grip z high
        5 grip z low
        """
        data = []
        data.extend(int.to_bytes(wX, 2, 'big', signed=True)) # mm
        data.extend(int.to_bytes(wY, 2, 'big', signed=True)) # mm
        data.extend(int.to_bytes(wZ, 2, 'big')) # mm
        can.send('can_detect', CAN_STATUS_GRIP_POS, False, data)
        red.set('caption', f"Grip: {wX}, {wY}, {wZ}")
    else:
        can.send('can_detect', CAN_STATUS_GRIP_POS, False, [0, 0, 0, 0, 0, 0])
        red.set('caption', f"Grip: ?, ?, ?")

    if globals.imagesToWrite & VISION_OUTPUT_RGB: 
        Path('/home/pi/tmp/images/rgb.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
        Path('/home/pi/tmp/images/rgb.lock').unlink()
