import cv2
import numpy as np
import os
from pathlib import Path
import serial # https://pyserial.readthedocs.io/en/latest/pyserial_api.html
from struct import unpack
from time import sleep

FRAME_HEAD = b"\x00\xFF"
FRAME_TAIL = b"\xDD"
FRAME_TAIL_INT = 0xDD

os.makedirs('/home/pi/tmp/images', exist_ok=True)

usb = serial.Serial('/dev/ttyUSB0', 115200, 8, 'N', 1, timeout=0.1) # usbreset
print(f'usb.is_open: {usb.is_open}')

def uart_readBytes():
    # return usb.read(256)
    return usb.read_until(FRAME_TAIL)

def uart_hasData():
    return usb.in_waiting

def uart_sendCmd(cmd):
    usb.write(cmd)

# uart_sendCmd(b"AT+BAUD=2\r")
# sleep(0.1)
uart_sendCmd(b"AT+ISP=0\r")
sleep(0.1)
uart_sendCmd(b"AT+BINN=2\r")
sleep(0.1)
uart_sendCmd(b"AT+UNIT=9\r")
sleep(0.1)
uart_sendCmd(b"AT+FPS=5\r")
sleep(0.1)
uart_sendCmd(b"AT+DISP=7\r")
sleep(0.1)
uart_sendCmd(b"AT+ISP=1\r")
sleep(2)

"""
Packet format:
head  length                              rc cc ID ID       data data cs end
00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 ... -3 -2 -1
00 FF XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX XX ... XX XX DD

| header(2B) | len(2B) | command(1B) | output_mode(1B)     | sensor temp(1B) | driver temp(1B) |
| 0xCC  0xA0 |  >=18   |     0xXX    | 0:Depth only, 1:+IR |      uint8      |      uint8      |
------------------------------------------------------------------------------------------------
|    exposure time(4B)    | errcode(1B) | reserved1(1B) | res_rows(1B) | res_cols(1B) |
|[23:20][19:12][11:8][7:0]|             |      0x00     |     uint8    |     uint8    |
---------------------------------------------------------------------------------------
| Frame ID(2B) | ISP ver(1B) | reserved3(1B) | frame data((len-16)B) | checksum(1B) | tail(1B) |
|    [11:0]    |    0x23     |      0x00     | xxxxxxxxxxxxxxxxxxxxx | sum of above |   0xDD   |
"""

rawData = b''
prevID = -1
while True:
    if not uart_hasData():
        continue
    rawData += uart_readBytes()
    print('.', end='')
    idx = rawData.find(FRAME_HEAD)
    if idx < 0:
        rawData = rawData[-1:]
        continue
    if idx > 0:
        rawData = rawData[idx:]

    if len(rawData) < 647: # minimum length is 625 data + 20 header + 2 tail bytes
        continue

    # print(rawData)
    # check data length 2Byte
    dataLen = unpack("H", rawData[2:4])[0]
    frameLen = len(FRAME_HEAD) + 2 + dataLen + 2
    # print(f"dataLen: {dataLen} frameLen: {frameLen} len(rawData): {len(rawData)}")
    if len(rawData) < frameLen: # We need more data to complete the frame
        continue

    print(f"\ndataLen: {dataLen} frameLen: {frameLen} len(rawData): {len(rawData)}")

    # print('rawData')
    # print(rawData.hex())

    # frame is now complete
    frameData = rawData[:frameLen]
    # print('frameData')
    # print(frameData.hex())

    # We may have some of the next frame
    rawData = rawData[frameLen:]
    # print('new rawData')
    # print(rawData.hex())

    # frame tail
    frameTail = frameData[-1]
    print(f"tail: {hex(frameTail)}")
    if frameTail != FRAME_TAIL_INT:
        print('invalid tail')
        frameData = b''
        continue

    # checksum
    cksum = frameData[-2]
    print(f"checksum: {hex(cksum)}")
    if cksum != sum(frameData[:frameLen - 2]) % 256:
        print('checksum failed')
        frameData = b''
        continue

    frameID = unpack("H", frameData[16:18])[0]
    print(f"prevID: {prevID} frameID: {frameID}")

    if frameID == prevID:
        print('duplicate frame')
        frameData = b''
        continue
    prevID = frameID

    # numR = unpack("B", frameData[14:15])[0]
    # numC = unpack("B", frameData[15:16])[0]
    numR = frameData[14]
    numC = frameData[15]
    print((numR, numC))

    frameDataLen = dataLen - 16
    frameData = np.array([unpack("B", frameData[20+i:21+i])[0] for i in range(0, frameDataLen)], dtype=np.uint8).reshape(numR, numC)
    
    # frameData = b''
    # continue

    # print(frameData)
    
    # for r in range(numR):
    #     print(f'{r:>3}', end='')
    #     for c in range(numC):
    #         # print(f' {frameData[(numC - 1 - c) * numR + r]:>3}', end='')
    #         print(f' {frameData[r * numC + c]:>3}', end='')
    #     print('')

    row1 = int(numR / 2 + 0.5) - 2
    row2 = row1 + 3

    frameData = np.array(frameData, dtype=np.uint8).reshape(numR, numC)
    # img = cv2.applyColorMap(frameData, np.arange(255, -1, -1, dtype=np.uint8)) #cv2.COLORMAP_BONE)
    img = np.zeros((numR, numC, 3))
    for r in range(numR):
        for c in range(numC):
            if frameData[r, c] == 255:
                img[r, c] = (0, 0, 48)
            else:
                img[r, c, :] = 255 - frameData[r, c]
        
    cv2.line(img, (0, row1 - 1), (numC, row1 - 1), (128, 0, 0), 1) # horiz line
    cv2.line(img, (0, row2 + 1), (numC, row2 + 1), (128, 0, 0), 1) # horiz line

    Path('/home/pi/tmp/images/depth.lock').touch()
    cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.resize(img, (400, 400), interpolation = cv2.INTER_NEAREST))
    Path('/home/pi/tmp/images/depth.lock').unlink()

    # frameData = b''
    # continue

    out = np.zeros((numC, 256, 3), dtype=np.uint8)
    for c in range(numC):
        # z = int(np.mean(frameData[row1:row2+1, c]) + 0.5)
        z = 0
        n = 0
        for r in range(row1, row2+1):
            if frameData[r, c] < 255:
                n += 1
                z += frameData[r, c]
        if n > 0:
            z = int(z / n + 0.5)
            for y in range(z + 1):
                out[c, y] = (128, 128, 128)
        else:
            for y in range(256):
                out[c, y] = (64, 64, 64)

    Path('/home/pi/tmp/images/grid.lock').touch()
    cv2.imwrite('/home/pi/tmp/images/grid.png', cv2.resize(np.rot90(out), (400, 400), interpolation = cv2.INTER_NEAREST))
    Path('/home/pi/tmp/images/grid.lock').unlink()

    frameData = b''

    # break


    """
                    def compute_real_distance(val, unit):
                    ret = 0
                    if unit != 0:
                        ret = val * unit
                    else:
                        ret = int(val) / 5.1
                        ret *= ret

                    if len(self.distances) == 0:
                        for _ in range(8):
                            self.distances.append(ret)
                    else:
                        self.distances.append(ret)
                        self.distances = self.distances[-8:]
                    return int(np.mean(self.distances))

"""