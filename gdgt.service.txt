[Unit]
Description=gdgt voice
After=network.target

[Service]
Type=idle
ExecStart=/usr/bin/python3 -u main.py
WorkingDirectory=/home/pi/gdgt/vision/src
StandardOutput=inherit
StandardError=inherit
Restart=always
User=pi

[Install]
WantedBy=multi-user.target
