"""
Vision - Globals
"""

import math

from can_ids import *

# App state

cam_angle = 90 - 15  # 15 degrees down
cam_angle_rads = math.radians(cam_angle)
cam_angle_sin = math.sin(cam_angle_rads)
cam_angle_cos = math.cos(cam_angle_rads)

frig_threshold = 8

imagesToWrite = VISION_OUTPUT_RGB | VISION_OUTPUT_MASK | VISION_OUTPUT_GRAY | VISION_OUTPUT_DEPTH

WORLD_OFFSET_X = -32.5  # RGB camera is 32.5 mm to the left of center
# D435 is 542 mm above robot origin (ground) when looking down
WORLD_OFFSET_Y = 542
# D435 ref plane is 117 mm in front of robot origin (center of drive wheels) when looking down
WORLD_OFFSET_Z = 117

FLH = 616
FLV = 616
