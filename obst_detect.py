"""
Obstacle and clear path detection
"""

import numpy as np
import numba
from numba import jit
import cv2
from time import sleep # process_time
import math
# import logging
import redis
# import os
from pathlib import Path
# import json

import pyrealsense2 as rs

import globals
# from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
from my_rs import rs_get_depth_frame, rs_get_frames, depth_scale, rs_set_ir_emitter, rs_set_ir_power
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

# FILE = path.splitext(path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

GRIDX = 60
GRIDXHALF = 30
GRIDZ = 60
GRIDZSTART = 8 # Distance from front of camera to start grid. Related to globals.CAMHEIGHT
GRIDSCALE = 12 # Pixels per grid cell in output image
LASERPWR = 150
# GROUNDY = 50
HORIZTHRESHOLD = 100
VERTTHRESHOLD = 300
ZTHRESHOLD = 2

GRID_NO_DATA = 0
GRID_HORIZ = 1
GRID_TRANS = 2
GRID_VERT = 3
GRID_NAVIGABLE = 4
GRID_PADDING = 5
GRID_PATH = 6

OUT_GRID = 0
OUT_COLORMAP = 1

STATE_IDLE = 0
STATE_WAITING = 1
STATE_DONE = 2

CAMHEIGHT = globals.WORLD_OFFSET_Y

##### Vision #####

# Processing blocks
pc = rs.pointcloud()
# decimate = rs.decimation_filter()
# decimate.set_option(rs.option.filter_magnitude, 4)

# Create arrays for future processing
out = np.empty((GRIDX, GRIDZ, 3), dtype=np.uint8)

@jit(numba.uint8[:,:,:](numba.float32[:,:], numba.float32, numba.float32), nopython=True)
def processVerts(vs, a_sin, a_cos):
    global HORIZTHRESHOLD
    
    counts = np.zeros((GRIDX, GRIDZ), dtype=np.uint16)
    means = np.zeros((GRIDX, GRIDZ), dtype=np.float32)
    m2s = np.zeros((GRIDX, GRIDZ), dtype=np.float32)
    g = np.zeros((GRIDX, GRIDZ, 2), dtype=np.uint8)

    for i in range(vs.shape[0]):
        v = vs[i]

        # if depth is zero (no data) skip the point
        if v[2] == 0: continue

        x = int(v[0] * 50.0 + 0.5) + GRIDXHALF
        # if x is outside our area of interest skip the point
        if x < 0 or x >= GRIDX: continue

        """
        x' = x * cos - y * sin
        y' = x * sin + y * cos

        But here we are rotating around the X axis, and Z is
        in place of X above.

        """

        # Rotate vector
        y0 = -v[1]
        z0 = v[2]

        # z = (z0 * a_cos + y0 * a_sin)
        z = int((z0 * a_cos + y0 * a_sin) * 50.0 + 0.5) - GRIDZSTART
        # if z is outside our area of interest skip the point
        if z < 0 or z >= GRIDZ: continue

        # y1 = (-z0 * a_sin + y0 * a_cos)
        y = CAMHEIGHT + (-z0 * a_sin + y0 * a_cos) * 1000
        # print(z0, y0, z, y1, y)
        # continue
        # if y > 300: continue
        if y < 0: y = 0.0

        counts[x, z] += 1
        delta = y - means[x, z]
        means[x, z] += delta / counts[x, z]
        delta2 = y - means[x, z]
        m2s[x, z] += delta * delta2

    for x in range(GRIDX):
        for z in range(GRIDZ):
            if counts[x, z] == 0: continue
            if counts[x, z] > 1:
                dev = m2s[x, z] / counts[x, z]
                if dev < HORIZTHRESHOLD:
                    g[x, z, 0] = GRID_HORIZ
                elif dev > VERTTHRESHOLD:
                    g[x, z, 0] = GRID_VERT
                else:
                    g[x, z, 0] = GRID_TRANS
                g[x, z, 1] = int(means[x, z] / 10 + 0.5)
    
    return g

@jit(numba.void(numba.uint8[:,:,:]), nopython=True)
def gridFlow(g):
    for i in range(GRIDZ + GRIDXHALF - 1):
        for z in range(0, i + 1):
            if z >= GRIDZ: break
            for lr in range(2):
                if lr == 0:
                    x = GRIDXHALF - i + z
                    if x < 0: continue
                else:
                    x = GRIDXHALF + 1 + i - z
                    if x >= GRIDX: continue
                if g[x, z, 0] != GRID_HORIZ: continue
                # print(x, z, g[x, z, 0], g[x, z, 1])
                if z == 0:
                    y = 0
                    # print(y)
                else:
                    n = 0
                    y = 0
                    for j in range(-1, 2):
                        if x + j < 0 or x + j >= GRIDX: continue
                        # print('', x + j, z - 1, g[x + j, z - 1, 0], g[x + j, z - 1, 1])
                        if g[x + j, z - 1, 0] == GRID_NAVIGABLE:
                            n += 1
                            y += g[x + j, z - 1, 1]
                    if n == 0:
                        # print("  none to compare")
                        continue
                    y = float(y) / n
                # print('', '', y, abs(g[x, z, 1] - y), ZTHRESHOLD)
                if abs(g[x, z, 1] - y) <= ZTHRESHOLD:
                    g[x, z, 0] = GRID_NAVIGABLE
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] != GRID_NAVIGABLE and g[x - 1, z, 0] == GRID_NAVIGABLE and g[x + 1, z, 0] == GRID_NAVIGABLE and g[x, z - 1, 0] == GRID_NAVIGABLE and g[x, z + 1, 0] == GRID_NAVIGABLE:
                g[x, z, 0] = GRID_NAVIGABLE

@jit(numba.void(numba.uint8[:,:,:]), nopython=True)
def obstaclePadding(g):
    padding = (
        # gdgt: 44 cm wide
        (-1, 6), (0, 6), (1, 6),
        (-3, 5), (-2, 5), (-1, 5), (0, 5), (1, 5), (2, 5), (3, 5),
        (-4, 4), (-3, 4), (-2, 4), (-1, 4), (0, 4), (1, 4), (2, 4), (3, 4), (4, 4),
        (-5, 3), (-4, 3), (-3, 3), (-2, 3), (-1, 3), (0, 3), (1, 3), (2, 3), (3, 3), (4, 3), (5, 3),
        (-5, 2), (-4, 2), (-3, 2), (-2, 2), (-1, 2), (0, 2), (1, 2), (2, 2), (3, 2), (4, 2), (5, 2),
        (-6, 1), (-5, 1), (-4, 1), (-3, 1), (-2, 1), (-1, 1), (0, 1), (1, 1), (2, 1), (3, 1), (4, 1), (5, 1), (6, 1),
        (-6, 0), (-5, 0), (-4, 0), (-3, 0), (-2, 0), (-1, 0), (1, 0), (2, 0), (3, 0), (4, 0), (5, 0), (6, 0),
        (-6, -1), (-5, -1), (-4, -1), (-3, -1), (-2, -1), (-1, -1), (0, -1), (1, -1), (2, -1), (3, -1), (4, -1), (5, -1), (6, -1),
        (-5, -2), (-4, -2), (-3, -2), (-2, -2), (-1, -2), (0, -2), (1, -2), (2, -2), (3, -2), (4, -2), (5, -2),
        (-5, -3), (-4, -3), (-3, -3), (-2, -3), (-1, -3), (0, -3), (1, -3), (2, -3), (3, -3), (4, -3), (5, -3),
        (-4, -4), (-3, -4), (-2, -4), (-1, -4), (0, -4), (1, -4), (2, -4), (3, -4), (4, -4),
        (-3, -5), (-2, -5), (-1, -5), (0, -5), (1, -5), (2, -5), (3, -5),
        (-1, -6), (0, -6), (1, -6),
    )
    for x in range(1, GRIDX - 1):
        for z in range(1, GRIDZ - 1):
            if g[x, z, 0] == GRID_HORIZ or g[x, z, 0] == GRID_TRANS or g[x, z, 0] == GRID_VERT:
                for p in padding:
                    if x + p[0] < 0 or x + p[0] >= GRIDX or z + p[1] < 0 or z + p[1] >= GRIDZ: continue
                    if g[x + p[0], z + p[1], 0] == GRID_NAVIGABLE:
                        g[x + p[0], z + p[1], 0] = GRID_PADDING

@jit(nopython=True)
def findPath2(g:numba.uint8[:,:,:]):
    dead_end = False
    first_obst = 0
    x = GRIDXHALF
    heading = 0
    heading_lock = False
    for z in range(GRIDZ):
        # print(x, z, g[x, z, 0])
        if g[x, z, 0] != GRID_NAVIGABLE:
            if first_obst == 0: first_obst = z
            new_x = -1
            for i in range(1, 10):
                if x - i >= 0 and g[x - i, z, 0] == GRID_NAVIGABLE:
                    new_x = x - i
                    break
                elif x + i < GRIDX and g[x + i, z, 0] == GRID_NAVIGABLE:
                    new_x = x + i
                    break
            if new_x == -1:
                dead_end = True
                break
            else:
                x = new_x
        g[x, z, 0] = GRID_PATH
        if z == 0: continue
        h = float(x - GRIDXHALF) / z
        if not heading_lock:
            if heading == 0:
                heading = h
            elif heading > 0:
                if h > heading: heading = h
                else: heading_lock = True
            elif heading < 0:
                if h < heading: heading = h
                else: heading_lock = True

    # return (first_obst, int(math.degrees(math.atan(heading))), dead_end)
    return (first_obst, heading, dead_end)

@jit(numba.void(numba.uint8[:,:,:], numba.uint8[:,:,:]), nopython=True)
def grid2out(grid, out):
    out.fill(0)
    CLASSIFY_COLORS = (
        (0, 0, 0),
        (128, 128, 128),
        (128, 128, 255),
        (0, 0, 255),
        (255, 255, 255),
        (192, 192, 255),
        (255, 0, 0)
    )
    for x in range(GRIDX):
        for z in range(GRIDZ):
            if grid[x, z, 0] == GRID_NO_DATA: continue
            out[x, z] = CLASSIFY_COLORS[grid[x, z, 0]]

##### INIT #####

def init():
    # global state
    can.del_filter('obst_detect', 'all', 'all')
    # can.add_filter('obst_detect', CAN_STATUS_HEADLIGHT, 0x7FF)

    rs_set_ir_emitter(True)
    rs_set_ir_power(100)

    globals.cam_angle = 30 # degrees down
    can.send('obst_detect', CAN_CMD_VISION, False, [VISION_D435_ANGLE, globals.cam_angle])
    sleep(2)
    globals.cam_angle_rads = math.radians(globals.cam_angle)
    globals.cam_angle_sin = math.sin(globals.cam_angle_rads)
    globals.cam_angle_cos = math.cos(globals.cam_angle_rads)

    globals.imagesToWrite = VISION_OUTPUT_RGB | VISION_OUTPUT_DEPTH | VISION_OUTPUT_GRID

    # state = STATE_IDLE

##### TICK #####

def tick():
    # return true to run again
    # global state

    if globals.imagesToWrite & VISION_OUTPUT_RGB: 
        depth_frame, color_frame = rs_get_frames()

        # Validate that both frames are valid
        if not depth_frame or not color_frame:
            return STATE_WAITING

        # color_image = np.asanyarray(color_frame.get_data())
        Path('/home/pi/tmp/images/rgb.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/rgb.jpg', np.asanyarray(color_frame.get_data()))
        Path('/home/pi/tmp/images/rgb.lock').unlink()

    else:
        depth_frame = rs_get_depth_frame()

        # Validate that frame is valid
        if not depth_frame:
            return STATE_WAITING

    if globals.imagesToWrite & VISION_OUTPUT_DEPTH: 
        # depth_image = np.asanyarray(depth_frame.get_data())
        Path('/home/pi/tmp/images/depth.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(np.asanyarray(depth_frame.get_data()), alpha=0.03), cv2.COLORMAP_JET))
        Path('/home/pi/tmp/images/depth.lock').unlink()

    # Pointcloud data to array
    points = pc.calculate(depth_frame)
    verts = np.asanyarray(points.get_vertices()).view(np.float32).reshape(-1, 3)  # xyz

    grid = processVerts(verts, globals.cam_angle_sin, globals.cam_angle_cos)

    # for x in range(GRIDX):
    #     for z in range(GRIDZ):
    #         if counts[x, z] == 0: continue
    #         # print(f'{x} {z} {counts[x, z]} {means[x, z]} {m2s[x, z]} {grid[x, z, 0]} {grid[x, z, 1]}')
    #         print(f'{x:>3d} {z:>3d} {counts[x, z]:>4d} {means[x, z]:>6.2f} {m2s[x, z] / counts[x, z]:>6.3f} {grid[x, z, 0]:>3d} {grid[x, z, 1]:>3d}')

    # for x in range(GRIDX):
    #     for z in range(GRIDZ):
    #         print(f'{x:>3d} {z:>3d} {grid[x, z, 0]:>3d} {grid[x, z, 1]:>3d}')

    # gridFlow(grid)

    # obstaclePadding(grid)

    # first_obst, heading, dead_end = findPath2(grid)

    # print(f"Obst: {first_obst} Head: {heading} Deadend: {dead_end}")
    # red.rpush('CANtx', json.dumps({
    #     'can_id': CAN_TS_VIS_OBST,
    #     'dlc': 3,
    #     'data': [first_obst, 128 + heading, 1 if dead_end else 0]
    # }))

    if globals.imagesToWrite & VISION_OUTPUT_GRID: 
        grid2out(grid, out)
        Path('/home/pi/tmp/images/grid.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/grid.png', cv2.resize(np.rot90(out), (480, 480), interpolation = cv2.INTER_NEAREST))
        Path('/home/pi/tmp/images/grid.lock').unlink()

    return 0 #STATE_DONE
