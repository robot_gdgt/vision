<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$src = empty($_GET['src']) ? '1_0' : $_GET['src'];

$redis = new Redis();
//Connecting to Redis
try {
	$redis->pconnect('/var/run/redis/redis-server.sock');
} catch(Exception $e) {
	readfile("please_stand_by.jpg");
	die;
}
$redis->rpush('img_req', $src);
while(!file_exists("/var/tmp/images/$src.jpg")) {
	sleep(0.05);
}

header('Content-Type: image/jpeg');
header('Cache-control: max-age=0, must-revalidate');
readfile("/var/tmp/images/$src.jpg");
unlink("/var/tmp/images/$src.jpg");
