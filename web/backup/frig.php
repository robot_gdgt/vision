<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$src = empty($_COOKIES['src']) ? 0 : intval($_COOKIES['src']);
if($src < 10 || $src > 12) $src = 0;
?>
<html>
	<head>
		<meta name="viewport" content="width=640">
		<title>gdgt vision</title>
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/frig.js"></script>
		<script>
			var api_url = 'api.php';
			var img_url = 'img_can.php';
		</script>
		<style>
			html, body {
				margin: 0;
				padding: 0;
				font-family: Helvetica, san-serif;
			}

			p, button {
				margin: 8px;
			}

			#content {
				width: 640px;
				margin: 0 auto;
			}

			#img_div {
				position: relative;
				width: 640px;
				text-align: center;
				background-color: #222;
			}

			#stats {
				position: absolute;
				text-align: left;
				left: 10px;
				bottom: 6px;
				color: white;
			}
		</style>
	</head>
	<body>
		<div id="content">
		<div id="img_div">
		<div id="stats">
			Frig: <span id="frig"></span><br>
			Left: <span id="left"></span>
		</div>
		<img id="video_feed" src="">
		</div>
		<p>
			Source:
			<label><input type="radio" name="img_src" value=7 <?= $src == 7 ? 'checked' : '' ?>> RGB</label>
			<label><input type="radio" name="img_src" value=11 <?= $src == 11 ? 'checked' : '' ?>> Gray</label>
			<label><input type="radio" name="img_src" value=12 <?= $src == 12 ? 'checked' : '' ?>> Mask</label>
			<label><input type="radio" name="img_src" value=13 <?= $src == 13 ? 'checked' : '' ?>> Edge</label>
			<label><input type="radio" name="img_src" value=10 <?= $src == 10 ? 'checked' : '' ?>> Depth</label>
			<label><input type="checkbox" name="img_pause" value=1> Pause</label>
		</p>
		</div>
	</body>
</html>
