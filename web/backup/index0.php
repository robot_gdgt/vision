<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

// $redis = new Redis();
// //Connecting to Redis
// $redis->connect('/var/run/redis/redis-server.sock');
// $src = $redis->get('src');
$src = empty($_COOKIES['src']) ? 1 : intval($_COOKIES['src']);

$redis = new Redis();
//Connecting to Redis
try {
	$redis->pconnect('/var/run/redis/redis-server.sock');
} catch(Exception $e) {
	die('{"error":"unable to connect to redis server"}');
}
$laser = $redis->get('laser');
$camheight = $redis->get('camheight');
?>
<html>
	<head>
		<meta name="viewport" content="width=640">
		<title>gdgt vision</title>
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/index.js"></script>
		<script>
			var api_url = 'api.php';
		</script>
		<style>
			html, body {
				margin: 0;
				padding: 0;
				font-family: Helvetica, san-serif;
			}

			p, button {
				margin: 8px;
			}

			#content {
				width: 640px;
				margin: 0 auto;
			}

			#img_div {
				position: relative;
				width: 640px;
				text-align: center;
				background-color: #222;
			}

			#stats {
				position: absolute;
				text-align: left;
				left: 10px;
				bottom: 6px;
				color: white;
			}
		</style>
	</head>
	<body>
		<div id="content">
		<div id="img_div">
		<div id="stats">
			FPS: <span id="fps"></span><br>
			Distance: <span id="dist"></span><br>
			Heading: <span id="dir"></span><br>
			Dead End: <span id="dead_end"></span>
		</div>
		<img id="video_feed" src="">
		</div>
		<p>
			Source:
			<label><input type="radio" name="img_src" value=1 <?= $src == 1 ? 'checked' : '' ?>> RGB</label>
			<label><input type="radio" name="img_src" value=2 <?= $src == 2 ? 'checked' : '' ?>> Depth</label>
			<label><input type="radio" name="img_src" value=3 <?= $src == 3 ? 'checked' : '' ?>> Obst</label>
			<label><input type="checkbox" name="img_pause" value=1> Pause</label>
		</p>
		<p>
			Laser:
			<label><input type="radio" class="api" name="laser" value="0" <?= $laser == 0 ? 'checked' : '' ?>> Off</label>
			<label><input type="radio" class="api" name="laser" value="1" <?= $laser == 1 ? 'checked' : '' ?>> 25%</label>
			<label><input type="radio" class="api" name="laser" value="2" <?= $laser == 2 ? 'checked' : '' ?>> 50%</label>
			<label><input type="radio" class="api" name="laser" value="3" <?= $laser == 3 ? 'checked' : '' ?>> 75%</label>
			<label><input type="radio" class="api" name="laser" value="4" <?= $laser == 4 ? 'checked' : '' ?>> 100%</label>
		</p>
		<p>
			Camera Height:
			<input type="text" name="camheight" size="3" value="<?= $camheight ?>">
		</p>
		<p>
			<label><input type="checkbox" class="api" name="record" value="1"  {{ 'checked' if state.record == '1' }}> Record to folder</label>
		</p>
		</div>
	</body>
</html>
