<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$src = empty($_GET['src']) ? 0 : intval($_GET['src']);

switch($src) {
    case 1:
        $src = '/home/pi/tmp/images/rgb.jpg';
        $type = 'jpeg';
        $lock = '/home/pi/tmp/images/rgb.lock';
        break;
    case 2:
        $src = '/home/pi/tmp/images/gray.jpg';
        $type = 'jpeg';
        $lock = '/home/pi/tmp/images/gray.lock';
        break;
    case 3:
        $src = '/home/pi/tmp/images/mask.png';
        $type = 'png';
        $lock = '/home/pi/tmp/images/mask.lock';
        break;
    case 4:
        $src = '/home/pi/tmp/images/depth.jpg';
        $type = 'jpeg';
        $lock = '/home/pi/tmp/images/depth.lock';
        break;
    case 5:
        $src = '/home/pi/tmp/images/grid.png';
        $type = 'png';
        $lock = '/home/pi/tmp/images/grid.lock';
        break;
    default:
        $src = '/home/pi/gdgt_vision/web/images/please_stand_by.jpg';
        $type = 'jpeg';
        $lock = null;
}

if($lock !== null) {
    while(file_exists($lock)) {}
}
            
header('Content-Type: image/'.$type);
header('Cache-control: max-age=0, must-revalidate');
readfile($src);
// unlink("/home/pi/tmp/images/$src");
