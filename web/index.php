<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$src = empty($_COOKIES['src']) ? 0 : intval($_COOKIES['src']);
if($src < 1 || $src > 5) $src = 0;
?>
<html>
	<head>
		<meta name="viewport" content="width=900">
		<title>gdgt vision</title>
		<script src="js/jquery-3.4.1.min.js"></script>
		<script src="js/index.js"></script>
		<script>
			var api_url = 'api.php';
			var img_url = 'img.php';
		</script>
		<style>
			html, body {
				margin: 0;
				padding: 0;
				font-family: Helvetica, san-serif;
			}

			p, button {
				margin: 8px;
			}

			#content {
				width: 640px;
				margin: 0 auto;
			}

			#img_div {
				position: relative;
				width: 640px;
				text-align: center;
				background-color: #222;
			}

			#caption {
				position: absolute;
				text-align: left;
				left: 10px;
				bottom: 6px;
				color: white;
			}
		</style>
	</head>
	<body>
		<div id="content">
		<div id="img_div">
		<div id="caption">
		</div>
		<img id="video_feed" src="">
		</div>
		<p>
			Source:
			<label><input type="radio" name="img_src" value=1 <?= $src == 1 ? 'checked' : '' ?>> RGB</label>
			<label><input type="radio" name="img_src" value=2 <?= $src == 2 ? 'checked' : '' ?>> Gray</label>
			<label><input type="radio" name="img_src" value=3 <?= $src == 3 ? 'checked' : '' ?>> Mask</label>
			<label><input type="radio" name="img_src" value=4 <?= $src == 4 ? 'checked' : '' ?>> Depth</label>
			<label><input type="radio" name="img_src" value=5 <?= $src == 5 ? 'checked' : '' ?>> Grid</label>
			<label><input type="checkbox" name="img_refresh" value=1> Auto Refresh</label>
		</p>
		</div>
	</body>
</html>
