<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);

$k = @$_GET['k'];

$redis = new Redis();
//Connecting to Redis
try {
	$redis->pconnect('/var/run/redis/redis-server.sock');
} catch(Exception $e) {
	die('{"error":"unable to connect to redis server"}');
}

if($k == 'obst') {
	while(!($obst = $redis->get('obst'))) {
		sleep(0.05);
	}
	header('Content-Type: application/json');
	header('Cache-control: max-age=0, must-revalidate');
	echo $obst;
} elseif($k == 'caption') {
	while(!($caption = $redis->get('caption'))) {
		sleep(0.05);
	}
	header('Content-Type: application/json');
	header('Cache-control: max-age=0, must-revalidate');
	echo json_encode(['caption'=>$caption]);
} else {
	$v = @$_GET['v'];
	$redis->set($k, $v);

	echo "$k set to $v";
}
