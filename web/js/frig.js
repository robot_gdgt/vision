"use strict";

var paused = false;
var error_cnt = 0;
var obst;

function set_image_src() {
	if(paused) return;
	var src = $('input[name=img_src]:checked').val();
	console.log(`src: ${src}`);
	$('#video_feed').attr('src', `img_can.php?src=${src}&t=${Date.now()}`);

	var request = $.ajax({
		url: `${api_url}?k=frig`,
		type: 'GET',
		processData: false,
		success: function(response, status) {
			console.log('response', response);
			console.log('status', status);
			$('#frig').text(response.frig.join(', '));
			if(response.left) $('#left').text(response.left.join(', '));
			else $('#left').text('?');
		},
		error: function(jqXHR, status, error) {
			console.log('jqXHR', jqXHR);
			console.log('status', status);
			console.log('error', error);
		}
	});
}

$(function() {
	$('#video_feed').on('load', function() {
		setTimeout(set_image_src, 500);
		error_cnt = 0;
	});

	$('#video_feed').on('error', function() {
		this.src = '/gdgt/images/please_stand_by.jpg';
		error_cnt++;
	});

	$('input[name=img_pause]').click(function() {
		const $this = $(this);
		if($this.is(":checked")) {
			paused = true;
			console.log('paused');
		} else {
			paused = false;
			console.log('unpaused');
			set_image_src();
		}
	});

	$('input[name=img_src]').click(function() {
		if(paused) {
			$('input[name=img_pause]').prop('checked', false);
			paused = false;
			console.log('unpaused');
			set_image_src();
		}
	});

	set_image_src();
});
