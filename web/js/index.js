"use strict";

var auto_refresh = false;
var error_cnt = 0;
var obst;

function set_image_src() {
	var src = $('input[name=img_src]:checked').val();
	console.log(`src: ${src}`);
	$('#video_feed').attr('src', `${img_url}?src=${src}&t=${Date.now()}`);

	var request = $.ajax({
		url: `${api_url}?k=caption`,
		type: 'GET',
		processData: false,
		success: function(response, status) {
			console.log('response', response);
			console.log('status', status);
			$('#caption').text(response.caption);
		},
		error: function(jqXHR, status, error) {
			console.log('jqXHR', jqXHR);
			console.log('status', status);
			console.log('error', error);
		}
	});
}

$(function() {
	$('#video_feed').on('load', function() {
		if(auto_refresh) {
			setTimeout(set_image_src, 500);
			error_cnt = 0;
		}
	});

	$('#video_feed').on('error', function() {
		this.src = 'images/please_stand_by.jpg';
		error_cnt++;
	});

	$('input[name=img_refresh]').click(function() {
		const $this = $(this);
		if($this.is(":checked")) {
			auto_refresh = true;
			console.log('auto_refresh');
			set_image_src();
		} else {
			auto_refresh = false;
			console.log('paused');
		}
	});

	$('input[name=img_src]').click(function() {
		set_image_src();
	});

	set_image_src();
});
