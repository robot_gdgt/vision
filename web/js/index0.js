"use strict";

var image_counter = 0;
var paused = false;
var error_cnt = 0;
var obst;

function set_image_src() {
	if(paused) return;
	var src = $('input[name=img_src]:checked').val();
	console.log(`src: ${src}_${image_counter}`);
	$('#video_feed').attr('src', `/img.php?src=${src}_${image_counter}`);
	var request = $.ajax({
		url: `${api_url}?k=obst`,
		type: 'GET',
		processData: false,
		success: function(response, status) {
// 			console.log('response', response);
// 			console.log('status', status);
			$('#fps').text(response.fps);
			$('#dist').text(response.dist);
			$('#dir').text(response.dir);
			$('#dead_end').text(response.dead_end ? '1' : '0');
		},
		error: function(jqXHR, status, error) {
			console.log('jqXHR', jqXHR);
			console.log('status', status);
			console.log('error', error);
		}
	});
	image_counter++;
}

$(function() {
	$('input.api').click(function() {
		const $this = $(this);
		var k = this.name;
		var v = this.type == 'checkbox' ? ($this.is(":checked") ? this.value : '0') : $this.val();
		console.log(`${api_url}?k=${k}&v=${v}`);
		var request = $.ajax({
			url: `${api_url}?k=${k}&v=${v}`,
			type: 'GET',
			success: function(response, status) {
				console.log('response', response);
// 				console.log('status', status);
			},
			error: function(jqXHR, status, error) {
				console.log('jqXHR', jqXHR);
				console.log('status', status);
				console.log('error', error);
			}
		});
	});

	$('input[name=camheight]').on('change', function() {
		const $this = $(this);
		var k = this.name;
		var v = $this.val();
		console.log(`${api_url}?k=${k}&v=${v}`);
		var request = $.ajax({
			url: `${api_url}?k=${k}&v=${v}`,
			type: 'GET',
			success: function(response, status) {
				console.log('response', response);
// 				console.log('status', status);
			},
			error: function(jqXHR, status, error) {
				console.log('jqXHR', jqXHR);
				console.log('status', status);
				console.log('error', error);
			}
		});
	});

	$('#video_feed').on('load', function() {
		setTimeout(set_image_src, 500);
		error_cnt = 0;
	});

	$('#video_feed').on('error', function() {
		this.src = 'please_stand_by.jpg';
		error_cnt++;
	});

	$('input[name=img_pause]').click(function() {
		const $this = $(this);
		if($this.is(":checked")) {
			paused = true;
			console.log('paused');
		} else {
			paused = false;
			console.log('unpaused');
			set_image_src();
		}
	});

	$('input[name=img_src]').click(function() {
		if(paused) {
			$('input[name=img_pause]').prop('checked', false);
			paused = false;
			console.log('unpaused');
			set_image_src();
		}
	});

	set_image_src();
});
