Started with 2020-02-13-raspbian-buster-lite.img

boot/config.txt
	hdmi_force_hotplug=1
	hdmi_group=2
	hdmi_mode=35
copy config.txt to boot/config.txt

password for pi: keepout

system updates software

restart

Prefs > Rasp Pi Config
	system
		hostname: gdgt2
		wait for network
	interfaces
		Canera
		SSH
		SPI
		Serial port
		Serial console

nano ~/.bashrc
	modify aliases: l, ll
	add: py="python3"

set desktop picture

shutdown

https://medium.com/@ccarnino/backup-raspberry-pi-sd-card-on-macos-the-2019-simple-way-to-clone-1517af972ca5
backup SD card to buster_desktop1.iso (change .cdr > .iso)

sudo apt update = All packages are up to date.
sudo apt upgrade = 0 upgraded, 0 newly installed, 0 to remove and 0 not upgraded.

sudo apt install llvm = 7.0-47 or 7.0.1-9

https://github.com/numba/llvmlite
llvmlite versions	compatible LLVM versions
0.29.0 - ...		7.0.x, 7.1.x, 8.0.x
0.27.0 - 0.28.0		7.0.x

pip3 -V = pip 18.1 from /usr/lib/python3/dist-packages/pip (python 3.7)

Python 2.7.16 = /usr/bin/python
Python 3.7.3 = /usr/bin/python3

Numba 0.48 is compatible with Python 3.6 or later, and Numpy versions 1.15 or later.
Numba Dependencies
	Python versions: 3.6-3.8
	llvmlite 0.31.*
	NumPy >=1.15

https://pypi.org/project/llvmlite/
pip3 install llvmlite = Successfully installed llvmlite-0.31.0

pip3 install numba =
	Requirement already satisfied: setuptools in /usr/lib/python3/dist-packages (from numba) (40.8.0)
	Requirement already satisfied: llvmlite<0.32.0,>=0.31.0dev0 in ./.local/lib/python3.7/site-packages (from numba) (0.31.0)
	Requirement already satisfied: numpy>=1.15 in /usr/lib/python3/dist-packages (from numba) (1.16.2)
	Installing collected packages: numba
	Successfully installed numba-0.48.0

import numba = /home/pi/.local/lib/python3.7/site-packages/numba/errors.py:137: UserWarning: Insufficiently recent colorama version found. Numba requires colorama >= 0.3.9

pip3 install colorama (0.4.3) = Requirement already satisfied: colorama in /usr/lib/python3/dist-packages (0.3.7)

sys.path = ['', '/usr/lib/python37.zip', '/usr/lib/python3.7', '/usr/lib/python3.7/lib-dynload', '/home/pi/.local/lib/python3.7/site-packages', '/usr/local/lib/python3.7/dist-packages', '/usr/lib/python3/dist-packages']

numpy.version.version = '1.16.2'

backup: buster_desktop2.iso

https://www.pyimagesearch.com/2018/09/19/pip-install-opencv/

sudo apt install libhdf5-dev libhdf5-serial-dev libhdf5-103
sudo apt install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
sudo apt install libatlas-base-dev
sudo apt install libjasper-dev
sudo pip3 install opencv-contrib-python==4.1.0.25
pip3 install "picamera[array]" =
	Requirement already satisfied: picamera[array] in /usr/lib/python3/dist-packages (1.13)
	Requirement already satisfied: numpy in /usr/lib/python3/dist-packages (from picamera[array]) (1.16.2)
pip3 install imutils = Successfully installed imutils-0.5.3
pip3 install -U Flask = Successfully installed Flask-1.1.1 Jinja2-2.11.1 Werkzeug-1.0.0

backup: buster_desktop3.iso

sudo apt install git = git is already the newest version (1:2.20.1-2+deb10u1).

https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_raspbian.md

sudo apt install -y libdrm-amdgpu1 libdrm-amdgpu1-dbg libdrm-dev libdrm-exynos1 libdrm-exynos1-dbg libdrm-freedreno1 libdrm-freedreno1-dbg libdrm-nouveau2 libdrm-nouveau2-dbg libdrm-omap1 libdrm-omap1-dbg libdrm-radeon1 libdrm-radeon1-dbg libdrm-tegra0 libdrm-tegra0-dbg libdrm2 libdrm2-dbg
	^ some or all of these packages were not found
sudo apt install -y libdrm-amdgpu1 libdrm-dev libdrm-exynos1 libdrm-freedreno1 libdrm-nouveau2 libdrm-omap1 libdrm-radeon1 libdrm-tegra0 libdrm2
sudo apt install -y libglu1-mesa libglu1-mesa-dev glusterfs-common libglu1-mesa libglu1-mesa-dev libglui-dev libglui2c2
sudo apt install -y libglu1-mesa libglu1-mesa-dev mesa-utils mesa-utils-extra xorg-dev libgtk-3-dev libusb-1.0-0-dev

cd ~
git clone https://github.com/IntelRealSense/librealsense.git
cd librealsense
sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
sudo -i
udevadm control --reload-rules && udevadm trigger
exit

sudo apt install -y python3-can
sudo apt install -y python3-protobuf
sudo apt install python3-dev = python3-dev is already the newest version (3.7.3-1).
sudo apt install python3-rpi.gpio = python3-rpi.gpio is already the newest version (0.7.0~buster-1).

nano ~/.bashrc
	export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
source ~/.bashrc

sudo nano /etc/dphys-swapfile
	CONF_SWAPSIZE=2048
sudo /etc/init.d/dphys-swapfile restart swapon -s

sudo apt install cmake
cmake --version = cmake version 3.13.4

cd ~/librealsense
mkdir build && cd build
cmake .. -DBUILD_EXAMPLES=true -DCMAKE_BUILD_TYPE=Release -DFORCE_LIBUVC=true
make -j1
sudo make install

cmake .. -DBUILD_PYTHON_BINDINGS=bool:true -DPYTHON_EXECUTABLE=$(which python3)
make -j1
sudo make install

sudo nano /etc/dphys-swapfile
	CONF_SWAPSIZE=100
sudo /etc/init.d/dphys-swapfile restart swapon -s

backup: buster_desktop4.iso

nano ~/.bashrc
	export PYTHONPATH=$PYTHONPATH:/usr/local/lib
source ~/.bashrc

sudo apt install python-opengl
(sudo -H pip3 install pyopengl)
pip3 install pyopengl = Successfully installed pyopengl-3.1.5
(sudo -H pip3 install pyopengl_accelerate)
pip3 install pyopengl_accelerate = Successfully installed pyopengl-accelerate-3.1.5

(sudo raspi-config
	"7.Advanced Options" - "A7 GL Driver" - "G2 GL (Fake KMS)"

reboot)

realsense-viewer
rs-enumerate-devices

pip3 install var_dump = Successfully installed var-dump-1.2

sudo apt install python3-gpiozero = python3-gpiozero is already the newest version (1.5.1).

sudo apt install redis
pip3 install redis = Successfully installed redis-3.4.1

cd /etc/redis
sudo cp redis.conf redis.conf.backup
sudo nano /etc/redis/redis.conf
	port 0
	tcp-backlog 0
	unixsocket /var/run/redis/redis-server.sock
	unixsocketperm 777
>	daemonize yes
>	supervised no
>	pidfile /var/run/redis/redis-server.pid
>	logfile /var/log/redis/redis-server.log
>	dir /var/lib/redis
	maxmemory 50M
	maxmemory-policy allkeys-lru
> means left as-is

sudo systemctl start redis
sudo systemctl stop redis
sudo systemctl enable redis

redis-cli -s /var/run/redis/redis-server.sock

pip3 install posix_ipc = Successfully installed posix-ipc-1.0.4

reboot

redis-cli -s /var/run/redis/redis-server.sock < success

cd /home/pi/
git clone https://gitlab.com/robot_gdgt/vision.git
mv vision gdgt_vision
cd gdgt_vision
git config --global user.email "greg.schumacher@gmail.com"
git config --global user.name "Greg Schumacher"
git config credential.helper store
git push
	greg.schumacher
	myFilesHere

backup: buster_desktop5.iso

cd /home/pi/
git clone https://gitlab.com/robot_gdgt/can-proxy.git
mv can-proxy gdgt_can-proxy
cd gdgt_can-proxy
git config credential.helper store

cd /home/pi/
git clone https://gitlab.com/robot_gdgt/shared.git
mv shared gdgt_shared
cd gdgt_shared
git config credential.helper store

nano /home/pi/.local/lib/python3.7/site-packages/gdgt_shared.pth
	/home/pi/gdgt_shared

cd ~
git clone https://github.com/tartley/colorama.git
cd colorama
sudo python3 setup.py install

https://www.dexterindustries.com/howto/run-a-program-on-your-raspberry-pi-at-startup/#systemd

sudo nano /lib/systemd/system/gdgt_can-proxy.service
[Unit]
Description=gdgt can-proxy
After=syslog.target

[Service]
User=pi
Group=pi
Environment="PYTHONUNBUFFERED=1"
ExecStart=/usr/bin/python3 /home/pi/gdgt_can-proxy/src/can-proxy.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
---
sudo chmod 644 /lib/systemd/system/gdgt_can-proxy.service
sudo systemctl daemon-reload
sudo systemctl enable gdgt_can-proxy.service
sudo systemctl start gdgt_can-proxy.service
sudo systemctl stop gdgt_can-proxy.service
sudo systemctl disable gdgt_can-proxy.service

sudo nano /lib/systemd/system/gdgt_capture.service
[Unit]
Description=gdgt capture
After=syslog.target

[Service]
User=pi
Group=pi
Environment="PYTHONUNBUFFERED=1"
Environment="PYTHONPATH=/usr/local/lib"
ExecStart=/usr/bin/python3 /home/pi/gdgt_vision/capture/src/capture.py
Restart=on-failure

[Install]
WantedBy=multi-user.target
---

sudo chmod 644 /lib/systemd/system/gdgt_capture.service
sudo systemctl daemon-reload
sudo systemctl enable gdgt_capture.service
sudo systemctl start gdgt_capture.service
sudo systemctl stop gdgt_capture.service
sudo systemctl disable gdgt_capture.service

https://pimylifeup.com/raspberry-pi-lighttpd/

sudo apt-get update
sudo apt-get upgrade
sudo apt-get remove apache2 = Package 'apache2' is not installed, so not removed

sudo apt-get install lighttpd
sudo nano /var/www/html/index.html

sudo apt-get install php7.3-fpm php7.3-mbstring php7.3-mysql php7.3-curl php7.3-gd php7.3-curl php7.3-zip php7.3-xml -y
sudo lighttpd-enable-mod fastcgi
sudo lighttpd-enable-mod fastcgi-php
sudo nano /etc/lighttpd/conf-available/15-fastcgi-php.conf
-----
# -*- depends: fastcgi -*-
# /usr/share/doc/lighttpd/fastcgi.txt.gz
# http://redmine.lighttpd.net/projects/lighttpd/wiki/Docs:ConfigurationOptions#mod_fastcgi-fastcgi

## Start an FastCGI server for php (needs the php5-cgi package)
fastcgi.server += ( ".php" =>
        ((
                "socket" => "/var/run/php/php7.3-fpm.sock",
                "broken-scriptfilename" => "enable"
        ))
)
-----
sudo service lighttpd force-reload

https://www.domoticz.com/wiki/Setting_up_a_RAM_drive_on_Raspberry_Pi

sudo mkdir /var/tmp = mkdir: cannot create directory ‘/var/tmp’: File exists
sudo nano /etc/fstab
tmpfs /var/tmp tmpfs nodev,nosuid,size=1M 0 0
sudo mount -a

sudo nano /etc/lighttpd/lighttpd.conf
sudo /etc/init.d/lighttpd restart

sudo apt install pkg-php-tools
sudo pecl channel-update pecl.php.net
sudo apt install php-dev
sudo pecl install redis
cd /etc/php/7.3/mods-available/
sudo nano redis.ini
cd /etc/php/7.3/fpm/conf.d
sudo ln -s /etc/php/7.3/mods-available/redis.ini 20-redis.ini
sudo /etc/init.d/lighttpd restart

Build process completed successfully
Installing '/usr/lib/php/20180731/redis.so'
install ok: channel://pecl.php.net/redis-5.2.1
configuration option "php_ini" is not set to php.ini location
You should add "extension=redis.so" to php.ini
