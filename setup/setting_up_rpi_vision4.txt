Started with 2020-02-13-raspbian-buster-lite.img

password for pi: keepout

Prefs > Rasp Pi Config
	system
		hostname: gdgt2
		wait for network
	interfaces
		Canera
		SSH
		SPI
		Serial port
		Serial console

reboot

nano ~/.bashrc
	modify aliases: l, ll
	add: py
source ~/.bashrc

https://github.com/jjhelmus/berryconda

copied Berryconda3-2.0.0-Linux-armv7l.sh > ~
chmod +x Berryconda3-2.0.0-Linux-armv7l.sh
./Berryconda3-2.0.0-Linux-armv7l.sh
source .bashrc

conda update conda
conda update --all

conda install -c numba numba
conda update numpy
conda update pip # All requested packages already installed.
conda update python # All requested packages already installed.
conda install flask

backup: gdgt2 before opencv install.dmg

https://www.pyimagesearch.com/2019/09/16/install-opencv-4-on-raspberry-pi-4-and-raspbian-buster/

sudo apt-get purge wolfram-engine
sudo apt-get purge libreoffice*
sudo apt-get clean
sudo apt-get autoremove

sudo apt-get update && sudo apt-get upgrade
sudo apt-get install build-essential cmake pkg-config
sudo apt-get install libjpeg-dev libtiff5-dev libjasper-dev libpng-dev
sudo apt-get install libavcodec-dev libavformat-dev libswscale-dev libv4l-dev
sudo apt-get install libxvidcore-dev libx264-dev
sudo apt-get install libfontconfig1-dev libcairo2-dev
sudo apt-get install libgdk-pixbuf2.0-dev libpango1.0-dev
sudo apt-get install libgtk2.0-dev libgtk-3-dev
sudo apt-get install libatlas-base-dev gfortran
# sudo apt-get install libhdf5-dev libhdf5-serial-dev libhdf5-103
# sudo apt-get install libqtgui4 libqtwebkit4 libqt4-test python3-pyqt5
sudo apt-get install python3-dev

wget https://bootstrap.pypa.io/get-pip.py
sudo python3 get-pip.py
sudo rm -rf ~/.cache/pip
pip install "picamera[array]"

cd ~
wget -O opencv.zip https://github.com/opencv/opencv/archive/4.2.0.zip
wget -O opencv_contrib.zip https://github.com/opencv/opencv_contrib/archive/4.2.0.zip
unzip opencv.zip
unzip opencv_contrib.zip
mv opencv-4.2.0 opencv
mv opencv_contrib-4.2.0 opencv_contrib

sudo nano /etc/dphys-swapfile
	CONF_SWAPSIZE=2048
sudo /etc/init.d/dphys-swapfile restart swapon -s

cd ~/opencv
mkdir build
cd build
cmake -D CMAKE_BUILD_TYPE=RELEASE \
-D CMAKE_INSTALL_PREFIX=/usr/local \
-D OPENCV_EXTRA_MODULES_PATH=~/opencv_contrib/modules \
-D ENABLE_NEON=ON \
-D ENABLE_VFPV3=ON \
-D BUILD_TESTS=OFF \
-D INSTALL_PYTHON_EXAMPLES=ON \
-D INSTALL_C_EXAMPLES=OFF \
-D OPENCV_ENABLE_NONFREE=ON \
-D CMAKE_SHARED_LINKER_FLAGS=-latomic \
-D BUILD_EXAMPLES=OFF \
-D BUILD_opencv_java=OFF \
-D BUILD_opencv_python=OFF \
-D BUILD_opencv_python2=OFF \
-D BUILD_opencv_python3=ON \
-D PYTHON3_EXECUTABLE=~/berryconda3/bin/python3.6 \
-D PYTHON3_PACKAGES_PATH=~/berryconda3/lib/python3.6/site-packages/ \
-D PYTHON3_LIBRARIES=~/berryconda3/lib/libpython3.6m.so \
-D PYTHON3_INCLUDE_DIRS=~/berryconda3/include/python3.6m/ \
-D PYTHON3_NUMPY_INCLUDE_DIRS=~/berryconda3/lib/python3.6/site-packages/numpy/core/include \
-D PYTHON_DEFAULT_EXECUTABLE=~/berryconda3/bin/python3.6 \
-D OPENCV_PYTHON3_INSTALL_PATH=~/berryconda3/pkgs ..

	-D PYTHON3_LIBRARY=~/berryconda3/lib/libpython3.6m.so \
	-D PYTHON3_INCLUDE_DIR=~/berryconda3/include/python3.6m/ \
	-D PYTHON3_INCLUDE=~/berryconda3/include/python3.6m \

	PYTHON3_EXECUTABLE                           /usr/bin/python3.4
	PYTHON3_INCLUDE_DIR                          /usr/include/python3.4m
	PYTHON3_LIBRARY                              /usr/lib/x86_64-linux-gnu/libpython3.4m.so
	PYTHON3_NUMPY_INCLUDE_DIRS                   /usr/lib/python3/dist-packages/numpy/core/include
	PYTHON3_PACKAGES_PATH                        /usr/lib/python3/dist-packages

--   Python 3:
--     Interpreter:                 /home/pi/berryconda3/bin/python3.6 (ver 3.6.6)
--     Libraries:                   /home/pi/berryconda3/lib/libpython3.6m.so (ver 3.6.6)
--     numpy:                       /home/pi/berryconda3/lib/python3.6/site-packages/numpy/core/include (ver 1.15.1)
--     install path:                /home/pi/berryconda3/pkgs/cv2/python-3.6
--
--   Python (for build):            /home/pi/berryconda3/bin/python3.6

make -j4

sudo make install
sudo ldconfig

cd /home/pi/berryconda3/pkgs/cv2/python-3.6
sudo cp cv2.cpython-36m-arm-linux-gnueabihf.so /home/pi/berryconda3/lib/python3.6/site-packages/cv2.so

backup: gdgt2 before realsense install.dmg

sudo apt update
sudo apt upgrade

https://github.com/IntelRealSense/librealsense/blob/master/doc/installation_raspbian.md

sudo apt-get install -y libdrm-amdgpu1 libdrm-amdgpu1-dbg libdrm-dev libdrm-exynos1 libdrm-exynos1-dbg libdrm-freedreno1 libdrm-freedreno1-dbg libdrm-nouveau2 libdrm-nouveau2-dbg libdrm-omap1 libdrm-omap1-dbg libdrm-radeon1 libdrm-radeon1-dbg libdrm-tegra0 libdrm-tegra0-dbg libdrm2 libdrm2-dbg
	^ some or all of these packages were not found
sudo apt-get install -y libdrm-amdgpu1 libdrm-dev libdrm-exynos1 libdrm-freedreno1 libdrm-nouveau2 libdrm-omap1 libdrm-radeon1 libdrm-tegra0 libdrm2
sudo apt-get install -y libglu1-mesa libglu1-mesa-dev glusterfs-common libglu1-mesa libglu1-mesa-dev libglui-dev libglui2c2
sudo apt-get install -y libglu1-mesa libglu1-mesa-dev mesa-utils mesa-utils-extra xorg-dev libgtk-3-dev libusb-1.0-0-dev

sudo apt install git # git is already the newest version

cd ~
git clone https://github.com/IntelRealSense/librealsense.git
cd librealsense
sudo cp config/99-realsense-libusb.rules /etc/udev/rules.d/
sudo -i
udevadm control --reload-rules && udevadm trigger
exit

nano ~/.bashrc
	export LD_LIBRARY_PATH=/usr/local/lib:$LD_LIBRARY_PATH
source ~/.bashrc

sudo apt install -y python3-can
sudo apt install -y python3-protobuf

cd ~/librealsense
mkdir build && cd build
cmake .. -DBUILD_EXAMPLES=true -DCMAKE_BUILD_TYPE=Release -DFORCE_LIBUVC=true
make -j1
sudo make install

cmake .. -DBUILD_PYTHON_BINDINGS=bool:true -DPYTHON_EXECUTABLE=$(which python3)
make -j1
sudo make install

sudo nano /etc/dphys-swapfile
	CONF_SWAPSIZE=100
sudo /etc/init.d/dphys-swapfile restart swapon -s

backup: gdgt2 after realsense install.dmg

nano ~/.bashrc
	export PYTHONPATH=$PYTHONPATH:/usr/local/lib
source ~/.bashrc

sudo apt install python-opengl
sudo -H pip3 install pyopengl
sudo -H pip3 install pyopengl_accelerate
sudo raspi-config
	"7.Advanced Options" - "A7 GL Driver" - "G2 GL (Fake KMS)"

reboot

realsense-viewer
rs-enumerate-devices

pip install imutils

pip install var_dump

sudo apt install redis
pip install redis

sudo apt install python3-gpiozero # already the newest

cd /home/pi/
git clone https://gitlab.com/robot_gdgt/vision.git
cd vision
git config --global user.email "greg.schumacher@gmail.com"
git config --global user.name "Greg Schumacher"
git config credential.helper store
git push
	greg.schumacher
	myFilesHere

pip install posix_ipc

https://redis.io/topics/quickstart
http://mjavery.blogspot.com/2016/05/setting-up-redis-on-raspberry-pi.html
https://habilisbest.com/install-redis-on-your-raspberrypi
sudo mkdir /etc/redis
sudo mkdir /var/redis
wget http://download.redis.io/redis-stable.tar.gz
tar xvzf redis-stable.tar.gz
cd redis-stable
sudo cp utils/redis_init_script /etc/init.d/redis-server
sudo cp redis.conf /etc/redis/
sudo chmod 644 /etc/redis/redis.conf
cd ..
sudo rm -Rf redis*
sudo adduser --system --group --disabled-login redis --no-create-home --shell /bin/nologin --quiet
cat /etc/passwd | grep redis
sudo nano /etc/redis/redis.conf
	port 0
	tcp-backlog 0
	unixsocket /tmp/redis.sock
	unixsocketperm 700
	daemonize yes
	pidfile /var/run/redis-server.pid
	logfile /var/log/redis-server.log
	stop-writes-on-bgsave-error no
	dir /var/redis
	maxmemory 50M
	maxmemory-policy allkeys-lru
sudo mkdir -p /var/run/redis
sudo chown -R redis:redis /var/run/redis
sudo nano /etc/init.d/redis-server
sudo chmod +x /etc/init.d/redis-server
sudo update-rc.d redis-server defaults
sudo /etc/init.d/redis-server start

sudo touch /var/log/redis-server.log
sudo chmod 777 /var/log/redis-server.log
sudo chown redis:redis /var/redis
sudo chmod 777 /var/redis

https://gist.github.com/mkocikowski/aeca878d58d313e902bb
https://gist.github.com/hackedunit/a53f0b5376b3772d278078f686b04d38
sudo adduser --system --group --disabled-login redis --no-create-home --shell /bin/nologin --quiet
cat /etc/passwd | grep redis
sudo cp redis.conf /etc/redis/
sudo chmod 644 /etc/redis.conf
sudo mkdir /var/redis
sudo chown redis:redis /var/redis
sudo chmod 755 /var/redis
sudo mkdir /var/log/redis
sudo chown redis:redis /var/log/redis
sudo chmod 755 /var/log/redis
sudo nano /etc/redis.conf
	port 0
	tcp-backlog 0
	unixsocket /tmp/redis.sock
	unixsocketperm 700
	daemonize no
	supervised systemd
	pidfile /var/run/redis-server.pid
	logfile /var/log/redis/redis-server.log
	stop-writes-on-bgsave-error no
	dir /var/redis
	maxmemory 50M
	maxmemory-policy allkeys-lru
sudo nano /etc/systemd/system/redis.service
	[Unit]
	Description=Redis In-Memory Data Store
	After=syslog.target

	[Service]
	User=redis
	Group=redis
	ExecStart=/usr/bin/redis-server /etc/redis.conf
	ExecStop=/usr/bin/redis-cli -s /tmp/redis.sock shutdown
	RestartSec=5s
	Restart=on-success

	[Install]
	WantedBy=multi-user.target

sudo systemctl start redis
sudo systemctl enable redis

sudo apt install -y python3-can
sudo apt install -y python3-protobuf
# cp -R /usr/lib/python3/dist-packages/can /home/pi/berryconda3/lib/python3.6/site-packages/can
sudo apt install python3-gpiozero # already the newest
nano /home/pi/berryconda3/lib/python3.6/site-packages/more.pth
	/usr/lib/python3/dist-packages
sudo apt install pigpio # pigpio is already the newest version (1.71-0~rpt1)
pip install RPi.GPIO # Requirement already satisfied: RPi.GPIO in /usr/lib/python3/dist-packages (0.7.0)
sudo apt install python3-dev python3-rpi.gpio # python3-dev is already the newest version (3.7.3-1). python3-rpi.gpio is already the newest version (0.7.0~buster-1).
conda install -c poppy-project rpi.gpio < no go

{
https://www.raspberrypi-spy.co.uk/2012/05/install-rpi-gpio-python-library/
cd ~
wget https://files.pythonhosted.org/packages/cb/88/d3817eb11fc77a8d9a63abeab8fe303266b1e3b85e2952238f0da43fed4e/RPi.GPIO-0.7.0.tar.gz
tar -xvf RPi.GPIO-0.7.0.tar.gz
cd RPi.GPIO-0.7.0
sudo python setup.py install
} did not work - installed python 2.7 compat version

sudo apt remove python3-rpi.gpio

sudo apt update
sudo apt upgrade

sudo apt install python3-rpi.gpio

conda create --name gdgt python=3.6.6
conda activate gdgt
conda deactivate

sudo ln -s /home/pi/berryconda3/etc/profile.d/conda.sh /etc/profile.d/conda.sh

sudo nano /boot/config.txt
add:
	init_uart_baud=115200

	# CAN adapter
	dtparam=spi=on
	dtoverlay=mcp2515-can0,oscillator=8000000,interrupt=22
	dtoverlay=spi-bcm2835-overlay

sudo apt install wiringpi < wiringpi is already the newest version (2.50).

------------



sudo cp gdgt.service.txt /lib/systemd/system/gdgt.service
sudo systemctl daemon-reload
sudo systemctl enable gdgt.service
sudo reboot
