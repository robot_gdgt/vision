"""
Vision - My RS Inteface
"""

import pyrealsense2 as rs

from var_dump import var_dump as vd

print('Starting my_rs...')

# Configure depth and color streams
pipeline = rs.pipeline()
config = rs.config()

config.enable_stream(rs.stream.depth, 640, 480, rs.format.z16, 30)
config.enable_stream(rs.stream.color, 640, 480, rs.format.bgr8, 30)

# Start streaming
profile = pipeline.start(config)

# Get data scale from the device and convert to meters
device = profile.get_device()
depth_sensor = device.first_depth_sensor()
depth_scale = depth_sensor.get_depth_scale() * 1000 # convert to mm
# depth_scale = 0.96 * 1000 # Includes conversion from meters to mm
print(f"depth_scale: {depth_scale}")

# depth_scale = profile.get_device().first_depth_sensor().get_depth_scale() * 1000 # convert to mm
# print(f"depth_scale: {depth_scale}")

# Create an align object
# rs.align allows us to perform alignment of depth frames to others frames
# The 'align_to' is the stream type to which we plan to align depth frames.
align_to = rs.stream.color
align = rs.align(align_to)

def rs_get_color_frame():
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()

    return frames.get_color_frame()

def rs_get_depth_frame():
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()

    return frames.get_depth_frame()

def rs_get_frames():
    # Wait for a coherent pair of frames: depth and color
    frames = pipeline.wait_for_frames()

    # Align the depth frame to color frame
    aligned_frames = align.process(frames)

    # Get aligned frames
    depth_frame = aligned_frames.get_depth_frame() # aligned_depth_frame is a 640x480 depth image
    color_frame = aligned_frames.get_color_frame()

    return (depth_frame, color_frame)

def rs_set_ir_emitter(flag):
    depth_sensor.set_option(rs.option.emitter_enabled, flag)

def rs_set_ir_power(power):
    depth_sensor.set_option(rs.option.laser_power, power)
