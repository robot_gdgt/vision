"""
Vision - Gripper Detect
"""

import numpy as np
import cv2
from time import sleep, time
import math
import logging
import redis
# import os
from pathlib import Path
import json

import globals
from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

# globals.WORLD_OFFSET_X = -32.5  # RGB camera is 32.5 mm to the left of center
# # D435 is 542 mm above robot origin (ground) when looking down
# globals.WORLD_OFFSET_Y = 542
# # D435 ref plane is 117 mm in from of robot origin (center of drive wheels) when looking down
# globals.WORLD_OFFSET_Z = 117

# globals.FLH = 616
# globals.FLV = 616

SAMPLESIZE = 25
GRIP_WIDTH = 62

gripMin = [ 80,  0, 235]
gripMax = [100, 40, 255]

def xyd2local(x, y, d):
    return (d * (x - (640 / 2)) / globals.FLH, d * ((480 / 2) - y) / globals.FLV, d)

def local2world(local):
    x, y, z = local
    r = math.sqrt(y ** 2 + z ** 2)
    angA = math.asin(y / r)

    z1 = r * math.sin(angA + globals.cam_angle_rads)
    h1 = math.sqrt(math.pow(r, 2) - math.pow(z1, 2))

    return (round(globals.WORLD_OFFSET_X + x), round(globals.WORLD_OFFSET_Y - h1 - 10), round(globals.WORLD_OFFSET_Z + z1))

def init():
    globals.cam_angle = 90 - 45  # 45 degrees down
    can.send('frig', CAN_CMD_VISION, False, [VISION_D435_ANGLE, 90 - globals.cam_angle])

def tick():
    color_frame_on = None
    color_frame_off = None

    # Turn on ref LEDs
    can.send('frig', CAN_GRIP_REF_LED, False, [1])
    sleep(0.5)

    # Validate that both frames are valid
    while not color_frame_on:
        depth_frame, color_frame_on = rs_get_frames()

    # Turn off ref LEDs
    can.send('frig', CAN_GRIP_REF_LED, False, [0])
    sleep(0.5)

    # Validate that both frames are valid
    while not color_frame_off:
        depth_frame, color_frame_off = rs_get_frames()

    color_image = np.asanyarray(color_frame_on.get_data())
    gray_image_on = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    gray_image_off = cv2.cvtColor(np.asanyarray(color_frame_off.get_data()), cv2.COLOR_BGR2GRAY)
    # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
    # if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    # return

    _, Mask = cv2.threshold(cv2.absdiff(gray_image_on, gray_image_off), 64, 255, cv2.THRESH_BINARY)
    # Path('/home/pi/tmp/images/mask.lock').touch()
    # cv2.imwrite('/home/pi/tmp/images/mask.png', Mask)
    # Path('/home/pi/tmp/images/mask.lock').unlink()
    # return

    # cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
    # cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

    #################################
    # Find the blue gripper markers #
    #################################
    wX = 0
    wY = 0
    wZ = 0

    # minValues = np.array(gripMin, np.uint8)
    # maxValues = np.array(gripMax, np.uint8)
    # Mask = cv2.inRange(hsv_image, minValues, maxValues)

    # cv2.rectangle(Mask, (0, 0), (640, 250), (0), -1)
    # Mask = cv2.erode(Mask, kernel, iterations=1)
    # Mask = cv2.dilate(Mask, kernel, iterations=1)
    if globals.imagesToWrite & VISION_OUTPUT_MASK: 
        Path('/home/pi/tmp/images/mask.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/mask.png', Mask)
        Path('/home/pi/tmp/images/mask.lock').unlink()

    # _, thresh = cv2.threshold(cMask, 128, 255, 0)
    contours, _ = cv2.findContours(Mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
    # for c in contours:
    #     print(f"{cv2.contourArea(c)} ", end=' ')
    # print()

    if len(contours) >= 2:
        if len(contours) > 2:
            contours = sorted(contours, key=cv2.contourArea, reverse=True)

        # for c in contours:
        #     print(f"{cv2.contourArea(c)} ", end=' ')
        # print()

        pts = []
        for c in contours[:2]:
            x, y, w, h = cv2.boundingRect(c)
            cv2.rectangle(color_image, (x, y), (x + w, y + h), (255, 255, 255), 1)
            pts.append({'x': x + w / 2, 'y': y + h / 2})
        print(f"pt0: {pts[0]['x']}, {pts[0]['y']}  pt1: {pts[1]['x']}, {pts[1]['y']}")

        rW = math.sqrt(math.pow(pts[0]['x'] - pts[1]['x'], 2) + math.pow(pts[0]['y'] - pts[1]['y'], 2))

        cZ = globals.FLH * GRIP_WIDTH / rW
        print(f"cZ: {round(cZ)}")

        pX = int((pts[0]['x'] + pts[1]['x']) / 2)
        pY = int((pts[0]['y'] + pts[1]['y']) / 2)

        # cnz = np.count_nonzero(depth_cropped)
        # if cnz > 0:
        #     Z = np.sum(depth_cropped) / cnz
        # W = 0
        # cZ = np.max(depth_cropped)
        if cZ > 0:
            cX = cZ * (pX - (640 / 2)) / globals.FLH
            cY = cZ * ((480 / 2) - pY) / globals.FLV
            # W = cZ * rW / globals.FLH

            R = math.sqrt(cY ** 2 + cZ ** 2)
            AngA = math.asin(cY / R)

            Z1 = R * math.sin(AngA + globals.cam_angle_rads)
            H1 = math.sqrt(math.pow(R, 2) - math.pow(Z1, 2))

            wX = round(globals.WORLD_OFFSET_X + cX)
            wY = round(globals.WORLD_OFFSET_Y - H1)
            wZ = round(globals.WORLD_OFFSET_Z + Z1)

            # print(f"G Median: {round(cZ)} - Image X, Y: {round(pX)}, {round(grY)} - Grip W: {round(grW)} - Camera X, Y, Z: {round(cX)}, {round(cY)}, {round(cZ)} - World X, Y, Z: {round(wX)}, {round(wY)}, {round(wZ)}")
            print(f"G Image X, Y: {round(pX)}, {round(pY)} - Camera X, Y, Z: {round(cX)}, {round(cY)}, {round(cZ)} - World X, Y, Z: {round(wX)}, {round(wY)}, {round(wZ)}")

            # draw the biggest contour (c) in white
            # cv2.rectangle(color_image, (grX, rY), (grX + rW, rY + rH), (255, 255, 255), 1)
            cv2.line(color_image, (round(pts[0]['x']), round(pts[0]['y'])), (round(pts[1]['x']), round(pts[1]['y'])), (255, 255, 255), 2) # line
            cv2.line(color_image, (pX, pY - 10), (pX, pY + 10), (255, 255, 255), 1)

    if wZ:
        """
        0 can x high
        1 can x low
        2 can y high
        3 can y low
        4 can z high
        5 can z low
        """
        can.send('frig', CAN_TS_GRIP, False, [
            (wX >> 8) & 0xFF,
            wX & 0xFF,
            (wY >> 8) & 0xFF,
            wY & 0xFF,
            (wZ >> 8) & 0xFF,
            wZ & 0xFF,
        ])
        red.set('can_grip', json.dumps({'obj':'grip', 'loc':[wX, wY, wZ]}))

    if globals.imagesToWrite & VISION_OUTPUT_RGB: 
        Path('/home/pi/tmp/images/rgb.lock').touch()
        cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
        Path('/home/pi/tmp/images/rgb.lock').unlink()
