"""
Vision - April Tag Test
"""

import numpy as np
import cv2
import apriltag
from time import sleep, time
import math
# import logging
import redis
# import os
from pathlib import Path
# import json

import globals
from my_rs import rs_get_frames
# import can_proxy_thread as can
# from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
# from can_ids import *
from var_dump import var_dump as vd

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

# get some farmes so the auto-exposure can do its thing
for i in range(0, 5):
    color_frame = None
    # Validate that both frames are valid
    while not color_frame:
        _, color_frame = rs_get_frames()
    print(f"Got frame {i}")
    
def tick():
    color_frame = None

    while not color_frame:
        _, color_frame = rs_get_frames()
    
    color_image = np.asanyarray(color_frame.get_data())
    gray_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2GRAY)
    # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
    # if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    # return

    # cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
    # cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

    ######################
    # Find the april tag #
    ######################

    # define the AprilTags detector options and then detect the AprilTags
    # in the input image
    print("[INFO] detecting AprilTags...")
    options = apriltag.DetectorOptions(families="tag16h5")
    detector = apriltag.Detector(options)
    results = detector.detect(gray_image)
    print(f"[INFO] {len(results)} total AprilTags detected")

    """
    DetectionBase(tag_family='tag36h11', tag_id=2, hamming=0, goodness=0.0, decision_margin=98.58241271972656, homography=array([[ -1.41302664e-01,   1.08428082e+00,   1.67512900e+01],
   [ -8.75899366e-01,   1.50245469e-01,   1.70532040e+00],
   [ -4.89183533e-04,   2.12210247e-03,   6.02052342e-02]]), center=array([ 278.23643912,   28.32511859]), corners=array([[ 269.8939209 ,   41.50381088],
   [ 269.57183838,   11.79248142],
   [ 286.1383667 ,   15.84242821],
   [ 286.18066406,   43.48323059]]))
   """
    
    # loop over the AprilTag detection results
    for r in results:
        vd(r.corners)
        # sides = [0, 0, 0, 0]
        # sides[0] = math.sqrt((r.corners[0][0] - r.corners[1][0]) ** 2 + (r.corners[0][1] - r.corners[1][1]) ** 2)
        # sides[1] = math.sqrt((r.corners[1][0] - r.corners[2][0]) ** 2 + (r.corners[1][1] - r.corners[2][1]) ** 2)
        # sides[2] = math.sqrt((r.corners[2][0] - r.corners[3][0]) ** 2 + (r.corners[2][1] - r.corners[3][1]) ** 2)
        # sides[3] = math.sqrt((r.corners[3][0] - r.corners[0][0]) ** 2 + (r.corners[3][1] - r.corners[0][1]) ** 2)

        longestSideLen = 0
        sideLen = math.sqrt((r.corners[0][0] - r.corners[1][0]) ** 2 + (r.corners[0][1] - r.corners[1][1]) ** 2)
        if sideLen > longestSideLen:
            longestSideLen = sideLen
        sideLen = math.sqrt((r.corners[1][0] - r.corners[2][0]) ** 2 + (r.corners[1][1] - r.corners[2][1]) ** 2)
        if sideLen > longestSideLen:
            longestSideLen = sideLen
        sideLen = math.sqrt((r.corners[2][0] - r.corners[3][0]) ** 2 + (r.corners[2][1] - r.corners[3][1]) ** 2)
        if sideLen > longestSideLen:
            longestSideLen = sideLen
        sideLen = math.sqrt((r.corners[3][0] - r.corners[0][0]) ** 2 + (r.corners[3][1] - r.corners[0][1]) ** 2)
        if sideLen > longestSideLen:
            longestSideLen = sideLen

        # extract the bounding box (x, y)-coordinates for the AprilTag
        # and convert each of the (x, y)-coordinate pairs to integers
        (ptA, ptB, ptC, ptD) = r.corners
        ptB = (int(ptB[0]), int(ptB[1]))
        ptC = (int(ptC[0]), int(ptC[1]))
        ptD = (int(ptD[0]), int(ptD[1]))
        ptA = (int(ptA[0]), int(ptA[1]))
        # draw the bounding box of the AprilTag detection
        cv2.line(color_image, ptA, ptB, (0, 255, 0), 1)
        cv2.line(color_image, ptB, ptC, (0, 255, 0), 1)
        cv2.line(color_image, ptC, ptD, (0, 255, 0), 1)
        cv2.line(color_image, ptD, ptA, (0, 255, 0), 1)
        # draw the center (x, y)-coordinates of the AprilTag
        (cX, cY) = (int(r.center[0]), int(r.center[1]))
        cv2.circle(color_image, (cX, cY), 3, (0, 0, 255), -1) 
        # draw the tag family on the color_image
        # tagFamily = r.tag_family.decode("utf-8")
        cv2.putText(color_image, f"{r.tag_id}", (ptA[0], ptA[1] - 15),
            cv2.FONT_HERSHEY_SIMPLEX, 1.0, (0, 0, 0), 2)
        print(f"tag_id={r.tag_id}, hamming={r.hamming}, goodness={r.goodness}, decision_margin={r.decision_margin}")

    red.set('caption', f"C: {cX},{cY} longestSideLen: {longestSideLen}")
 
    # show the output color_image after AprilTag detection
    Path('/home/pi/tmp/images/rgb.lock').touch()
    cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
    Path('/home/pi/tmp/images/rgb.lock').unlink()

# while(True):
#     tick()
#     sleep(2.0)

tick()
