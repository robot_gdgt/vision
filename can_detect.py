"""
Vision - Can Detect
"""

import numpy as np
import cv2
from time import sleep, time
import math
import logging
import redis
# import os
from pathlib import Path
import json

import globals
from my_rs import rs_get_frames, rs_set_ir_emitter, depth_scale
import can_proxy_thread as can
from RollingMedian import RollingMedian
# from speak_direct import *
from gdgt_platform import *
from can_ids import *
from var_dump import var_dump as vd

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

try:
    red = redis.Redis(unix_socket_path=REDIS_SOCKET, decode_responses=True)
    red.get('dummy')
except redis.exceptions.ConnectionError:
    raise Exception('Is the redis server running?')

SAMPLESIZE = 25

STATE_IDLE = 0
STATE_WAIT_HEADLIGHT = 1
STATE_PROCESSING = 2
STATE_DONE = 9

state = STATE_IDLE

# canMin0 = [0, 140, 70]
# canMax0 = [10, 255, 190]
# canMin1 = [169, 140, 70]
# canMax1 = [179, 255, 190]
canMin0 = [0, 110, 55]
canMax0 = [5, 255, 195]
canMin1 = [174, 110, 55]
canMax1 = [179, 255, 195]

kernel = np.ones((3, 3), np.uint8)

rmX = RollingMedian(5)
rmY = RollingMedian(5)
rmZ = RollingMedian(5)

def xyd2local(x, y, d):
    return (d * (x - (640 / 2)) / globals.FLH, d * ((480 / 2) - y) / globals.FLV, d)

def local2world(local):
    x, y, z = local
    r = math.sqrt(y ** 2 + z ** 2)
    angA = math.asin(y / r)

    z1 = r * math.sin(angA + globals.cam_angle_rads)
    h1 = math.sqrt(math.pow(r, 2) - math.pow(z1, 2))

    return (round(globals.WORLD_OFFSET_X + x), round(globals.WORLD_OFFSET_Y - h1 - 10), round(globals.WORLD_OFFSET_Z + z1))

##### INIT #####

def init():
    global state
    can.del_filter('can_detect', 'all', 'all')
    can.add_filter('can_detect', CAN_STATUS_HEADLIGHT, 0x7FF)

    globals.cam_angle = 90 - 45  # 45 degrees down
    can.send('can_detect', CAN_CMD_VISION, False, [VISION_D435_ANGLE, 90 - globals.cam_angle])
    globals.cam_angle_rads = math.radians(globals.cam_angle)
    globals.cam_angle_sin = math.sin(globals.cam_angle_rads)
    globals.cam_angle_cos = math.cos(globals.cam_angle_rads)

    can.send('can_detect', CAN_CMD_HEADLIGHT, False, [100])

    rmX.reset()
    rmY.reset()
    rmZ.reset()

    state = STATE_WAIT_HEADLIGHT
    
##### TICK #####

def tick():
    # return true to run again
    global state

    if state == STATE_WAIT_HEADLIGHT:
        CANrx = can.get('can_detect')
        if CANrx is not None:
            print(f'\nCAN Rx: {CANrx}', end='')
            if CANrx['id'] == CAN_STATUS_HEADLIGHT and len(CANrx['data']) > 0 and CANrx['data']:
                state = STATE_PROCESSING

    if state == STATE_PROCESSING:
        depth_frame, color_frame = rs_get_frames()

        # Validate that both frames are valid
        if not depth_frame or not color_frame:
            return state

        # Convert images to numpy arrays
        depth_image = np.asanyarray(depth_frame.get_data())
        if globals.imagesToWrite & VISION_OUTPUT_DEPTH: 
            Path('/home/pi/tmp/images/depth.lock').touch()
            cv2.imwrite('/home/pi/tmp/images/depth.jpg', cv2.applyColorMap(cv2.convertScaleAbs(depth_image, alpha=0.03), cv2.COLORMAP_JET))
            Path('/home/pi/tmp/images/depth.lock').unlink()

        color_image = np.asanyarray(color_frame.get_data())
        # color_image = cv2.GaussianBlur(color_image, (5, 5), 0)
        # if globals.imagesToWrite & VISION_OUTPUT_RGB: cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
        # return
        hsv_image = cv2.cvtColor(color_image, cv2.COLOR_BGR2HSV)

        # cv2.line(color_image, (0, 240), (639, 240), (128, 128, 128), 1) # horiz line
        # cv2.line(color_image, (320, 0), (320, 479), (128, 128, 128), 1) # vert line

        ####################
        # Find the red can #
        ####################
        wX = 0
        wY = 0
        wZ = 0

        minValues = np.array(canMin0, np.uint8)
        maxValues = np.array(canMax0, np.uint8)
        Mask0 = cv2.inRange(hsv_image, minValues, maxValues)

        minValues = np.array(canMin1, np.uint8)
        maxValues = np.array(canMax1, np.uint8)
        Mask1 = cv2.inRange(hsv_image, minValues, maxValues)

        Mask = Mask0 + Mask1

        # Mask = cv2.erode(Mask, kernel, iterations=1)
        Mask = cv2.dilate(Mask, kernel, iterations=2)
        if globals.imagesToWrite & VISION_OUTPUT_MASK: 
            Path('/home/pi/tmp/images/mask.lock').touch()
            cv2.imwrite('/home/pi/tmp/images/mask.png', Mask)
            Path('/home/pi/tmp/images/mask.lock').unlink()

        # _, thresh = cv2.threshold(Mask, 128, 255, 0)
        contours, _ = cv2.findContours(Mask, cv2.RETR_EXTERNAL, cv2.CHAIN_APPROX_NONE)
        # for c in contours:
        #     print(f"{cv2.contourArea(c)} ", end=' ')
        # print()

        if len(contours) > 0:
            # find the biggest countour (c) by the area
            c = max(contours, key = cv2.contourArea)
            rX, rY, rW, rH = cv2.boundingRect(c)

            pX = int(rX + rW / 2)
            if SAMPLESIZE < rW:
                sx0 = pX - int(SAMPLESIZE / 2)
                sx1 = sx0 + SAMPLESIZE
            else:
                sx0 = rX
                sx1 = rX + rW
            pY = int(rY + rH / 2)
            if SAMPLESIZE < rH:
                sy0 = pY - int(SAMPLESIZE / 2)
                sy1 = sy0 + SAMPLESIZE
            else:
                sy0 = rY
                sy1 = rY + rH
            
            depth_cropped = depth_image[sy0:sy1, sx0:sx1].astype(float)                  

            cZ = np.median(depth_cropped)
            # print(f"cZ: {cZ}")
            if cZ > 0:
                cZ = cZ * depth_scale
                print(f"cZ: {cZ}")
                cX = cZ * (pX - (640 / 2)) / globals.FLH
                cY = cZ * ((480 / 2) - pY) / globals.FLV
                canW = cZ * rW / globals.FLH
                canH = cZ * rH / globals.FLV

                print(f"canW: {canW} canH: {canH}")

                if canW > 40 and canW < 80 and canH > 50 and canH < 110:
                    R = math.sqrt(cY ** 2 + cZ ** 2)
                    AngA = math.asin(cY / R)

                    Z1 = R * math.sin(AngA + globals.cam_angle_rads)
                    H1 = math.sqrt(math.pow(R, 2) - math.pow(Z1, 2))

                    wX = round(globals.WORLD_OFFSET_X + cX)
                    wY = round(globals.WORLD_OFFSET_Y - H1 - 10)
                    wZ = round(globals.WORLD_OFFSET_Z + Z1)

                    print(f"C Median: {round(cZ)} - Image X, Y: {round(pX)}, {round(pY)} - Can W, H: {round(canW)}, {round(canH)} - Camera X, Y, Z: {round(cX)}, {round(cY)}, {round(cZ)} - World X, Y, Z: {round(wX)}, {round(wY)}, {round(wZ)}")

                    # draw the biggest contour (c) in white
                    cv2.rectangle(color_image, (rX, rY), (rX + rW, rY + rH), (255, 255, 255), 1)

                    # draw the sample area in blue
                    cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (255, 255, 255), 2)

                    rmX.add(wX)
                    rmY.add(wY)
                    rmZ.add(wZ)

                    if rmX.full:
                        """
                        0 can x high
                        1 can x low
                        2 can y high
                        3 can y low
                        4 can z high
                        5 can z low
                        """
                        wX = rmX.get()
                        wY = rmY.get()
                        wZ = rmZ.get()
                        can.send('can_detect', CAN_STATUS_CAN_POS, False, [
                            (wX >> 8) & 0xFF,
                            wX & 0xFF,
                            (wY >> 8) & 0xFF,
                            wY & 0xFF,
                            (wZ >> 8) & 0xFF,
                            wZ & 0xFF,
                        ])
                        red.set('caption', f"Can: {wX}, {wY}, {wZ}")
                        can.send('can_detect', CAN_CMD_HEADLIGHT, False, [0])
                        can.del_filter('can_detect', 'all', 'all')

                        state = STATE_IDLE

                else:
                    # draw the biggest contour (c) in gray
                    cv2.rectangle(color_image, (rX, rY), (rX + rW, rY + rH), (128, 128, 128), 1)

                    # draw the sample area in gray
                    cv2.rectangle(color_image, (sx0, sy0), (sx1, sy1), (128, 128, 128), 2)

        if globals.imagesToWrite & VISION_OUTPUT_RGB: 
            Path('/home/pi/tmp/images/rgb.lock').touch()
            cv2.imwrite('/home/pi/tmp/images/rgb.jpg', color_image)
            Path('/home/pi/tmp/images/rgb.lock').unlink()

    return state

def idle():
    can.send('can_detect', CAN_CMD_HEADLIGHT, False, [0])
