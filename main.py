"""
Vision - Main
"""

import logging
import math
import os
import subprocess
from time import sleep, monotonic

from can_ids import *
import can_proxy_thread as can
from gdgt_platform import *
from speak_direct_can_thread import speak_direct
from status_led import StatusLED
from tts_groups import *
from var_dump import var_dump as vd

import scanner
# import wall_detect
# import frig_detect
# import door_detect
# import can_detect
# import grip_detect
import globals

hbLED = StatusLED(LED_PIN, True)
hbLED.heartbeat()

# FILE = os.path.splitext(os.path.basename(__file__))[0]
# logger.init(FILE, logging.INFO)
# logger.init(None, logging.INFO)

# logging.basicConfig(level=logging.DEBUG)

##### STATES #####

STATE_NAMES = (
    'Idle',
    'Scanner',
    'Find Hall',
    'Find Frig',
    'Door Align',
    'Find Can',
    'Find Gripper',
    'Find Table',
    'Find Home',
)

##### GLOBALS #####

state = VISION_PGM_IDLE
state_prev = -1
prev_CANrx = None

##### SETUP #####

can.del_filter('main', 'all', 'all')
can.add_filter('main', CAN_CMD_HALT, 0x7FF)
can.add_filter('main', CAN_CMD_QUIT, 0x7FF)
can.add_filter('main', CAN_CMD_VISION, 0x7FF)
can.add_filter('main', CAN_STATUS_POS, 0x7F0)

os.makedirs('/home/pi/tmp/images', exist_ok=True)

##### LOOP #####

spinner = 0
spinners = ['-', '\\', '|', '/']
print('READY\n-', end='')
spinnerTrigger = monotonic()
# loopCounter = 0
# loopCounterStart = process_time()
# frameCounter = 0

while True:
    if (monotonic() - spinnerTrigger) >= 0.1:
        spinner = (spinner + 1) % 4
        print(f'\b{spinners[spinner]}', end='', flush=True)
        spinnerTrigger = monotonic()

    CANrx = can.get('main')
    if CANrx is not None:
        print(f'\nCAN Rx id: {format(CANrx["id"], "#X")} msg: {CANrx}')
        if CANrx['id'] == CAN_CMD_VISION and len(CANrx['data']) > 0:
            if CANrx['data'][0] == VISION_PGM and len(CANrx['data']) > 1:
                state = CANrx['data'][1]
            elif CANrx['data'][0] == VISION_D435_ANGLE and len(CANrx['data']) > 1:
                globals.cam_angle = 90 - CANrx['data'][1]
                logging.info(f'globals.cam_angle: {globals.cam_angle}')
                globals.cam_angle_rads = math.radians(globals.cam_angle)
                globals.cam_angle_sin = math.sin(globals.cam_angle_rads)
                logging.info(f'globals.cam_angle_sin: {globals.cam_angle_sin}')
                globals.cam_angle_cos = math.cos(globals.cam_angle_rads)
                logging.info(f'globals.cam_angle_cos: {globals.cam_angle_cos}')
            elif CANrx['data'][0] == VISION_OUTPUT and len(CANrx['data']) > 1:
                globals.imagesToWrite = CANrx['data'][1]
                logging.info(f'globals.imagesToWrite: {globals.imagesToWrite}')
        elif (CANrx['id'] & 0x7F0) == CAN_STATUS_POS and CANrx['rr']:
            print('get position of something')
            state = CANrx['id']
        elif CANrx['id'] == CAN_CMD_QUIT:
            logging.info('quit')
            state = CAN_CMD_QUIT
            break
        elif CANrx['id'] == CAN_CMD_HALT:
            logging.info('shutdown')
            state = CAN_CMD_HALT
            break

    state_changed = (state_prev != state)
    if state_changed:
        if state_prev in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_HALL_WALLS_POS, CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS}:
            scanner.idle()

        if state < len(STATE_NAMES): 
            print(f'\n\n===== New state: {STATE_NAMES[state]} ({state}) =====')
        else:
            print(f'\n\n===== New state: {state} =====')
        state_prev = state

    if state == CAN_CMD_QUIT or state == CAN_CMD_HALT:
        break

    if state in {CAN_STATUS_HALL_ENTER_POS, CAN_STATUS_HALL_WALLS_POS, CAN_STATUS_FRIG_POS, CAN_STATUS_DOOR_POS}:
        if state_changed:
            scanner.init(state)
            frameCounter = 0
        
        ret = scanner.tick(state)
        if not ret: # return true to run again
            state = VISION_PGM_IDLE

    # elif state == VISION_PGM_WALL_FIND:
    #     if state_changed:
    #         wall_detect.init(state)
    #         frameCounter = 0
        
    #     ret = wall_detect.tick(state)
    #     if not ret: # return true to run again
    #         state = VISION_PGM_IDLE
    #     elif ret == 2:
    #         frameCounter = frameCounter + 1

    # elif state == VISION_PGM_FRIG_FIND:
    #     if state_changed:
    #         frig_detect.init(state)
        
    #     frig_detect.tick(state)

    # elif state == VISION_PGM_DOOR_ALIGN:
    #     if state_changed:
    #         door_detect.init(state)
        
    #     door_detect.tick(state)

    elif state == VISION_PGM_CAN:
        if state_changed:
            can_detect.init(state)
        
        if not can_detect.tick(state): # return true to run again
            state = VISION_PGM_IDLE

    elif state == VISION_PGM_GRIP:
        if state_changed:
            grip_detect.init(state)
        
        grip_detect.tick(state)

    elif state == VISION_PGM_TABLE:
        if state_changed:
            pass

    elif state == VISION_PGM_HOME:
        if state_changed:
            pass

    # Calculating the fps
    # now = process_time()
    # if now - loopCounterStart >= 5:
    #     lps = loopCounter / (now - loopCounterStart)
    #     fps = frameCounter / (now - loopCounterStart)
    #     print(f"LPS: {round(lps, 0)} FPS: {round(fps, 2)}")
    #     loopCounterStart = now
    #     loopCounter = 0
    #     frameCounter = 0

    # Blink the LED
    hbLED.tickle()

if state == CAN_CMD_HALT:
    sleep(5)
    subprocess.run("sudo halt", shell=True)

print('\nGood-bye')
